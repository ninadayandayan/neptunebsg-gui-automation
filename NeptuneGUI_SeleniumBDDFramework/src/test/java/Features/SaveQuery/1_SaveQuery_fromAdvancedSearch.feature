Feature: Save a query thru Dashboard - Advanced Search

  @Sanity
  Scenario Outline: Save a query thru Dashboard - Advanced Search - Security ID
    And enter single full <secID> search on Security ID field
    And User save a query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | secID        | queryName              |
      | BE6000183549 | TestAutoQuery_AS_SECID |


  #-------------------------save multiple queries-------------------------------#
  @Sanity
  Scenario Outline: Save a query thru Dashboard - Advanced Search - Currency, SecName and Side
    And enter ticker <secName> search on Security Name field
    And enter parameter <entityName> search on Entity Name field
    And User select SIDE <side> from SIDE drop down list
    And User save a query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | entityName | secName | side | queryName                |
      | Deutsche   | DB      | BID  | TestAutoQuery_AS_Random1 |

  @Sanity
  Scenario Outline: Save a query thru Dashboard - Advanced Search - SecID, Multiple Sector and Type
    And enter single full <secID> search on Security ID field
    And User select TYPE <type> from TYPE drop down list
    And User save a query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | secID        | type | queryName                |
      | BE6248510610 | AXE  | TestAutoQuery_AS_Random2 |


