Feature: Save a query thru Dashboard - Quick Search

  @Sanity
  Scenario Outline: Save a query thru Dashboard - Quick Search - Security ID
    And enter security ID <value> search on Quick search input field
    And User save a query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | value        | queryName              |
      | BE6282459609 | TestAutoQuery_QS_SECID |

        #-----------------------Save multiple queries------------------------------#

  @Sanity
  Scenario Outline: Save a query thru Dashboard - Quick Search - Currency, SecName and Type
    And enter currency <value1> search on Quick search input field
    And enter security Name <value2> search on Quick search input field
    And enter side <value3> search on Quick search input field
    And User save a query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | value1 | value2 | value3 | queryName                |
      | USD    | SOCGEN | INV    | TestAutoQuery_QS_Random1 |

  @Sanity
  Scenario Outline: Save a query thru Dashboard - Quick Search - Currency, SecName and Type
    And enter currency <value1> search on Quick search input field
    And enter security Name <value2> search on Quick search input field
    And enter side <value3> search on Quick search input field
    And User save a query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | value1 | value2 | value3 | queryName                |
      | GBP    | BAC    | AXE    | TestAutoQuery_QS_Random2 |

