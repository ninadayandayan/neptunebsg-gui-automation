Feature: Save a query thru Dashboard - Advanced Search

  @Sanity
  Scenario Outline: Save a query thru Dashboard - Advanced Search
    When User clicks on Advanced Search tab
    And User select the saved query <queryName> in My Queries under Saved Queries
    Then verify if results <secID> are displayed based on the filter used by the query
    Then verify if search parameter is displayed in Recent Search as <query>
    Then User sign out successfully

    Examples:
      | queryName              | query                  | secID        |
      | TestAutoQuery_AS_SECID | TestAutoQuery_AS_SECID | IT0004890882 |


