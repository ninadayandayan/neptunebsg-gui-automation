Feature: Save a shared query thru Dashboard - Quick Search

  @Sanity
  Scenario Outline: Save a shared query thru Dashboard - Quick Search - Security ID
    And enter security ID <value> search on Quick search input field
    And User save a shared query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | value        | queryName               |
      | DE000A11QR73 | TestAutoShared_QS_SECID |

  @Sanity
  Scenario Outline: Save a shared query thru Dashboard - Quick Search - Security Name
    And enter security Name <value> search on Quick search input field
    And User save a shared query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | value  | queryName                 |
      | BAYNGR | TestAutoShared_QS_SECName |


  @Sanity
  Scenario Outline: Save a shared query thru Dashboard - Quick Search - Entity Name
    And enter entity name <value> on Quick search input field
    And User save a shared query <queryName>
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | value    | queryName                |
      | Deutsche | TestAutoShared_QS_Entity |