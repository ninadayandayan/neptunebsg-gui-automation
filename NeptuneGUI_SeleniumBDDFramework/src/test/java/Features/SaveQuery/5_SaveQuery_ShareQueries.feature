Feature: Share one query thru Dashboard - Quick Search

  @skipped
  Scenario Outline: Share one query thru Dashboard - Quick Search
    And User select the saved query <queryName> in My Queries under Saved Queries
    And User share one query <queryName> in My Queries under Saved Queries
    Then User sign out successfully

    Examples:

      | queryName              |
      | TestAutoQuery_QS_SECID |


  @skipped
  Scenario Outline: Share one query thru Dashboard - Quick Search
    And User select the second saved query <queryName> in My Queries under Saved Queries
    And User share second query <queryName> in My Queries under Saved Queries
    Then User sign out successfully

    Examples:

      | queryName                |
      | TestAutoQuery_QS_SECName |

