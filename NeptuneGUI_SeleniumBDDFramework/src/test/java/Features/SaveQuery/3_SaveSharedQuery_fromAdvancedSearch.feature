Feature: Save a shared query thru Dashboard - Advanced Search

  @Sanity
  Scenario Outline: Save a shared query thru Dashboard - Advanced Search - Security ID
    When User clicks on Advanced Search tab
    And enter single full <secID> search on Security ID field
    And User save a shared query <queryName>
    Then verify if saved shared query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | secID        | queryName                    |
      | IT0004890882 | TestAutoSharedQuery_AS_SECID |

  @Sanity
  Scenario Outline: Save a shared query thru Dashboard - Advanced Search - Security Name
    When User clicks on Advanced Search tab
    And User save a shared query <queryName>
    Then verify if saved shared query <queryName> is saved in My Queries under Saved Queries - Dashboard
    Then User sign out successfully

    Examples:
      | queryName                      |
      | TestAutoSharedQuery_AS_SECName |


