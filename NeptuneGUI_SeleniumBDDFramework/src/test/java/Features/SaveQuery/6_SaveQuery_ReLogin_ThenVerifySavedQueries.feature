Feature: Re-Login to check if Saved Queries are still being retrieved = DASHBOARD

  @Sanity
  Scenario Outline: Re-Login and check saved query - Advanced Search
    When User clicks on Advanced Search tab
    #And enter parameter <entityName> search on Entity Name field
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    #Then verify if results <value> are displayed based on the filter used by the query
    #Then verify if search parameter is displayed in Recent Search as <queryName>
    Then User sign out successfully

    Examples:
      | queryName              |
      | TestAutoQuery_AS_SECID |


  @Sanity
  Scenario Outline: Re-Login and check saved query - Advanced Search
    When User clicks on Advanced Search tab
    #And User select the saved query <queryName> in My Queries under Saved Queries
    Then verify if saved query <queryName> is saved in My Queries under Saved Queries - Dashboard
    #Then verify if results <secID> are displayed based on the filter used by the query
    #Then verify if search parameter is displayed in Recent Search as <queryName>

    Examples:
      | queryName                |
      | TestAutoQuery_AS_SecName |