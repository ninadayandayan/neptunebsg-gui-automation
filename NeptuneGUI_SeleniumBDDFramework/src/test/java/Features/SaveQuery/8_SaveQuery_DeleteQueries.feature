Feature: Share one query thru Dashboard - Quick Search

  @skipped
  Scenario Outline: Share one query thru Dashboard - Quick Search
    And User select the saved query <queryName> in My Queries under Saved Queries
    And User delete one query <queryName> in My Queries under Saved Queries
    Then User sign out successfully

    Examples:

      | queryName                |
      | TestAutoQuery_QS_Random1 |


  @skipped
  Scenario Outline: Share one query thru Dashboard - Quick Search
    And User select the second saved query <queryName> in My Queries under Saved Queries
    And User delete second query <queryName> in My Queries under Saved Queries
    Then User sign out successfully

    Examples:

      | queryName                |
      | TestAutoQuery_QS_Random2 |
