Feature: Advanced Search: Recent search should show the last 5 used search criteria

  @Regression
  Scenario Outline:Recent search should show the last 5 used search criteria
    When User clicks on Advanced Search tab
    And User press enter from keyboard - Advanced Search
    Then verify if search parameter is displayed in Recent Search as <enter_query>
    And User clear search results
    And enter select ccy <ccy> on CCY drop down list
    Then verify if search parameter is displayed in Recent Search as <ccy_query>
    And User clear search results
    And enter single partial <secID> search on Security ID field
    Then verify if search parameter is displayed in Recent Search as <secID_query>
    And User clear search results
    And enter ticker <secName> search on Security Name field
    Then verify if search parameter is displayed in Recent Search as <secName_query>
    And User clear search results
    And enter parameter <entityName> search on Entity Name field
    Then verify if search parameter is displayed in Recent Search as <entityName_query>
    And User sign out successfully

    Examples:

      | ccy      | secID | secName | entityName | enter_query      | ccy_query           | secID_query | secName_query | entityName_query |
      | Multiple | XS    | BAYNGR  | Deutsche   | [All Securities] | [MULTIPLE CURRENCY] | XS          | BAYNGR        | Deutsche         |









