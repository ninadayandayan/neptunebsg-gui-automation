Feature: Advanced Search: Enter a search for Entity Name

  @Sanity_B1
  Scenario Outline:Enter a search with FULL Entity Name
    When User clicks on Advanced Search tab

    #---------full entity name----------#

    And enter parameter <fullEntity> search on Entity Name field
    Then verify if search parameter <fullEntity> is displayed in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <fullN_query>

    #-------partial entity name---------#

    And enter parameter <partialEntity> search on Entity Name field
    Then verify if search parameter <partialEntity> is displayed in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <partialN_query>
    And User sign out successfully

    Examples:
      | fullEntity                 | fullN_query                | partialEntity | partialN_query |
      | Deutsche Bahn Finance B.V. | Deutsche Bahn Finance B.V. | Morgan        | Morgan         |


  @
  Scenario Outline:Enter a search with partial Entity Name
    When User clicks on Advanced Search tab
    And enter parameter <value> search on Entity Name field
    Then verify if search parameter <value> is displayed in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | partialEntity | partialN_query |
      | Deutsche      | Deutsche       |







