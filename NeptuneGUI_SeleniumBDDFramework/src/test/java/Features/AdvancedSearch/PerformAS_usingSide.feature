Feature: Perform Advanced Search using Side

  @Sanity_B1
  Scenario Outline: Perform Advanced Search using Side - BID and ASK
    When User clicks on Advanced Search tab
    And User select SIDE <bid_value> from SIDE drop down list
    Then verify if side <bid_query> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <bid_query>

    #--------------------ASK---------------#
    And User select SIDE <ask_value> from SIDE drop down list
    Then verify if side <ask_value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <ask_query>

    And User sign out successfully
    Examples:
      | bid_value | bid_query | ask_value | ask_query |
      | BID       | BID       | ASK       | ASK       |


  @
  Scenario Outline: Perform Advanced Search using Side - ASK
    When User clicks on Advanced Search tab
    And User select SIDE <value> from SIDE drop down list
    Then verify if side <value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value | query |
      | ASK   | ASK   |