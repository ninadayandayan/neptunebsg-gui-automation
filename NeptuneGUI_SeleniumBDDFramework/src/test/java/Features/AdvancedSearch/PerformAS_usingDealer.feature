Feature: Advanced Search: Perform search using single dealer

  @inprogress
  Scenario Outline:Perform search using single and multiple dealer
    When User clicks on Advanced Search tab
    And enter select dealer <value> on dealer drop down list
    Then verify if search parameter <dealerName> is displayed in Dashboard - Dealer
    Then verify if search parameter is displayed in Recent Search as <query>


    #---------------Multiple dealers------------------#
    And enter select dealer <value> on dealer drop down list
    Then verify if multiple search parameters <dealerC> <dealerD> are displayed in Dashboard - Dealer
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value  | dealerName                | query                    | value    | dealerC                   | dealerD                   | query                  |
      | Single | Etrading Software Bank A1 | Etrading Software Bank A | Multiple | Etrading Software Bank C1 | Etrading Software Bank D1 | [MULTIPLE SOURCE DESC] |

  @
  Scenario Outline:Perform search using multiple dealer
    #When User clicks on Advanced Search tab
    #And enter select dealer <value> on dealer drop down list
    #Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Dealer
    #Then verify if search parameter is displayed in Recent Search as <query>
    #And User sign out successfully

    Examples:
      | value    | value1                    | value2                    | query                  |
      | Multiple | Etrading Software Bank C1 | Etrading Software Bank D1 | [MULTIPLE SOURCE DESC] |





