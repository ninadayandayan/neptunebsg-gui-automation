Feature: Advanced Search: Enter a search with a no match Security Name

  @Regression
  Scenario Outline:Enter a search with Security Name that has special characters
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if popup error messages are displayed in the screen
    And User sign out successfully

    Examples:
      | value           |
      | BAYNGR@#$%^&*() |









