Feature: Perform Advanced Search for Cancelled Status in Market Depth Tab

  @Sanity_B1
  Scenario Outline: Perform Advanced Search for selected indication with Cancelled Status
    When User clicks on Advanced Search tab
    Then User unchecked exclude cancels checkbox and click search
    Then verify if search parameter <value> is displayed in Dashboard -  CXL
    Then verify if search parameter <value> is displayed in Market Depth - CXL
    And User sign out successfully

    Examples:
      |value   |
      | CXL    |