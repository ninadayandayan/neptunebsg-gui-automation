Feature: Advanced Search: Perform search using single and multiple sectors

  @Sanity_B1
  Scenario Outline:Perform search using single and multiple sector
    And user select sector <single_value> on sector drop down list
    Then verify if search parameter <singleSector> is displayed in Dashboard - Sector
    Then verify if search parameter is displayed in Recent Search as <singleSector_query>

    #-------multiple sectors--------------#
    And user select sector <multiple_value> on sector drop down list
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Sector
    Then verify if search parameter is displayed in Recent Search as <multipleSector_query>

    And User sign out successfully

    Examples:
      | single_value | singleSector    | singleSector_query | multiple_value | value1 | value2  | multipleSector_query |
      | Single       | Basic Resources | Basic Resources    | Multiple       | Banks  | Covered | [MULTIPLE SECTOR]    |

  @
  Scenario Outline:Perform search using multiple sector
    And user select sector <value> on sector drop down list
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Sector
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value    | value1 | value2  | query             |
      | Multiple | Banks  | Covered | [MULTIPLE SECTOR] |









