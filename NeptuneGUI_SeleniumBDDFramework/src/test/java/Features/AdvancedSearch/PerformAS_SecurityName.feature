Feature: Advanced Search: Enter a search with partial Security Name

  @Sanity_B1
  Scenario Outline:Enter a search with partial Security Name
    When User clicks on Advanced Search tab
    And enter ticker <value> search on Security Name field
    Then verify if search parameter <value> is displayed in Dashboard - Security Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value  | query  |
      | BAYNGR | BAYNGR |









