Feature: Advanced Search: Enter an INVALID Security ID

  @Regression
  Scenario Outline:Enter a search with Security ID that has special characters
    When User clicks on Advanced Search tab
    And enter ticker <value1> search on Security Name field
    Then verify if popup error messages are displayed in the screen
    And enter ticker <value2> search on Security Name field
    Then verify if popup error messages are displayed in the screen
    And enter ticker <value3> search on Security Name field
    Then verify if popup error messages are displayed in the screen
    And User sign out successfully

    Examples:
      | value1             | value2                       | value3       |
      | XS1954740286_@#$%^ | ^&*()111qwertyuuioopadlffdld | BE6000183502 |
