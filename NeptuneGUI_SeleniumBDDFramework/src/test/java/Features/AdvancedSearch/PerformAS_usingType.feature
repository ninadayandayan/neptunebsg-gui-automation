Feature: Perform Advanced Search using Type

  @Sanity_B1
  Scenario Outline: Perform Advanced Search using Type - AXE and INV
    When User clicks on Advanced Search tab
    And User select TYPE <axe_value> from TYPE drop down list
    Then verify if type <axe_value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <axe_query>

    #---------------INV-----------------#
    And User select TYPE <inv_value> from TYPE drop down list
    Then verify if type <inv_value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <inv_query>

    And User sign out successfully

    Examples:
      | axe_value | axe_query | inv_value | inv_query |
      | Axe       | AXE       | Inv       | INV       |


  @
  Scenario Outline: Perform Advanced Search using Type - INV
    When User clicks on Advanced Search tab
    And User select TYPE <value> from TYPE drop down list
    Then verify if type <value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value | query |
      | Inv   | INV   |