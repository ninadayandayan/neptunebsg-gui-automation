Feature: User clicks expand button

  @Regression
  Scenario: User clicks expand button
    Given User clicks on Advanced Search tab
    And User press enter from keyboard - Advanced Search
    And User clicks on expand button
    Then User sign out successfully
