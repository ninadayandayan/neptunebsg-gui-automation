Feature: Advanced Search: Perform search using single and multiple Country of Issuer

  @Sanity_B1
  Scenario Outline:Perform search using single Country of Issuer
    When User clicks on Advanced Search tab
    And user select Country of Issuer <single_cc> <single_CI_val> on Ctry of Issuer drop down list
    Then verify if search parameter <single_CI> is displayed in Dashboard - Ctry of Issuer
    Then verify if search parameter is displayed in Recent Search as <singleCI_query>

    #-----------Multiple Country of Issuer-----------#
    And user select Country of Issuer <multiple_cc> <multiple_CI_val> on Ctry of Issuer drop down list
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Ctry of Issuer
    Then verify if search parameter is displayed in Recent Search as <multipleCI_query>
    And User sign out successfully

    Examples:
      | single_cc | single_CI_val | single_CI | singleCI_query     | multiple_cc | multiple_CI_val | value1        | value2         | multipleCI_query          |
      | AUS       | Single        | Australia | Australia (Issuer) | united      | Multiple        | United States | United Kingdom | [MULTIPLE COUNTRY ISSUER] |

  @
  Scenario Outline:Perform search using multiple Country of Issuer
    When User clicks on Advanced Search tab
    And user select Country of Issuer <country_code> <value> on Ctry of Issuer drop down list
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Ctry of Issuer
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | country_code | value    | value1        | value2         | query                     |
      | united       | Multiple | United States | United Kingdom | [MULTIPLE COUNTRY ISSUER] |








