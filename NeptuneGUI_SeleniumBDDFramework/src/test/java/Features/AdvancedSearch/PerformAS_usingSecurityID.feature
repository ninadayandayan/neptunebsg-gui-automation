Feature: Advanced Search: Enter a search using Security ID(Full,Partial and Multiple ISINs search)

  @Sanity_B1
  Scenario Outline:Enter a search with specific Security ID
    When User clicks on Advanced Search tab

    #-------enter single full security ID---------#
    And enter single full <singlefull_value> search on Security ID field
    Then verify if search parameter <singlefull_value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <singlefull_query>

     #-----enter single partial security ID-------#
    And enter single partial <singlepartial_value> search on Security ID field
    Then verify if search parameter <singlepartial_value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <singlepartial_query>

    #-------------enter second ISIN----------#
    And enter single full <singlefull2> search on Security ID field
    Then verify if search parameter <singlefull2> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <singlefull2query>

    #-------------enter third ISIN----------#
    And enter single full <singlefull3> search on Security ID field
    Then verify if search parameter <singlefull3> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <singlefull3query>

    And User sign out successfully

    Examples:
      | singlefull_value | singlefull_query | singlepartial_value | singlepartial_query | singlefull2  | singlefull2query | singlefull3  | singlefull3query |
      | DE000A0T7J03     | DE000A0T7J03     | DE                  | DE                  | BE6282459609 | BE6282459609     | DE000A11QR65 | DE000A11QR65     |

  @
  Scenario Outline: 2nd Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        | query        |
      | BE6282459609 | BE6282459609 |


  @
  Scenario Outline: 3rd Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        | query        |
      | DE000A11QR65 | DE000A11QR65 |


