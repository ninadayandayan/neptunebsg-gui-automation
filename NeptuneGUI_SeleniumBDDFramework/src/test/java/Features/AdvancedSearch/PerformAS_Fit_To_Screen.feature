Feature: User clicks fit to screen button

  @Regression
  Scenario: User clicks fit to screen button
    Given User clicks on Advanced Search tab
    And User press enter from keyboard - Advanced Search
    And User clicks on fit to screen button
    Then User sign out successfully
