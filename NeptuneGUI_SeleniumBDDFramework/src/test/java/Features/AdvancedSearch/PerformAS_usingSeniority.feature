Feature: Advanced Search: Perform search using single and multiple Seniority

  @Sanity_B1
  Scenario Outline:Perform search using single Seniority
    When User clicks on Advanced Search tab
    And user select seniority <single_value> on Seniority drop down list
    Then verify if search parameter <singleseniority_value> is displayed in Dashboard - Seniority
    Then verify if search parameter is displayed in Recent Search as <singleseniorityquery>

    #-----------Multiple Seniority------------#
    And user select seniority <multiple_value> on Seniority drop down list
    Then verify if multiple search parameter <value1> <value2> are displayed in Dashboard - Seniority
    Then verify if search parameter is displayed in Recent Search as <multipleseniorityquery>

    And User sign out successfully

    Examples:
      | single_value | singleseniority_value | singleseniorityquery | multiple_value | value1 | value2         | multipleseniorityquery |
      | Single       | Senior                | Senior               | Multiple       | Senior | Unsubordinated | [MULTIPLE SENIORITY]   |


  @
  Scenario Outline:Perform search using multiple Seniority
    When User clicks on Advanced Search tab
    And user select seniority <value> on Seniority drop down list
    Then verify if multiple search parameter <value1> <value2> are displayed in Dashboard - Seniority
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value    | value1 | value2         | query                |
      | Multiple | Senior | Unsubordinated | [MULTIPLE SENIORITY] |









