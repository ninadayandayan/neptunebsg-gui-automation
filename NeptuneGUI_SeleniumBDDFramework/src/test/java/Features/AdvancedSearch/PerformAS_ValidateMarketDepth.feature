Feature: Advanced Search: Search for specific Security ID and validate Market Depth load

  @Sanity_B1
  Scenario Outline: Advanced Search: Search for specific Security ID and validate Market Depth load
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    And User select indication and validate market depth load
    And User sign out successfully

    Examples:
      | value       |
      | DE000A0T7J03|

  @skip
  Scenario Outline: Watch List: Search for specific Security ID and validate Market Depth load
    #When User clicks on Watch list
    And enter single full <value> search on Security ID field
    And User select indication and validate market depth load
    And User sign out successfully
    Examples:
      | value |
      | DE000A0T7J03|
