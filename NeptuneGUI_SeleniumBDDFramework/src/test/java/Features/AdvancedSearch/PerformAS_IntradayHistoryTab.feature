Feature: Perform Advanced Search for specific Security ID and verify Intraday History

  @Sanity_B1
  Scenario Outline: Perform Advanced Search for specific Security ID and verify Intraday History
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then User select indication and validate Intraday history - Timestamp and Amount
    And User sign out successfully

    Examples:
      |value            |
      | DE000A0T7J03    |