Feature: Advanced Search: Perform search using single and multiple currency

  @Sanity_B1
  Scenario Outline:Perform search using single and multiple currency
    When User clicks on Advanced Search tab
    And enter select ccy <option_value> on CCY drop down list
    Then verify if search parameter <single_ccy> is displayed in Dashboard -  CCY
    Then verify if search parameter is displayed in Recent Search as <singleCC_query>

    #---------Multiple currency-------------#
    And enter select ccy <value> on CCY drop down list
    Then verify if multiple search parameter <value1> <value2> is displayed in Dashboard -  CCY
    Then verify if search parameter is displayed in Recent Search as <multipleCC_query>
    And User sign out successfully

    Examples:
      | option_value | single_ccy | singleCC_query | value    | value1 | value2 | multipleCC_query    |
      | Single       | EUR        | EUR            | Multiple | GBP    | USD    | [MULTIPLE CURRENCY] |


  @
  Scenario Outline:Perform search using multiple currency
    When User clicks on Advanced Search tab
    And enter select ccy <value> on CCY drop down list
    Then verify if multiple search parameter <value1> <value2> is displayed in Dashboard -  CCY
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value    | value1 | value2 | query               |
      | Multiple | GBP    | USD    | [MULTIPLE CURRENCY] |








