Feature: Advanced Search: Perform search using single and multiple Asset Class

  @Sanity_B1
  Scenario Outline:Perform search using single Asset Class
    When User clicks on Advanced Search tab
    And user select asset class <singleAsset_Val> on Asset Class drop down list
    Then verify if search parameter <assetClass> is displayed in Dashboard - Asset Class
    Then verify if search parameter is displayed in Recent Search as <single_assetQuery>

    #----------------Multiple Asset Class-------------#
    And user select asset class <multipleAsset_Val> on Asset Class drop down list
    Then verify if multiple search parameter <value1> <value2> are displayed in Dashboard - Asset Class
    Then verify if search parameter is displayed in Recent Search as <multiple_assetQuery>
    And User sign out successfully

    Examples:
      | singleAsset_Val | assetClass | single_assetQuery | multipleAsset_Val | value1 | value2    | multiple_assetQuery |
      | Single          | Corporate  | Corporate         | Multiple          | Agency | Municipal | [MULTIPLE ASSET]    |


  @
  Scenario Outline:Perform search using multiple Asset Class
    When User clicks on Advanced Search tab
    And user select asset class <value> on Asset Class drop down list
    Then verify if multiple search parameter <value1> <value2> are displayed in Dashboard - Asset Class
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value    | value1 | value2    | query            |
      | Multiple | Agency | Municipal | [MULTIPLE ASSET] |









