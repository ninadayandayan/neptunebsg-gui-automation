Feature: Clear search results from Advanced Search

  @Sanity_B1
  Scenario Outline: Press enter from keyboard to trigger search upon login
    Given User clicks on Advanced Search tab
    And enter ticker <secName> search on Security Name field
    And verify if search parameter <secName> is displayed in Dashboard - Security Name
    And User press clear to clear search results
    Then User sign out successfully

    Examples:
      | secName |
      | BAYNGR  |
