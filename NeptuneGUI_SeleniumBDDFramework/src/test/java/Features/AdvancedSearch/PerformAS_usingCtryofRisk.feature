Feature: Advanced Search: Perform search using single and multiple Country of Risk

  @Sanity_B1
  Scenario Outline:Perform search using single and multiple Country of Risk
    When User clicks on Advanced Search tab
    And user select Country of Risk <single_cc> <single_COR> on Ctry of Risk drop down list
    Then verify if search parameter <value1> is displayed in Dashboard - Ctry of Risk
    Then verify if search parameter is displayed in Recent Search as <COR_query>

    #--------------Multiple Country of Risk---------------#
    And user select Country of Risk <multiple_cc> <multiple_COR> on Ctry of Risk drop down list
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Ctry of Risk
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | single_cc | single_COR | value1    | COR_query        | multiple_cc | multiple_COR | value1        | value2         | query                   |
      | AUS       | Single     | Australia | Australia (Risk) | united      | Multiple     | United States | United Kingdom | [MULTIPLE COUNTRY RISK] |


  @
  Scenario Outline:Perform search using multiple Country of Risk
    When User clicks on Advanced Search tab
    And user select Country of Risk <country_code> <value> on Ctry of Risk drop down list
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Ctry of Risk
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | country_code | value    | value1        | value2         | query                   |
      | united       | Multiple | United States | United Kingdom | [MULTIPLE COUNTRY RISK] |

