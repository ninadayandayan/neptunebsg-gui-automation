Feature: Advanced Search: Perform search using single and multiple Region

  @Sanity_B1
  Scenario Outline:Perform search using single region
    When User clicks on Advanced Search tab
    And user select region <singleRegion> on region drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Region
    Then verify if search parameter is displayed in Recent Search as <singleR_query>

    #-----------------multiple region----------------#
    And user select region <multipleRegion> on region drop down list
    Then verify if multiple search parameter <value1> <value2> are displayed in Dashboard - Region
    Then verify if search parameter is displayed in Recent Search as <multipleR_query>


    And User sign out successfully
    Examples:
      | value | singleRegion | singleR_query | multipleRegion | value1 | value2 | multipleR_query   |
      | Asia  | Single       | Asia          | Multiple       | Europe | N.Amer | [MULTIPLE REGION] |

  @
  Scenario Outline:Perform search using multiple region
    When User clicks on Advanced Search tab
    And user select region <value> on region drop down list
    Then verify if multiple search parameter <value1> <value2> are displayed in Dashboard - Region
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value    | value1 | value2 | query             |
      | Multiple | Europe | N.Amer | [MULTIPLE REGION] |









