Feature: Login successfully to Neptune

  @SmokeTest
  Scenario Outline: Login with correct username and password in Neptune BSG
    Given User navigate to the login page
    When User enters the login credentials
    And User clicks the login button
    Then User should see the dashboard - "<title>" page
    Then User sign out successfully
    Examples:
      | title |
      |Neptune|
