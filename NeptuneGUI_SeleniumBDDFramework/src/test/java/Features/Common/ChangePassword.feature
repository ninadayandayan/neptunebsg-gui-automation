Feature: Change password in Neptune

  @Security
  Scenario Outline: User can change password for an existing account
    Given User navigate to the login page
    When User enters the login credentials - <companyID>, <username>, <password>
    And User clicks the login button
    Then User should see the dashboard - "<title>" page
    And User select change password the main menu
    Then User update the password - <old_value> and <new_value>
    Examples:
      | title   | old_value  | new_value  | companyID | username | password   |
      | Neptune | Passw0rD01 | Passw0rD02 | DEV_BUY_1 | testauto | Passw0rD01 |

  @Security
  Scenario Outline: User Login with the updated password
    And User login using the updated password - <companyID>, <username>, <new_value>
    Then User should see the dashboard - "<title>" page
    Examples:
      | companyID | username | new_value  | title   |
      | DEV_BUY_1 | testauto | Passw0rD02 | Neptune |