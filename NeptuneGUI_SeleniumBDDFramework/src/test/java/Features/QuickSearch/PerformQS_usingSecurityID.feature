Feature: Quick Search: Enter a search using Security ID -  Full and Partial

  @Sanity_B1
  Scenario Outline:Enter a search with specific Security ID in Quick Search
    And enter security ID <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        | query                |
      | DE000A0T7J03 | [ q : DE000A0T7J03 ] |




  @Sanity_B1
  Scenario Outline:Enter a search with full and partial Security ID in Quick Search
    And enter security ID <value> search on Quick search input field
    Then verify if search parameter <value1> <value2> are displayed from Quick Search in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value           | value1       | value2 | query                   |
      | DE000A0T7J03 US | DE000A0T7J03 | US     | [ q : DE000A0T7J03 US ] |




