Feature: Using Quick Search, perform single full/partial Entity Name search as Entity Name

  @Sanity_B1
  Scenario Outline:Enter a search with FULL Entity Name
    And enter entity name <value> on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                      | query                                   |
      | Deutsche Bahn Finance B.V. | [ entity : Deutsche Bahn Finance B.V. ] |


  @Sanity_B1
  Scenario Outline:Enter a search with partial and full Entity Name
    And enter multiple entity name <value1> <value2> on Quick search input field
    Then verify if search parameters <value1> <value2> are displayed from Quick Search in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value1        | value2 | query                           |
      | Barclays Bank | HSBC   | [ entity : Barclays Bank,HSBC ] |







