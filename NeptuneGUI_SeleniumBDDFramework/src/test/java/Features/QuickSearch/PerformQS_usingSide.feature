Feature: Quick Search - Perform single full/partial Side search as Side

  @Sanity_B1
  Scenario Outline:Perform single SIDE search as Side - BID
    And enter side <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Side
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                    | query          |
      | BID                      | [ side : BID ] |


  @Sanity_B1
  Scenario Outline:Perform single SIDE search as Side - ASK
    And enter side <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Side
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                    | query          |
      | ASK                      | [ side : ASK ] |