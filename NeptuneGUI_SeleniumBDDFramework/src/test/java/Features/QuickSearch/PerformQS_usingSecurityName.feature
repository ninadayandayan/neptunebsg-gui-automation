Feature: Quick Search: Enter a search using Security Name: Full and Partial

  @Sanity_B1
  Scenario Outline:Enter a search with partial Security Name(Ticker) as Quick Search
    And enter security Name <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Security Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value  | query                |
      | BAYNGR | [ secname : BAYNGR ] |


  @Sanity_B1
  Scenario Outline:Enter a search with full Security Name(Ticker) as Sec Name
    And enter security Name <value> search on Quick search input field
    Then verify if search parameter <value> is displayed from Quick Search in Dashboard - Security Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                | query                              |
      | ABIBB 6.5 06/23/2017 | [ secname : ABIBB 6.5 06/23/2017 ] |




