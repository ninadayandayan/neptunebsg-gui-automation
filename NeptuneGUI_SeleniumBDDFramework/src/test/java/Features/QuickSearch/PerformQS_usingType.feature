Feature: Quick Search - Perform single full/partial Side search as Type

  @Sanity_B1
  Scenario Outline:Perform single TYPE search as Type - AXE
    And enter type <value> search on Quick search input field
    Then verify if type <value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value | query          |
      | AXE   | [ type : AXE ] |


  @Sanity_B1
  Scenario Outline:Perform single TYPE search as Type - INV
    And enter type <value> search on Quick search input field
    Then verify if type <value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value | query          |
      | INV   | [ type : INV ] |