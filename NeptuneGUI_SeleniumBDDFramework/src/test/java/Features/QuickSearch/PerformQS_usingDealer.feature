Feature: Quick Search - Perform single full/partial Dealer search as Quick Search

  @inprogress
  Scenario Outline:Perform single full Dealer search as Quick Search
    And enter dealer <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Dealer
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                    | query                                     |
      | ETrading Software Bank A | [ sourcedesc : ETrading Software Bank A ] |


  @Sanity_B1
  Scenario Outline:Perform single partial Dealer search as Quick Search
    And enter multiple dealers <value1> <value2> search on Quick search input field
    Then verify if search parameters <value1> <value2> are displayed in Dashboard - Dealer
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value1                   | value2                   | query                                                              |
      | ETrading Software Bank C | ETrading Software Bank D | [ sourcedesc : ETrading Software Bank C,ETrading Software Bank D ] |