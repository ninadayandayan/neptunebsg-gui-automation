Feature: Using Quick Search, typeahead for include cancels is displayed as "include CXLs"

  @Sanity_B1
  Scenario Outline: Using Quick Search, Cancelled indications are included in the results.
    Then enter include cancels <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard -  CXL
    Then verify if search parameter is displayed in Recent Search as <query>

    And User sign out successfully

    Examples:
      |value      |query                  |
      | cancel    |[ includecancels : Y ] |


  @Sanity_B1
  Scenario Outline: Using Quick Search, typeahead for include cancels is displayed as "include CXLs"
    Then enter include cancels <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard -  CXL
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value       |query                   |
      | include    |[ includecancels : Y ]  |