Feature: Quick Search: Perform search using single and multiple sectors

  @Sanity_B1
  Scenario Outline:Perform search using single sector
    And enter single sector <value> on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Sector
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value           | query                        |
      | Basic Resources | [ sector : Basic Resources ] |

  @Sanity_B1
  Scenario Outline:Perform search using multiple sector
    And enter multiple sector <value1> <value2> on Quick search input field
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Sector
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value1 | value2  | query                      |
      | Banks  | Covered | [ sector : Banks,Covered ] |
