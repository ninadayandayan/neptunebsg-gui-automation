Feature: Perform search by pressing Enter in Quick Search

  @Sanity_B1
  Scenario Outline: Press enter from keyboard to trigger search in Quick Search upon login
    Then User should see the dashboard - "<title>" page
    And User press enter from keyboard - Quick Search
    Then User sign out successfully
    Examples:
      | title |
      |Neptune|