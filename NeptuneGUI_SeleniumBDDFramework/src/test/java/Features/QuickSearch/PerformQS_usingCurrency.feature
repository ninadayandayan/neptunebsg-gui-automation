Feature: Quick Search - Perform single full/partial Currency search as Currency

  @Sanity_B1
  Scenario Outline:Perform single currency search as Currency
    Given user login successfully to Neptune
    And enter currency <value> search on Quick search input field
    Then verify if search parameter <value> is displayed in Dashboard - Currency
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value | query         |
      | EUR   | [ ccy : EUR ] |


  @Sanity_B1
  Scenario Outline:Perform multiple currency search as Currency
    Given user login successfully to Neptune
    And enter multiple currency <value1> <value2> search on Quick search input field
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Currency
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value1 | value2 | query             |
      | USD    | GBP    | [ ccy : USD,GBP ] |
