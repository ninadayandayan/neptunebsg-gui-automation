Feature: Quick Search - Perform search using facet Currency

  @onhold
  Scenario Outline:Perform search using facet Currency
    And User press enter from keyboard - Quick Search
    Then navigate to facet currency <value> and filter single currency
    Then verify if search parameter <value> is displayed in Dashboard -  CCY
    #Then verify if widget can be rearranged
    Then verify if widget can be collapsed
    Then verify if widget can be closed
    And User sign out successfully
    Examples:
      | value |
      | EUR   |