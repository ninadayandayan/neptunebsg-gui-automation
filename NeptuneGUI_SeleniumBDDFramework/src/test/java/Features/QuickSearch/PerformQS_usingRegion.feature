Feature: Quick Search: Perform search using single and multiple region

  @Sanity_B1
  Scenario Outline:Perform search using single region
    And user select region <value> on Quick Search input field
    Then verify if search parameter <value> is displayed in Dashboard - Region
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value | query             |
      | Asia  | [ region : Asia ] |

  @Sanity_B1
  Scenario Outline:Perform search using multiple sector
    And user select multiple regions <value1> <value2> on Quick Search input field
    Then verify if multiple search parameters <value1> <value2> are displayed in Dashboard - Region
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value1 | value2  | query                      |
      | Europe | America | [ region : Europe,N.Amer ] |
