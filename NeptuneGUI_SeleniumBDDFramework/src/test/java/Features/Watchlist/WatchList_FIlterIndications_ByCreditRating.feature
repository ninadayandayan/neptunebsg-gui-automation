Feature: Able to filter indications using Watch List filters - Credit Rating

  @onhold
  Scenario Outline: Able to filter indications using Watch List filters - Single Credit Rating
    Given User clicks on Watch List tab
    And user select credit rating <value> on credit rating dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for Credit Rating filter
    Then User sign out successfully

    Examples:
      | value  | value1 |
      | Single | Asia   |

  @onhold
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Credit Rating
    Given User clicks on Watch List tab
    And user select credit rating <value> on credit rating dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for Credit Rating filter
    Then User sign out successfully

    Examples:
      | value    | value1 | value2 |
      | Multiple | Europe | C.Amer |