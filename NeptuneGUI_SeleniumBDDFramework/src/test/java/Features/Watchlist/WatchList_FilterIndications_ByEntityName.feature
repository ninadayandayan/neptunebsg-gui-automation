Feature: Able to filter indications using Watch List filters - Entity Name

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Full Entity Name
    And User clicks on Watch List tab
    And User enter entity name <value> on Entity Name field in Watch List
    Then verify if watch list tab automatically load up securities <value> for Entity Name filter
    Then User sign out successfully

    Examples:
      | value                      |
      | Deutsche Bahn Finance B.V. |

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Partial Entity Name
    And User clicks on Watch List tab
    And User enter entity name <value> on Entity Name field in Watch List
    Then verify if watch list tab automatically load up securities <value> for Entity Name filter
    Then User sign out successfully

    Examples:
      | value    |
      | Deutsche |