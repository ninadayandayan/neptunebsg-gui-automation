Feature: Able to filter indications using Watch List filters - Type

  @Sanity
  Scenario Outline:Able to filter indications using Watch List filters - AXE
    And User clicks on Watch List tab
    And User select TYPE <value> from TYPE drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for Type filter
    And User sign out successfully

    Examples:

      | value |
      | AXE   |


  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - INV
    And User clicks on Watch List tab
    And User select TYPE <value> from TYPE drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for Type filter
    And User sign out successfully

    Examples:
      | value |
      | INV   |