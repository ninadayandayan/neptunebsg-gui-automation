Feature: Able to filter indications using Watch List filters - Ctry of Issuer

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Single Ctry. of Issuer
    Given User clicks on Watch List tab
    And user select ctry of issuer <value> <country_code> on ctry of issuer dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for Ctry.of Issuer filter
    Then User sign out successfully

    Examples:
      | value  | country_code | value1    |
      | Single | AUS          | Australia |

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Ctry. of Issuer
    Given User clicks on Watch List tab
    And user select ctry of issuer <value> <country_code> on ctry of issuer dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for Ctry.of Issuer filter
    Then User sign out successfully

    Examples:
      | value    | country_code | value1         | value2        |
      | Multiple | United       | United Kingdom | United States |