Feature: Able to filter indications using Watch List filters - Currency

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Single Currency
    Given User clicks on Watch List tab
    And user select ccy <value> on ccy dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for CCY filter
    Then User sign out successfully

    Examples:
      | value  | value1 |
      | Single | EUR    |

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Currency
    Given User clicks on Watch List tab
    And user select multiple ccy <value> on ccy dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for CCY filter
    Then User sign out successfully

    Examples:
      | value    | value1 | value2 |
      | Multiple | GBP    | USD    |