Feature: Able to filter indications using Watch List filters - Include Cancels

  @Sanity
  Scenario Outline:Able to filter indications using Watch List filters - Include Cancels
    And User clicks on Watch List tab
    And User unchecked exclude cancels checkbox in Watch List
    Then verify if watch list tab automatically load up securities <value> for Include Cancels filter
    And User sign out successfully

    Examples:
      | value |
      | CXL   |