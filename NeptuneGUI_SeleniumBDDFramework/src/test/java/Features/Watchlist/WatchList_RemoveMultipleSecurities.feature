Feature: Remove multiple securities from the Watch List using the Watch List Tab

  @Sanity
  Scenario: Remove multiple securities from the Watch List using the Watch List Tab
    Given User clicks on Watch List tab
    And select multiple securities by checking the checkbox and click delete
    Then verify if securities were removed to watchlist
    Then User sign out successfully

