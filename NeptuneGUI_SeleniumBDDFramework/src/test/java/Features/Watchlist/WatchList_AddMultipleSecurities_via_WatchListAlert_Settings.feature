Feature: Add single security via 'Add Securities' - Watch List Alert Settings

  @inprogress
  Scenario Outline: Add single security via 'Add Securities'  - Watch List Alert Settings
    Given User clicks on Watch List tab
    And user clicks on Watch List Alert Settings
    And add security <value> via Add Securities
    Then verify newly added security <value>, <secName> toggle configuration
    Then verify newly added security <value>, <secName> is visible on Watch List Alert Settings table
    Then verify newly added security <value> is visible on Watch List Table
    Then User sign out successfully

    Examples:
      | value        | secName            |
      | US06738EAL92 | BACR 3.25 01/12/21 |


  @inprogress
  Scenario Outline: Add multiple securities via 'Add Securities'  - Watch List Alert Settings
    Given User clicks on Watch List tab
    And user clicks on Watch List Alert Settings
    And add multiple securities <secID1> <secID2> via Add Securities
    Then verify newly added securities <secID1> <secID2> Security Toggle Configuration
    Then verify newly added securities <secID1> <secID2> <secName1> <secName2> are visible on Watch List Alert Settings table
    Then verify newly added securities <secID1>,<secID2> are visible on Watch List Table
    Then User sign out successfully

    Examples:
      | secID1       | secID2       | secName1               | secName2            |
      | XS1078235733 | XS0068009637 | USIMIT 5.75 30/06/2049 | BACR 9.5 07/08/2021 |

