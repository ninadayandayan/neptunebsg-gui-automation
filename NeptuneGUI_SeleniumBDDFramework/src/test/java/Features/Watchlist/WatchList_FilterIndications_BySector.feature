Feature: Able to filter indications using Watch List filters - Sector

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Single Sector
    Given User clicks on Watch List tab
    And user select sector <value> on sector dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for Sector filter
    Then User sign out successfully

    Examples:
      | value  | value1          |
      | Single | Basic Resources |

  @WatchList
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Sector
    Given User clicks on Watch List tab
    And user select multiple sector <value> on sector dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for Sector filter
    Then User sign out successfully

    Examples:
      | value    | value1 | value2  |
      | Multiple | Banks  | Covered |