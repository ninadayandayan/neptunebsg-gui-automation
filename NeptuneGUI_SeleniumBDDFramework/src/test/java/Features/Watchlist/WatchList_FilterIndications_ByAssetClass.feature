Feature: Able to filter indications using Watch List filters - Asset Class

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Single Asset Class
    Given User clicks on Watch List tab
    And user select asset class <value> on asset class dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for Asset Class filter
    Then User sign out successfully

    Examples:
      | value  | value1    |
      | Single | Corporate |

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Asset Class
    Given User clicks on Watch List tab
    And user select asset class <value> on asset class dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for Asset Class filter
    Then User sign out successfully

    Examples:
      | value    | value1 | value2    |
      | Multiple | Agency | Municipal |

