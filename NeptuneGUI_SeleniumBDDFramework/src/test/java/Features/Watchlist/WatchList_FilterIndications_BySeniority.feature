Feature: Able to filter indications using Watch List filters - Seniority

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Single Seniority
    Given User clicks on Watch List tab
    And user select seniority <value> on seniority dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for Seniority filter
    Then User sign out successfully

    Examples:
      | value  | value1 |
      | Single | Senior |

  @WatchList
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Seniority
    Given User clicks on Watch List tab
    And user select seniority <value> on seniority dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for Seniority filter
    Then User sign out successfully

    Examples:
      | value    | value1         | value2       |
      | Multiple | Unsubordinated | Subordinated |