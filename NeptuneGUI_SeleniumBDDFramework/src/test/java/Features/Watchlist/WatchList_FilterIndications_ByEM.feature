Feature: Able to filter indications using Watch List filters - EM

  @Sanity
  Scenario Outline:Able to filter indications using Watch List filters - EM Only
    And User clicks on Watch List tab
    And User select EM <value> from EM drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for EM filter
    And User sign out successfully

    Examples:
      | value     |
      | EM (Only) |


  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - EM Excluded
    And User clicks on Watch List tab
    And User select EM <value> from EM drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for EM filter
    And User sign out successfully

    Examples:
      | value         |
      | EM (Excluded) |