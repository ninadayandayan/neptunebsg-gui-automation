Feature: Able to clear filter indications in Watch List using clear button

  @Sanity
  Scenario Outline: Able to clear filter indications in Watch List using clear button
    And User clicks on Watch List tab
    And User enter sec name <value> on Security Name field in Watch List
    Then verify if watch list tab automatically load up securities <value> for Security Name filter
    And User clicks on clear button
    Then verify if watch list tab automatically load up securities for with default filters
    Then User sign out successfully

    Examples:
      | value                 |
      | BAYNGR 3.0 01/07/2075 |