Feature: Able to filter indications using Watch List filters - Security Name

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Full Security Name
    And User clicks on Watch List tab
    And User enter sec name <value> on Security Name field in Watch List
    Then verify if watch list tab automatically load up securities <value> for Security Name filter
    Then User sign out successfully

    Examples:
      | value                 |
      | BAYNGR 3.0 01/07/2075 |


  @WatchList
  Scenario Outline: Able to filter indications using Watch List filters - Partial Security Name
    And User clicks on Watch List tab
    And User enter sec name <value> on Security Name field in Watch List
    Then verify if watch list tab automatically load up securities <value> for Security Name filter
    Then User sign out successfully

    Examples:
      | value  |
      | BAYNGR |