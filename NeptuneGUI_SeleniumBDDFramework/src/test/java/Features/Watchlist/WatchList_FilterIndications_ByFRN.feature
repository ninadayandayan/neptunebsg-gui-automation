Feature: Able to filter indications using Watch List filters - FRN

  @Sanity
  Scenario Outline:Able to filter indications using Watch List filters - FRN Only
    And User clicks on Watch List tab
    And User select FRN <value> from FRN drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for FRN filter
    And User sign out successfully

    Examples:
      | value      |
      | FRN (Only) |


  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - FRN Excluded
    And User clicks on Watch List tab
    And User select FRN <value> from FRN drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for FRN filter
    And User sign out successfully

    Examples:
      | value          |
      | FRN (Excluded) |