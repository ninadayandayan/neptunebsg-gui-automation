Feature: Able to filter indications using Watch List filters - Side

  @Sanity
  Scenario Outline:Able to filter indications using Watch List filters - BID
    And User clicks on Watch List tab
    And User select SIDE <value> from SIDE drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for Side filter
    And User sign out successfully

    Examples:
      | value |
      | BID   |


  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - ASK
    And User clicks on Watch List tab
    And User select SIDE <value> from SIDE drop down list - WatchList
    Then verify if watch list tab automatically load up securities <value> for Side filter
    And User sign out successfully

    Examples:
      | value |
      | ASK   |