Feature: Able to filter indications using Watch List filters - Region

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Single Region
    Given User clicks on Watch List tab
    And user select region <value> on region dropdown list - Watch List
    Then verify if watch list tab automatically load up securities <value1> for Region filter
    Then User sign out successfully

    Examples:
      | value  | value1 |
      | Single | Asia   |

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Multiple Region
    Given User clicks on Watch List tab
    And user select region <value> on region dropdown list - Watch List
    Then verify if watch list tab automatically load up multiple securities <value1> <value2> for Region filter
    Then User sign out successfully

    Examples:
      | value    | value1 | value2 |
      | Multiple | Europe | C.Amer |