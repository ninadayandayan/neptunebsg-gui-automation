Feature: Add multiple securities to the Watch List using the Dashboard Tab

  @Sanity
  Scenario Outline: Add multiple securities to the Watch List using the Dashboard Tab
    Given User clicks on Advanced Search tab
    And enter ticker <value> search on Security Name field
    And select security by checking the checkbox and press add
    Then verify if watchlist tab is loaded and securities are unchecked
    Then verify if securities are added to watchlist
    Then User sign out successfully

    Examples:
      | value  |
      | BAYNGR |
