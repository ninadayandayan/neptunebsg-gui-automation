Feature: Able to filter indications using Watch List filters - Security ID

  @Sanity
  Scenario Outline: Able to filter indications using Watch List filters - Security ID
    And User clicks on Watch List tab
    And User enter partial sec ID <value> on Security ID field in Watch List
    Then verify if watch list tab automatically load up securities <value> that matches the criteria
    Then User sign out successfully

    Examples:
      | value |
      | DE    |

  @WatchList
  Scenario Outline: Able to filter indications using Watch List filters - Security ID
    And User clicks on Watch List tab
    And User enter multiple sec ID <value1> <value2> on Security ID and verify automatic load of securities
    Then User sign out successfully

    Examples:
      | value1       | value2       |
      | DE000A11QR65 | DE000A11QR65 |