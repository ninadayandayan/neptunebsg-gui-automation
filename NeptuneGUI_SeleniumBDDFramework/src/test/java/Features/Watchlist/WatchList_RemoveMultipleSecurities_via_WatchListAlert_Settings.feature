Feature: Remove a single security via 'Add Securities' - Watch List Alert Settings

  @inprogress
  Scenario Outline: Add single security via 'Add Securities'  - Watch List Alert Settings
    Given User clicks on Watch List tab
    And user clicks on Watch List Alert Settings
    And remove security <value> via Add Securities
    Then verify if security <value> has been removed on Watch List Alert Settings table
    Then verify if security <value> has been removed on Watch List Table
    Then User sign out successfully

    Examples:
      | value        |
      | US06738EAL92 | 