package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearchPage;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import pageObjects.QuickSearchPage;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Search extends BaseUtil {

    public Properties properties = new Properties();
    public File testData = new File("src/main/resources/Configuration.properties");
    public String companyID, username, password;

    //Initialize page object class
    LoginPage login = new LoginPage(driver);
    AdvancedSearchPage advancedsearch = new AdvancedSearchPage(driver);
    MainPage main = new MainPage(driver);
    QuickSearchPage quickSearchPage = new QuickSearchPage(driver);

    public Search() throws AWTException {
    }


    @When("User clicks on Advanced Search tab")
    public void user_clicks_on_Advanced_Search_tab() throws IOException {

        System.out.println("Default tab is Advanced Search");

    }

    @When("enter parameter {} search on Entity Name field")
    public void enter_parameter_search_on_Entity_Name_field(String value) throws InterruptedException {

        advancedsearch.enterEntityName(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Given("enter entity name {} on Quick search input field")
    public void enter_entity_name_on_Quick_search_input_field(String value) {

        quickSearchPage.enterValueasAutoCompleteHint4(value);

    }

    @Given("enter multiple entity name {} {} on Quick search input field")
    public void enter_multiple_entity_name_on_Quick_search_input_field(String value1,String value2) {

        quickSearchPage.enterValueasAutoCompleteHint4(value1);
        quickSearchPage.enterValueasAutoCompleteHint4(value2);

    }

    @Given("enter single sector {}  on Quick Search input field")
    public void enter_single_on_Quick_Search_input_field() {

    }

    @Given("enter multiple sector {} {}  on Quick Search input field")
    public void enter_multiple_sector_on_Quick_Search_input_field() {

    }


    @Then("verify if search parameter {} is displayed in Dashboard - Entity Name")
    public void verify_if_search_parameter_entity_name_is_displayed_in_Dashboard_Entity_Name(String value) throws InterruptedException, IOException {

        main.clickExpand();
        main.checkMidLoop();
        main.verifyDataONGrid();
        main.verifyGrid_EntityName(value);
        takeScreenshot(driver);
        main.clickExpand();

    }




    @And("enter single full {} search on Security ID field")
    public void enterSingleFullSearchOnSecurityIDField(String value) throws InterruptedException {

        advancedsearch.enterSecurityID(value);
        waitforPageLoaded();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @When("enter single partial {} search on Security ID field")
    public void enter_single_partial_search_on_Security_ID_field(String value) throws InterruptedException {

        advancedsearch.enterSecurityID(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }


    @Then("User press clear to clear search results")
    public void user_press_clear_to_clear_search_results() throws InterruptedException, AWTException {

        advancedsearch.keyPress();
        waitforPageLoaded();
        advancedsearch.clearSearchResults();

    }

    @When("enter ticker {} search on Security Name field")
    public void enter_ticker_value_search_on_Security_Name_field(String value) throws InterruptedException {

        advancedsearch.enterSecurityName(value);
        advancedsearch.clickSearchBtn();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Security Name")
    public void verifyIfSearchParameterValueIsDisplayedInDashboardSecurityName(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_secName(value);
        takeScreenshot(driver);
    }

    @Then("verify if search parameter {} is displayed from Quick Search in Dashboard - Security Name")
    public void verify_if_search_parameter_is_displayed_from_Quick_Search_in_Dashboard_Security_Name(String value) throws AWTException, IOException {

        main.verifyGrid_MultipleSecName(value);
        takeScreenshot(driver);
    }

    @And("enter single sector {} on Quick search input field")
    public void enter_single_sector_on_Quick_search_input_field(String value) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value);

    }


    @Then("verify if search parameter {} is displayed in Dashboard - Sector")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Sector(String value) throws InterruptedException, IOException {

        main.clickExpand();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_Sector(value);
        takeScreenshot(driver);
        main.clickExpand();
    }

    @Then("verify if multiple search parameters {} {} are displayed in Dashboard - Sector")
    public void verify_if_multiple_search_parameter_are_displayed_in_Dashboard_Sector(String value1, String value2) throws InterruptedException, IOException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleSector(value1, value2);
        main.clickExpand();
        takeScreenshot(driver);
    }

    @And("enter single sector {} on Quick Search input field")
    public void enter_single_sector_on_Quick_Search_input_field() {



    }

    @And("enter multiple sector {} {} on Quick search input field")
    public void enter_multiple_sector_on_Quick_search_input_field(String value1, String value2) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value1);
        quickSearchPage.enterValueAsAutoCompleteHint5(value2);

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Ctry of Issuer")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Ctry_of_Issuer(String value) throws InterruptedException, IOException {

        main.verifyDataONGrid();
        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_CtryIssuer(value);
        main.clickExpand();
    }

    @Then("verify if multiple search parameters {} {} are displayed in Dashboard - Ctry of Issuer")
    public void verify_if_multiple_search_parameters_are_displayed_in_Dashboard_Ctry_of_Issuer(String value1, String value2) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleCtryIssuer(value1, value2);


    }

    @Then("verify if search parameter {} is displayed in Dashboard - Ctry of Risk")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Ctry_of_Risk(String value) throws InterruptedException, IOException {

        main.verifyDataONGrid();
        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_CtryRisk(value);
        main.clickExpand();
    }

    @Then("verify if multiple search parameters {} {} are displayed in Dashboard - Ctry of Risk")
    public void verify_if_multiple_search_parameters_are_displayed_in_Dashboard_Ctry_of_Risk(String value1, String value2) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleCtryofRisk(value1, value2);
        main.clickExpand();

    }
    @Then("verify if search parameter {} is displayed in Dashboard - Region")
    public void verify_if_search_parameter_Asia_is_displayed_in_Dashboard_Region(String value) throws InterruptedException, IOException {

        main.clickExpand();
        main.checkLoop();
        //main.verifyDataONGrid();
        main.verifyGrid_Region(value);
        main.clickExpand();

    }

    @Then("verify if multiple search parameter {} {} are displayed in Dashboard - Region")
    public void verify_if_multiple_search_parameter_are_displayed_in_Dashboard_Region(String value1, String value2) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleRegion(value1, value2);
        main.clickExpand();
    }
    @Then("verify if search parameter {} is displayed in Dashboard - Seniority")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Seniority(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Seniority(value);
        main.clickExpand();

    }

    @Then("verify if multiple search parameter {} {} are displayed in Dashboard - Seniority")
    public void verify_if_multiple_search_parameter_are_displayed_in_Dashboard_Seniority(String value1,String value2) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleSeniority(value1, value2);
        main.clickExpand();

    }

    @When("User select SIDE {} from SIDE drop down list")
    public void user_select_SIDE_from_SIDE_drop_down_list(String value) throws AWTException, InterruptedException {

        advancedsearch.selectfromSIDE(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @When("User select TYPE {} from TYPE drop down list")
    public void user_select_TYPE_from_type_drop_down_list(String value) throws AWTException, InterruptedException {

        advancedsearch.selectfromType(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }
    @Then("verify if search parameter {} is displayed in Dashboard - Asset Class")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Asset_Class(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_AssetClass(value);
        main.clickExpand();

    }

    @Then("verify if multiple search parameter {} {} are displayed in Dashboard - Asset Class")
    public void verify_if_multiple_search_parameter_are_displayed_in_Dashboard_Asset_Class(String value1, String value2) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleAssetClass(value1, value2);
        main.clickExpand();

    }

    @Then("User press enter from keyboard - Quick Search")
    public void user_press_enter_from_keyboard_QS() throws AWTException, IOException {

        quickSearchPage.keypressEnter();
        waitforPageLoaded();
        main.verifyDataONGrid();
        takeScreenshot(driver);

    }

    @Then("User press enter from keyboard - Advanced Search")
    public void user_press_enter_from_keyboard_AS() throws AWTException {

        advancedsearch.keyPress();
        waitforPageLoaded();

    }

    @Then("enter include cancels {} search on Quick search input field")
    public void enter_include_cancels_search_on_Quick_search_input_field(String value) {

        quickSearchPage.enterValueAsAutoCompleteHint2(value);
    }



    @Then("User unchecked exclude cancels checkbox and click search")
    public void user_unchecked_exclude_cancels_checkbox_and_click_search() throws InterruptedException {

        advancedsearch.setExclCancelsChkBox();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("User enter Sec ID {}, unchecked exclude cancels checkbox and click search")
    public void user_enter_Sec_ID_unchecked_exclude_cancels_checkbox_and_click_search(String value) throws InterruptedException {

        advancedsearch.enterSecurityID(value);
        advancedsearch.setExclCancelsChkBox();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @Given("enter security ID {} search on Quick search input field")
    public void enter_securityID_search_on_Quick_search_input_field(String value) throws AWTException {

        quickSearchPage.enterSecurityID(value);


    }

    @Given("enter security Name {} search on Quick search input field")
    public void enter_securityName_search_on_Quick_search_input_field(String value) throws AWTException {

        quickSearchPage.enterAsSecName(value);
        waitforPageLoaded();
    }

    @Given("enter dealer {} search on Quick search input field")
    public void enter_dealer_search_on_Quick_search_input_field(String value) {

        quickSearchPage.enterValueasAutoCompleteHint3(value);
        waitforPageLoaded();

    }

    @Given("enter side {} search on Quick search input field")
    public void enter_side_search_on_Quick_search_input_field(String value) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value);
        waitforPageLoaded();

    }
    @Given("enter currency {} search on Quick search input field")
    public void enter_single_currency_search_on_Quick_search_input_field(String value) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value);
        waitforPageLoaded();

    }

    @Given("enter multiple currency {} {} search on Quick search input field")
    public void enter_multiple_currency_search_on_Quick_search_input_field(String value1,String value2) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value1);
        quickSearchPage.enterValueAsAutoCompleteHint5(value2);

    }

    //Widget:
    @Then("navigate to facet currency {} and filter single currency")
    public void navigate_to_facet_currency_and_filter_single_currency(String value) {

    quickSearchPage.checkValueinWidget(value);

    }

    @Then("verify if widget can be rearranged")
    public void verify_if_widget_can_be_rearranged() {

        quickSearchPage.rearrangeWidget();

    }

    @Then("verify if widget can be collapsed")
    public void verify_if_widget_can_be_collapsed() {

        quickSearchPage.collapseWidget();

    }

    @Then("verify if widget can be closed")
    public void verify_if_widget_can_be_closed() {

        quickSearchPage.closeWidget();

    }

    public void testMultipleScenarios(){



    }


    @Given("user login successfully to Neptune")
    public void userLoginSuccessfullyToNeptune() {

        waitforPageLoaded();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

    }

    @And("enter select ccy {} on CCY drop down list")
    public void enterSelectCcyOnCCYDropDownList(String value) throws InterruptedException {

        advancedsearch.selectCCY(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @And("user select asset class {} on Asset Class drop down list")
    public void userSelectAssetClassValueOnAssetClassDropDownList(String value) throws InterruptedException, IOException {

        advancedsearch.selectAssetClass(value);
        advancedsearch.clickSearchBtn();
        main.verifyDataONGrid();
        waitforPageLoaded();

    }

    @And("enter select dealer {} on dealer drop down list")
    public void enterSelectDealerValueOnDealerDropDownList(String value) throws InterruptedException {

        waitforPageLoaded();
        advancedsearch.selectDealer(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @And("user select sector {} on sector drop down list")
    public void userSelectSectorValueOnSectorDropDownList(String value) throws InterruptedException {

        waitforPageLoaded();
        advancedsearch.selectSector(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @And("user select Country of Issuer {} {} on Ctry of Issuer drop down list")
    public void userSelectCountryOfIssuerCountry_codeOnCtryOfIssuerDropDownList(String value,String country_code) throws InterruptedException {

        waitforPageLoaded();
        advancedsearch.selectCtryOfIssuer(value, country_code);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @And("user select Country of Risk {} {} on Ctry of Risk drop down list")
    public void userSelectCountryOfRiskCountry_codeValueOnCtryOfRiskDropDownList(String country_code,String value) throws InterruptedException {

        waitforPageLoaded();
        advancedsearch.selectCtryOfRisk(country_code, value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @And("user select region {} on region drop down list")
    public void userSelectRegionValueOnRegionDropDownList(String value) throws InterruptedException, IOException {

        waitforPageLoaded();
        advancedsearch.selectRegion(value);
        advancedsearch.clickSearchBtn();
        //main.verifyDataONGrid();
        waitforPageLoaded();


    }

    @And("user select seniority {} on Seniority drop down list")
    public void userSelectSeniorityValueOnSeniorityDropDownList(String value) throws InterruptedException {

        waitforPageLoaded();
        advancedsearch.selectSeniority(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();


    }

    @Then("verify if popup error messages are displayed in the screen")
    public void verifyIfPopupErrorMessagesAreDisplayedInTheScreen() {

        main.verifyErrorMessage();

    }


    @And("User clear search results")
    public void userClearSearchResults() throws InterruptedException {

        advancedsearch.clearSearchResults();

    }

    @And("enter type {} search on Quick search input field")
    public void enterTypeValueSearchOnQuickSearchInputField(String value) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value);
        waitforPageLoaded();
    }

    @Then("verify if multiple search parameters {} {} are displayed in Dashboard - Dealer")
    public void verifyIfMultipleSearchParametersareDisplayedInDashboardDealer(String value1,String value2) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_MultipleDealer(value1, value2);

    }

    @And("user select region {} on Quick Search input field")
    public void userSelectRegionOnQuickSearchInputField(String value) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value);
        waitforPageLoaded();
    }

    @And("user select multiple regions {} {} on Quick Search input field")
    public void userSelectMultipleRegionOnQuickSearchInputField(String value1,String value2) {

        quickSearchPage.enterValueAsAutoCompleteHint5(value1);
        waitforPageLoaded();
        quickSearchPage.enterValueasAutoCompletedHint7(value2);
        waitforPageLoaded();
    }

    @Then("verify if multiple search parameters {} {} are displayed in Dashboard - Region")
    public void verifyIfMultipleSearchParametersValueValueAreDisplayedInDashboardRegion(String value1,String value2) {
    }

    @And("user clicks ccy {} drop down list")
    public void userClicksCcyDropDownList(String value) {

        advancedsearch.clickCCYandValidateList(value);
    }

    @And("enter multiple dealers {} {} search on Quick search input field")
    public void enterMultipleDealersValueSearchOnQuickSearchInputField(String value1,String value2) {

        quickSearchPage.enterValueasAutoCompleteHint3(value1);
        waitforPageLoaded();
        quickSearchPage.enterValueasAutoCompleteHint3(value2);
        waitforPageLoaded();

    }

    @Then("verify if search parameters {} {} are displayed in Dashboard - Dealer")
    public void verifyIfSearchParametersValueValueAreDisplayedInDashboardDealer(String value1,String value2) throws IOException {


        //main.verifyDataONGrid();
        main.verifyGrid_MultipleDealer(value1, value2);
    }
}
