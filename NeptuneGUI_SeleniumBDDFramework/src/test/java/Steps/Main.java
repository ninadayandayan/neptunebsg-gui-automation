package Steps;

import Base.BaseUtil;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageObjects.AdvancedSearchPage;
import pageObjects.LoginPage;
import pageObjects.MainPage;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Main extends BaseUtil {

    public Properties properties = new Properties();
    public File testData = new File("src/main/resources/Configuration.properties");
    public String companyID, username, password;

    //Initialize page object class
    LoginPage login = new LoginPage(driver);
    AdvancedSearchPage advancedsearch = new AdvancedSearchPage(driver);
    MainPage main = new MainPage(driver);

    @Before
    public void loginToApp() throws IOException, InterruptedException, AWTException {

        properties.load(new FileInputStream(testData));
        companyID = properties.getProperty("companyID");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        String title = driver.getTitle();

        login.setCompanyID(companyID);
        login.setUsername(username);
        login.setPassword(password);
        Thread.sleep(6000);
        waitforPageLoaded();

        try {
            if (title.equals("Neptune")) {
                Assert.assertTrue("Neptune BSG website is successfully displayed!",true);
                System.out.println("Neptune BSG website is successfully displayed!");
            }
        }catch (Exception e){

                Assert.fail("Environment is not accessible!");
                System.out.println("Environment is not accessible!");
        }

    }

    @And("User sign out successfully")
    public void userSignOutSuccessfully() throws IOException, InterruptedException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        login.selectFromMainMenu();
        login.clickLogout();

    }

    @Then("User clicks on fit to screen button")
    public void user_clicks_fit_to_screen_button() throws InterruptedException {

        main.clickFitToScreen();


    }

    @Then("User clicks on expand button")
    public void user_clicks_expand_button() throws InterruptedException {

        main.clickExpandScreen();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Security ID")
    public void verifyIfSearchParameterIsDisplayedInDashboard(String value) throws InterruptedException, IOException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        main.verifyDataONGrid();
        main.verifyGrid_secID(value);
        takeScreenshot(driver);


    }

    @Then("verify if search parameter {} {} are displayed from Quick Search in Dashboard - Security ID")
    public void verify_if_search_parameter_are_displayed_from_Quick_Search_in_Dashboard_Security_ID(String value1, String value2) throws IOException {

        main.verifyGrid_MultipleSecID(value1, value2);
        takeScreenshot(driver);

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Dealer")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Dealer(String value) throws IOException {

        //main.verifyDataONGrid();
        main.verifyGrid_Dealer(value);

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Side")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Side(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_Side(value);
        takeScreenshot(driver);

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Currency")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Currency(String value) throws IOException, InterruptedException {

        main.clickExpand();
        main.verifyDataONGrid();
        main.verifyGrid_CCY(value);
        takeScreenshot(driver);

    }

    @Then("verify if multiple search parameters {} {} are displayed in Dashboard - Currency")
    public void verify_if_multiple_search_parameters_are_displayed_in_Dashboard_Currency(String value1,String value2) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_MultipleCCy(value1,value2);
        takeScreenshot(driver);

    }

    @Then("verify if search parameters {} {} are displayed from Quick Search in Dashboard - Entity Name")
    public void verify_if_search_parameters_are_displayed_from_Quick_Search_in_Dashboard_Entity_Name(String value1,String value2) throws InterruptedException, IOException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_MultipleEntityName(value1,value2);
        takeScreenshot(driver);

    }

    @Then("User select indication and validate Intraday history - Timestamp and Amount")
    public void user_select_indication_and_validate_Intraday_history_Timestamp_and_Amount() {

        main.selectIndication();
        main.selectIntradayHistory();
        main.validateIntraday();
    }

    @Then("verify if search parameter is displayed in Recent Search as {}")
    public void verifyIfSearchParameterIsDisplayedInRecentSearchAs(String query) throws InterruptedException, IOException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        main.verifyRecentSearch(query);
    }

    @Then("User should see the dashboard - {string} page")
    public void user_should_see_the_dashboard_page(String title) throws IOException {

        login.verifyTitle(title);
        takeScreenshot(driver);
        waitforPageLoaded();


    }

    @Given("User navigate to the login page")
    public void iNavigateToTheLoginPage() throws Throwable {

    }

    @When("User enters the login credentials - {}, {}, {}")
    public void iEnterTheFollowingForLogin(String companyID,String username,String password) throws Throwable {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);


        login.setCompanyID(companyID);
        login.setUsername(username);
        login.setPassword(password);
    }

    @And("User clicks the login button")
    public void iClickLoginButton() throws Throwable {

        //login.clickSignIn();

    }

    @When("User select indication and validate market depth load")
    public void user_select_indication_and_validate_market_depth_load() {

        main.selectIndication();

    }

    @Then("verify if search parameter {} is displayed in Dashboard -  CCY")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_CCY(String value) throws InterruptedException, IOException {

        main.clickExpand();
        //main.checkMidLoop();
        main.verifyDataONGrid();
        main.verifyGrid_CCY(value);
        main.clickExpand();

    }

    @Then("verify if multiple search parameter {} {} is displayed in Dashboard -  CCY")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_CCY(String value1, String value2) throws InterruptedException { driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        main.clickExpand();
        main.checkMidLoop();
        main.verifyGrid_MultipleCCy(value1, value2);
        main.clickExpand();

    }

    @Then("verify if search parameter {} is displayed in Dashboard -  CXL")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_CXL(String value) throws IOException {

        main.verifyDataONGrid();
        main.sortStatus();
        main.verifyGrid_Status(value);
        takeScreenshot(driver);

    }

    @Then("verify if search parameter {} is displayed in Market Depth - CXL")
    public void verify_if_search_parameter_is_displayed_in_Market_Depth_CXL(String value) {

        main.selectIndication();
        main.uncheckCXL_MarketDepth();
        main.checkStat_MD(value);

    }

    @Then("verify if search parameter {} is displayed in Intraday History - CXL")
    public void verify_if_search_parameter_is_displayed_in_Intraday_History_CXL(String value) {

        main.selectIndication();
        main.selectIntradayHistory();
        main.setExclCancelsChkBox_Intraday();
        main.checkStatus_IntradayHistory(value);

    }
    @Then("verify if side {} is displayed in Dashboard")
    public void verify_if_side_is_displayed_in_Dashboard(String value) {


        main.verifyGrid_SIDE(value);

    }

    @Then("verify if type {} is displayed in Dashboard")
    public void verify_if_type_is_displayed_in_Dashboard(String value) {

            main.verifyGrid_Type(value);

    }


    @Then("User update the password - {} and {}")
    public void userUpdateThePassword(String old_value,String new_value) {

        login.updatePassword(old_value,new_value);



    }

    @Then("User login using the updated password - {}, {}, {}")
    public void userLoginUsingTheUpdatedPassword(String companyID,String username,String new_value) throws AWTException {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);


        login.setCompanyID(companyID);
        login.setUsername(username);
        login.setPassword(new_value);
        waitforPageLoaded();

    }

    @And("User select change password the main menu")
    public void userSelectChangePasswordTheMainMenu() throws InterruptedException {

        login.selectFromMainMenu();
        login.clickChangePassword();

    }

    @When("User enters the login credentials")
    public void userEntersTheLoginCredentials() {

    }
}
