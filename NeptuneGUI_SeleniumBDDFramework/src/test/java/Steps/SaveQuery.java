package Steps;

import Base.BaseUtil;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearchPage;
import pageObjects.MainPage;
import pageObjects.QuickSearchPage;
import pageObjects.SaveQueryPage;

import java.awt.*;
import java.io.IOException;

public class SaveQuery extends BaseUtil {

    //initialize page objects
    SaveQueryPage query = new SaveQueryPage(driver);
    MainPage main = new MainPage(driver);
    AdvancedSearchPage advancesearch = new AdvancedSearchPage(driver);
    QuickSearchPage quicksearch = new QuickSearchPage(driver);

    public SaveQuery() throws AWTException {


    }


    @Then("verify if saved query Message is displayed")
    public void verifyIfSavedQueryMessageIsDisplayed() {
    }

    @Then("verify if saved query {} is saved in My Queries under Saved Queries - Dashboard")
    public void verifyIfSavedQueryIsSavedInMyQueriesUnderSavedQueriesDashboard(String queryName) throws InterruptedException {

        //query.verifySavedQueryData_DataTable(queryName);
        query.selectFromSavedQuery_DataTable(queryName);

        
    }

    @And("User save a query {}")
    public void userASaveQuery(String queryName) throws IOException, InterruptedException {

        query.clickSaveQuery();
        query.saveQuery(queryName);
        //advancesearch.clearSearchResults();

        //query.verifyMyQueries(queryName);

    }

    @And("User save a shared query {}")
    public void userSaveASharedQueryQueryName(String queryName) throws IOException {

        //query.clickSaveQuery();
        query.clickSaveQuery();
        query.saveASharedQuery(queryName);

    }

    @Then("verify if saved shared query {} is saved in My Queries under Saved Queries - Dashboard")
    public void verifyIfSavedSharedQueryQueryNameIsSavedInMyQueriesUnderSavedQueriesDashboard(String queryName) {

        query.verifySharedSavedQuery_DataTable(queryName);
    }

    @Then("verify if results {} are displayed based on the filter used by the query")
    public void verifyIfResultsAreDisplayedBasedOnTheFilterUsedByTheQuery(String secID) throws IOException, InterruptedException {

        main.verifyGrid_secID(secID);
        advancesearch.getValueFromSecIDField(secID);
        
    }
    @And("User select the saved query {} in My Queries under Saved Queries")
    public void userSelectTheSavedQueryQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.selectFromSavedQuery(queryName);


    }

    @And("User select the second saved query {} in My Queries under Saved Queries")
    public void userSelectTheSecondSavedQueryQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.selectFromSecondQuery(queryName);


    }

    @And("User select the third saved query {} in My Queries under Saved Queries")
    public void userSelectTheThirdSavedQueryQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.selectFromThirdQuery(queryName);


    }

    @And("User select the saved queries {} {} in My Queries under Saved Queries")
    public void userSelectTheSavedQueriesQueryNameInMyQueriesUnderSavedQueries(String queryName1,String queryName2) {

        query.selectFromSavedQuery(queryName1);
        query.selectFromSavedQuery(queryName2);


    }

    @And("User share one query {} in My Queries under Saved Queries")
    public void userShareOneQueryInMyQueriesUnderSavedQueries(String queryName) {

        query.setQueryAsShared(queryName);
        query.clickEllipsisMenu();


    }

    @And("User share multiple queries {} {} in My Queries under Saved Queries")
    public void userShareMultipleQueriesQueryNameQueryNameInMyQueriesUnderSavedQueries(String queryName1,String queryName2) {

        query.setAsShared_MultipleQueries(queryName1, queryName2);

    }

    @And("User share second query {} in My Queries under Saved Queries")
    public void userShareSecondQueryQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.setSecondQueryAsShared(queryName);
        query.clickEllipsisMenu();
        
    }

    @And("User share third query {} in My Queries under Saved Queries")
    public void userShareThirdQueryQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.setThirdQueryAsShared(queryName);
        query.clickEllipsisMenu();
    }

    @And("User delete fourth query {} in My Queries under Saved Queries")
    public void userDeleteFourthQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.deleteFourthQuery(queryName);
        query.clickEllipsisMenu();

    }

    @And("User delete second query {} in My Queries under Saved Queries")
    public void userDeleteSecondQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.deleteSecondQuery(queryName);
        query.clickEllipsisMenu();

    }

    @And("User delete third query {} in My Queries under Saved Queries")
    public void userDeleteThirdQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.deleteThirdQuery(queryName);
    }

    @And("User delete one query {} in My Queries under Saved Queries")
    public void userDeleteOneQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.deleteQuery(queryName);
        query.clickEllipsisMenu();

    }

    @And("User select the fourth saved query {} in My Queries under Saved Queries")
    public void userSelectTheFourthSavedQueryNameInMyQueriesUnderSavedQueries(String queryName) {

        query.selectFromFourthQuery(queryName);


    }


}
