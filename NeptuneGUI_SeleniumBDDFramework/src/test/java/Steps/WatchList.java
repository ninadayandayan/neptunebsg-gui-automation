package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.*;

import java.awt.*;
import java.io.IOException;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class WatchList extends BaseUtil {

    //Initialize page object class
    LoginPage login = new LoginPage(driver);
    AdvancedSearchPage advancedsearch = new AdvancedSearchPage(driver);
    MainPage main = new MainPage(driver);
    WatchListPage watchList = new WatchListPage(driver);
    SaveQueryPage query = new SaveQueryPage(driver);


    @Then("verify if watchlist tab is loaded and securities are unchecked")
    public void verify_if_watchlist_tab_is_loaded_and_securities_are_unchecked() {

        waitforPageLoaded();
        watchList.verifyWatchListMessage();
        watchList.verifyTitlePage();


    }

    @Then("verify if securities are added to watchlist")
    public void securities_are_added_to_watchlist() throws InterruptedException {

        watchList.verifyAddedSecuritiesWatchList();
        query.clickEllipsisMenu();


    }

    @Given("User clicks on Watch List tab")
    public void user_clicks_on_Watch_List_tab() {

        waitforPageLoaded();
        watchList.clickWatchList();
        watchList.verifyTitlePage();
        waitforPageLoaded();
    }

    @Given("select multiple securities by checking the checkbox and click delete")
    public void select_security_by_checking_the_checkbox_and_click_delete() {

        watchList.checkSecurities_FromWatchList();
        watchList.removeSecurities();

    }

    @Then("verify if securities were removed to watchlist")
    public void verify_if_securities_were_removed_to_watchlist() {

        watchList.verifyWatchListMessage();
        watchList.verifyRemovedSecuritiesWatchList();

    }


    @Given("select security by checking the checkbox and press add")
    public void select_security_by_checking_the_checkbox_and_press_add() {

        watchList.checkSecurities_FromDashboard();
        watchList.addSecurities();

    }

    @Then("verify if dialog box shows up with the message: {}")
    public void verify_if_dialog_box_shows_up_with_the_message(String string) {


    }

    @Given("User enter partial sec ID {} on Security ID field in Watch List")
    public void user_enter_partial_sec_ID_on_Security_ID_field_in_Watch_List(String value) {

        watchList.enterSecID(value);

    }

    @Then("verify if watch list tab automatically load up securities {} that matches the criteria")
    public void verify_if_watch_list_tab_automatically_load_up_securities_that_matches_the_criteria(String value) throws IOException, InterruptedException {

        main.verifyDataONGrid();
        main.verifyGrid_secID(value);

    }

    @Then("verify if watch list tab automatically load up securities {} for Security Name filter")
    public void verify_if_watch_list_tab_automatically_load_up_securities_for_Security_Name_filter(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_secName(value);
    }

    @Then("User enter multiple sec ID {} {} on Security ID and verify automatic load of securities")
    public void
    user_enter_multiple_sec_ID_on_sec_ID_and_verify_automatic_load_of_securities(String value1, String value2) throws IOException, InterruptedException {

        watchList.enterSecID(value1);
        waitforPageLoaded();
        main.verifyDataONGrid();
        main.verifyGrid_secID(value1);
        watchList.enterSecID(value2);
        waitforPageLoaded();
        main.verifyDataONGrid();
        main.verifyGridSEC_ID(value2);

    }

    @Given("User enter sec name {} on Security Name field in Watch List")
    public void user_enter_sec_name_on_Security_Name_field_in_Watch_List(String value) {

        watchList.enterSecName(value);


    }

    @Given("User enter multiple sec ID {} {} on Security ID field in Watch List")
    public void user_enter_multiple_sec_ID_on_Security_ID_field_in_Watch_List(String value1, String value2) throws IOException, InterruptedException {

        watchList.enterSecID(value1);
        main.verifyGrid_secID(value1);
        watchList.enterSecID(value2);
        main.verifyGrid_secID(value2);

    }

    @Given("User enter entity name {} on Entity Name field in Watch List")
    public void user_enter_entity_name_on_Entity_Name_field_in_Watch_List(String value) {


        watchList.enterEntityName(value);

    }

    @Then("verify if watch list tab automatically load up securities {} for Entity Name filter")
    public void verify_if_watch_list_tab_automatically_load_up_securities_Entity_Name_filter(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_EntityName(value);

    }


    @Then("verify if watch list tab automatically load up securities {} for Side filter")
    public void verify_If_Watch_List_Tab_Automatically_Load_Up_Securities_Value_For_Side_Filter(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_SIDE(value);
    }

    @And("User select SIDE {} from SIDE drop down list - WatchList")
    public void userSelectSIDEValueFromSIDEDropDownListWatchList(String value) throws AWTException {

        watchList.selectFromSide(value);
        waitforPageLoaded();
    }

    @And("User select TYPE {} from TYPE drop down list - WatchList")
    public void userSelectTYPEValueFromTypeDropDownListWatchList(String value) throws AWTException {

        watchList.selectFromType(value);
        waitforPageLoaded();

    }

    @Then("verify if watch list tab automatically load up securities {} for Type filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForTypeFilter(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_Type(value);


    }

    @And("user select ccy {} on ccy dropdown list - Watch List")
    public void userSelectCcyOnCcyDropdownListWatchList(String value) {

        //query.clickEllipsisMenu();
        watchList.selectCCY(value);
        waitforPageLoaded();

    }

    @And("user select multiple ccy {} on ccy dropdown list - Watch List")
    public void userSelectmultipleCcyOnCcyDropdownListWatchList(String value) {

        watchList.selectCCY(value);
        waitforPageLoaded();

    }

    @Then("verify if watch list tab automatically load up securities {} for CCY filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForCCYFilter(String value1) throws IOException {

        watchList.clickExpandBtn();
        main.verifyDataONGrid();
        watchList.clickExpandBtn();
        main.verifyGrid_CCY(value1);
        watchList.clickExpandBtn();

    }


    @Then("verify if watch list tab automatically load up multiple securities {} {} for CCY filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForCCYFilter(String value1, String value2) throws IOException {

        watchList.clickExpandBtn();
        main.verifyDataONGrid();
        watchList.clickExpandBtn();
        main.verifyGrid_MultipleCCy(value1, value2);
        watchList.clickExpandBtn();
    }

    @Given("user select sector {} on sector dropdown list - Watch List")
    public void user_select_sector_Single_on_sector_dropdown_list_Watch_List(String value) {

        watchList.selectSector(value);
        //driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);

    }

    @Then("verify if watch list tab automatically load up securities {} for Sector filter")
    public void verify_if_watch_list_tab_automatically_load_up_securities_Single_for_Sector_filter(String value1) throws IOException {

        watchList.clickExpandBtn();
        main.checkMidLoop();
        main.verifyDataONGrid();
        main.verifyGrid_Sector(value1);
        watchList.clickExpandBtn();

    }

    @Given("user select multiple sector {} on sector dropdown list - Watch List")
    public void user_select_multiple_sector_on_sector_dropdown_list_Watch_List(String value) {

        watchList.selectSector(value);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Sector filter")
    public void verify_if_watch_list_tab_automatically_load_up_multiple_securities_for_Sector_filter(String value1, String value2) throws IOException {

        main.verifyDataONGrid();
        watchList.clickExpandBtn();
        main.checkLoop();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        main.verifyGrid_MultipleSector(value1, value2);
        watchList.clickExpandBtn();

    }

    @And("user select region {} on region dropdown list - Watch List")
    public void userSelectRegionValueOnRegionDropdownListWatchList(String value) {

        watchList.selectRegion(value);


    }

    @Then("verify if watch list tab automatically load up securities {} for Region filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForRegionFilter(String value1) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_Region(value1);
        watchList.clickExpandBtn();
    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Region filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForRegionFilter(String value1, String value2) throws IOException {

        main.verifyDataONGrid();
        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyGrid_MultipleRegion(value1, value2);
        watchList.clickExpandBtn();

    }

    @And("user select ctry of issuer {} {} on ctry of issuer dropdown list - Watch List")
    public void userSelectCtryOfIssuerValueOnRegionDropdownListWatchList(String value, String country_code) {

        watchList.selectCtryofIssuer(value, country_code);

    }

    @Then("verify if watch list tab automatically load up securities {} for Ctry.of Issuer filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForCtryOfIssuerFilter(String value) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_CtryIssuer(value);
        watchList.clickExpandBtn();

    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Ctry.of Issuer filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForCtryOfIssuerFilter(String value1, String value2) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_MultipleCtryIssuer(value2, value1);
        watchList.clickExpandBtn();

    }

    @Then("verify if watch list tab automatically load up securities {} for Ctry.of Risk filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForCtryOfRiskFilter(String value1) throws IOException {

        main.verifyDataONGrid();
        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyGrid_CtryRisk(value1);
        watchList.clickExpandBtn();

    }

    @And("user select ctry of issuer {} {} on ctry of risk dropdown list - Watch List")
    public void userSelectCtryOfIssuerValueCountry_codeOnCtryOfRiskDropdownListWatchList(String value, String country_code) {

        watchList.selectCtryofRisk(value, country_code);
    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Ctry.of Risk filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForCtryOfRiskFilter(String value1, String value2) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_MultipleCtryofRisk(value1, value2);
        watchList.clickExpandBtn();
    }

    @And("user select ctry of risk {} {} on ctry of risk dropdown list - Watch List")
    public void userSelectCtryOfRiskValueCountry_codeOnCtryOfRiskDropdownListWatchList(String value, String country_code) {

        watchList.selectCtryofRisk(value, country_code);

    }

    @Then("verify if watch list tab automatically load up securities {} for Asset Class filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForAssetClassFilter(String value1) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_AssetClass(value1);
        watchList.clickExpandBtn();
        waitforPageLoaded();

    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Asset Class filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForAssetClassFilter(String value1, String value2) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_MultipleAssetClass(value1,value2);
        watchList.clickExpandBtn();
        waitforPageLoaded();

    }

    @And("user select asset class {} on asset class dropdown list - Watch List")
    public void userSelectAssetClassValueOnAssetClassDropdownListWatchList(String value) {

        watchList.selectAssetClass(value);
        waitforPageLoaded();


    }

    @Then("verify if watch list tab automatically load up securities {} for Seniority filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForSeniorityFilter(String value1) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_Seniority(value1);
        watchList.clickExpandBtn();

    }

    @And("user select seniority {} on seniority dropdown list - Watch List")
    public void userSelectSeniorityValueOnSeniorityDropdownListWatchList(String value) {

        watchList.selectSeniority(value);

    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Seniority filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForSeniorityFilter(String value1, String value2) throws IOException {

        watchList.clickExpandBtn();
        main.checkLoop();
        main.verifyDataONGrid();
        main.verifyGrid_MultipleSeniority(value1, value2);
        watchList.clickExpandBtn();

    }

    @Then("verify if watch list tab automatically load up securities {} for Credit Rating filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForCreditRatingFilter(String value) {


    }

    @And("user select credit rating {} on credit rating dropdown list - Watch List")
    public void userSelectCreditRatingValueOnCreditRatingDropdownListWatchList(String value) {


    }

    @Then("verify if watch list tab automatically load up multiple securities {} {} for Credit Rating filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpMultipleSecuritiesValueValueForCreditRatingFilter(String value1, String value2) {


    }

    @Then("verify if watch list tab automatically load up securities {} for EM filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForEMFilter(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_EM(value);
    }

    @And("User select EM {} from EM drop down list - WatchList")
    public void userSelectEMValueFromEMDropDownListWatchList(String value) {

        watchList.selectFromEM(value);
        waitforPageLoaded();
    }

    @Then("verify if watch list tab automatically load up securities {} for FRN filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForFRNFilter(String value) throws IOException {

        main.verifyDataONGrid();
        main.verifyGrid_FRN(value);

    }

    @And("User select FRN {} from FRN drop down list - WatchList")
    public void userSelectFRNValueFromFRNDropDownListWatchList(String value) {

        waitforPageLoaded();
        watchList.selectFromFRN(value);

    }

    @Then("verify if watch list tab automatically load up securities {} for Include Cancels filter")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForIncludeCancelsFilter(String value) throws IOException {

        main.verifyDataONGrid();
        main.sortStatus();
        main.verifyGrid_Status(value);


    }

    @And("User unchecked exclude cancels checkbox in Watch List")
    public void userUncheckedExcludeCancelsCheckboxInWatchList() {

        watchList.clickExcludeCancels();


    }

    @And("User clicks on clear button")
    public void userClicksOnClearButton() {


        watchList.clickClearBtn();

        
    }

    @Then("verify if watch list tab automatically load up securities for with default filters")
    public void verifyIfWatchListTabAutomaticallyLoadUpSecuritiesValueForWithDefaultFilters() {

        watchList.checkIfInputFieldIsEmpty();

    }

    @And("add security {} via Add Securities")
    public void addSecurityValueViaAddSecurities(String value) {

        watchList.addSecID_To_WatchList_Alert_Settings(value);
        
    }

    @Then("verify newly added security {} is visible on Watch List Table")
    public void verifyNewlyAddedSecurityIsVisibleOnWatchListTable(String value) throws IOException, InterruptedException {

        watchList.enterSecID(value);
        main.verifyGrid_secID(value);
        
    }

    @And("user clicks on Watch List Alert Settings")
    public void userClicksOnWatchListAlertSettings() {

        watchList.clickWatchListAlertSettings();


    }


    @Then("verify newly added security {}, {} is visible on Watch List Alert Settings table")
    public void verifyNewlyAddedSecurityValueIsVisibleOnWatchListAlertSettingsTable(String value,String secName) {

        watchList.saveChangesToWatchList(value,secName);
        
    }


    @Then("verify newly added security {}, {} toggle configuration")
    public void verifyNewlyAddedSecurityValueSecNameToggleConfiguration(String value,String secName) {

        watchList.verifySecurityToggleConfiguration(value, secName);



    }

    @And("add multiple securities {} {} via Add Securities")
    public void addMultipleSecuritiesValueViaAddSecurities(String secID1,String secID2) {

        watchList.addMultipleSecIDs_To_WatchListAlert_Settings(secID1, secID2);


        
    }

    @Then("verify newly added securities {} {} {} {} are visible on Watch List Alert Settings table")
    public void verifyNewlyAddedSecuritiesareVisibleOnWatchListAlertSettingsTable(String secID1,String secID2,String secName1,String secName2) throws InterruptedException {

        watchList.verifyMultipleSecurities_AlertTable(secID1, secID2);
        watchList.saveMultipleSecIDs_To_WatchList(secID1,secID2,secName1,secName2);
    }

    @Then("verify newly added securities {},{} are visible on Watch List Table")
    public void verifyNewlyAddedSecuritiesareVisibleOnWatchListTable(String secID1,String secID2) throws IOException, InterruptedException {

        watchList.enterSecID(secID1);
        main.verifyGrid_secID(secID1);
        waitforPageLoaded();
        watchList.enterSecID(secID2);
        main.verifyGrid_secID(secID2);

    }

    @Then("verify newly added securities {} {} Security Toggle Configuration")
    public void verifyNewlyAddedSecuritiesToggleConfiguration(String secID1,String secID2) throws InterruptedException {

        watchList.verifyMultipleSecurities_SecurityToggleConfiguration(secID1, secID2);



    }

    @Then("verify if security {} has been removed on Watch List Alert Settings table")
    public void verifyIfSecurityHasBeenRemovedOnWatchListAlertSettingsTable() {
    }

    @Then("verify if security {} has been removed on Watch List Table")
    public void verifyIfSecurityHasBeenRemovedOnWatchListTable() {
    }

    @And("remove security {} via Add Securities")
    public void removeSecurityViaAddSecurities() {




    }
}
