package Steps;

import Base.BaseUtil;
import org.openqa.selenium.WebDriver;
import pageObjects.LoginPage;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class BaseTest extends BaseUtil {

    public Properties properties = new Properties();
    public File testData = new File("src/main/resources/Configuration.properties");
    public String companyID,username,password;

    public BaseTest(WebDriver driver) {
        BaseUtil.driver = driver;
    }

    //Initialize page object class
    LoginPage login = new LoginPage(driver);


    //@Before
    public void loginToApp() throws IOException, AWTException {
        properties.load(new FileInputStream(testData));
        companyID = properties.getProperty("companyID");
        username = properties.getProperty("username");
        password  = properties.getProperty("password");

        login.setCompanyID(companyID);
        login.setUsername(username);
        login.setPassword(password);
        login.clickSignIn();
        waitforPageLoaded();

    }

}
