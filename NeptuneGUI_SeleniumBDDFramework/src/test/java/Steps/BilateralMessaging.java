package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

public class BilateralMessaging extends BaseUtil {

    //Initialize page objects:
    pageObjects.BilateralMessaging bm = new pageObjects.BilateralMessaging(driver);

    @When("User clicks on Messaging History Tab")
    public void userClicksOnMessagingHistoryTab() {

        bm.navigateToBM();
        waitforPageLoaded();
    }

    @And("Validate Message displayed upon page load")
    public void validateMessageDisplayedUponPageLoad() {

        bm.validateHeader("Message History");
        bm.validateMessage("No negotiations found.");
    }
}
