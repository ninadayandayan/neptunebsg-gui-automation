package Base;

import io.cucumber.java.Scenario;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public class BaseUtil {

    public File testData = new File("src/main/resources/Configuration.properties");
    public static Properties properties = new Properties();
    public static File screenCaptures;

    public static WebDriver driver;
    public static WebDriverWait wait;

    public void waitforPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(12000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }

    }

    public void createDirectory() throws IOException {

        properties.load(new FileInputStream(testData));

        String screenshots = properties.getProperty("screenshotDir");
        screenCaptures = new File(screenshots);

        if (!screenCaptures.exists()) {
            screenCaptures.mkdirs();
            System.out.println(screenCaptures + " directory created.");
        } else {
            System.out.println("Directory already exists!");
        }

    }

    public static void takeScreenshot(WebDriver driver) throws IOException {

        try {
            //Convert web driver object to Takescreenshot
            TakesScreenshot screenshot = ((TakesScreenshot) (driver));

            //Call getScreenshot as method to create image file
            File scrFile = screenshot.getScreenshotAs(OutputType.FILE);

            // Open the current date and time
            String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());

            //move image file to new destination
            FileHandler.copy(scrFile, new File(screenCaptures + "/" + timestamp + ".png"));
            System.out.println("Screenshot Taken");

        } catch (Exception e) {
            System.out.println("Exception While Taking Screenshot " + e.getMessage());
        }

    }


    public boolean isElementPresent(By locator) {

        try {

            driver.findElement(locator);
            return true;

        } catch (org.openqa.selenium.NoSuchElementException e) {

            return false;

        }


    }
    public boolean isElementVisible(String cssLocator) {
        return driver.findElement(By.cssSelector(cssLocator)).isDisplayed();
    }


}


