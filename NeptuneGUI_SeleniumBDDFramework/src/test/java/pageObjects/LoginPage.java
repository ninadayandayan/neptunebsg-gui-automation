package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;

public class LoginPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    Actions action = new Actions(driver);

    //Elements:
    @FindBy(xpath = "//input[@id='sign-in-company']")

    public WebElement txtCompany;

    @FindBy(xpath = "//input[@id='sign-in-username']")

    public WebElement txtUsername;

    @FindBy(xpath = "//input[@id='sign-in-password']")

    public WebElement txtPassword;

    @FindBy(xpath = "//button[text()='Sign in']")

    public WebElement btnSignIn;

    @FindBy(xpath = "//li[@class='dropdown dropdown-usermenu']/a")

    public WebElement btnMainMenu;

    @FindBy(xpath = "//li/a[@id='logout-link']")

    WebElement btnSignOut;

    @FindBy(xpath = "//*[@id='settings-changepassword-link']")

    WebElement changePasswordBtn;

    @FindBy(xpath = "//*[@id='old-password']")

    WebElement oldPasswordInputField;

    @FindBy(xpath = "//*[@id='new-password']")

    WebElement newPasswordInputField;

    @FindBy(xpath = "//*[@id='confirm-password']")

    WebElement confirmPasswordInputField;

    @FindBy(xpath = "//*[@id='submit-changepassword']")

    WebElement changeBtn;

    @FindBy(xpath = "//*[@id='messages-passwordchangesuccess']")

    WebElement successMessage;

    public LoginPage(WebDriver driver) {
        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    public void setCompanyID(String companyID) {
        //wait.until(ExpectedConditions.elementToBeClickable(txtCompany));
        //js.executeScript("document.getElementById('sign-in-company').value='DEV_BUY_1';");
        txtCompany.clear();
        txtCompany.sendKeys(companyID);
        //System.out.println("Entered Company ID: " + companyID);
    }

    public void setUsername(String username) {
        //wait.until(ExpectedConditions.elementToBeClickable(txtUsername));
        txtUsername.clear();
        txtUsername.sendKeys(username);
        //System.out.println("Entered Username is:" + username);
    }

    public void setPassword(String password) throws AWTException {
        //wait.until(ExpectedConditions.elementToBeClickable(txtPassword));
        txtPassword.clear();
        txtPassword.sendKeys(password);
        //press enter from keyboard
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);


        //System.out.println("Entered password");
    }

    public void clickSignIn()  {
        wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnSignIn);
        btnSignIn.click();
        System.out.println("Triggered Sign in button");
    }

    public void selectFromMainMenu() throws InterruptedException {
        //wait.until(ExpectedConditions.elementToBeClickable(btnMainMenu));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnMainMenu);
        //btnMainMenu.click();
        action.doubleClick(btnMainMenu);
        Thread.sleep(6000);
        System.out.println("Triggered Main Menu");
    }

    public void clickLogout() {
        wait.until(ExpectedConditions.elementToBeClickable(btnSignOut));

        try {
            if(btnSignOut.isDisplayed()){
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnSignOut);
                action.doubleClick(btnSignOut);
                //btnSignOut.click();
                System.out.println("Triggered sign out button");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void clickChangePassword(){
        wait.until(ExpectedConditions.elementToBeClickable(changePasswordBtn));

        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", changePasswordBtn);
        //action.doubleClick(changePasswordBtn);
        //changePasswordBtn.click();
        System.out.println("Triggered change password from main menu");
    }

    public void updatePassword(String old_pw,String new_pw){

        wait.until(ExpectedConditions.visibilityOf(oldPasswordInputField));
        oldPasswordInputField.sendKeys(old_pw);
        System.out.println("Entered old password");

        newPasswordInputField.sendKeys(new_pw);
        System.out.println("Entered new password");

        //enter new password:
        confirmPasswordInputField.sendKeys(new_pw);
        System.out.println("Entered new password");

        //click change
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", changeBtn);
        action.doubleClick(changeBtn);
        //changeBtn.click();

        System.out.println("Triggered change button...");

        try {
            if(successMessage.isDisplayed()){
                Assert.assertTrue("Password successfully changed. You may now login using your new password.",true);
                System.out.println("Password successfully changed. You may now login using your new password.");

            }
        }catch (Exception e){

            Assert.fail("Password update failed!");
            System.out.println("Password update failed!");

        }

    }

    public void verifyTitle(String title) {

        String actualTitle = driver.getTitle();

        if (title.equals(actualTitle)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Dashboard Page is successfully displayed" + "\n"+ "Title displayed is: " + actualTitle);
        } else {
            Assert.fail("Page did not load!");
            System.out.println("Page did not load!");
        }

    }
}
