package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class SaveQueryPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    public Actions action = new Actions(driver);
    //elements:
    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[1]/button")
    WebElement savequery;

    @FindBy(xpath = "//*[@id='saveQueryModal']//*[contains(text(),'Save Query')]")
    WebElement saveQueryHeader;

    @FindBy(xpath = "//*[@id='save-query-title']")
    WebElement saveQueryInputField;

    @FindBy(xpath = "//*[@id='submit-savequery']")
    WebElement saveBtn;

    @FindBy(xpath = "//*[@id='save-query-share']")
    WebElement shareBtn;

    @FindBy(xpath = "//*[@id='cancel-savequery']")
    WebElement cancelBtn;

    @FindBy(xpath = "//*[@id='generic-modal-body-text']")
    WebElement querySavedMessage;

    @FindBy(xpath = "//*[@id='generic-modal']/div/div/div[3]/button")
    //*[@id='generic-modal']/div/div/div[3]/button
            WebElement okBtn;

    @FindBy(xpath = "//*[@id='my-query-table']")
    WebElement saveQueriesTable;

    @FindBy(xpath = " //*[contains(@class,'ui-widget-content slick-row')]//div/a[@class='view-saved-query']")
    WebElement viewSavedQuery;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[2]/div[2]/a")
    WebElement viewSavedQuery2;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[3]/div[2]/a")
    WebElement viewSavedQuery3;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[4]/div[2]/a")
    WebElement viewSavedQuery4;


    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[1]/div[2]/div/span[1]")
    WebElement firstQuery;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[2]/div[2]/div/span[1]")

    WebElement secondQuery;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[3]/div[2]/div/span[1]")
    WebElement thirdQuery;

    @FindBy(xpath = "//*[@id='submit-share-savequery']")
    WebElement submitShareBtn;

    @FindBy(xpath = "//*[@id='cancel-share-savequery']")
    WebElement cancelShareBtn;

    @FindBy(xpath = "//*[@id='submit-delete-savequery']")
    WebElement deleteQueryBtn;

    @FindBy(xpath = "//*[@id='header-session-items']/ul/li[3]/a")
    WebElement ellipsisMenu;

    @FindBy(xpath = "//*[@id='mysavedquery-widget']/span[@class='saved-queries-title']")
    WebElement saveQueriesHeader;

    @FindBy(xpath = "//*[@id='my-query-table']//*[@title='Shared']")
    WebElement sharedQueryIndicator;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[1]/div[2]/div/span[1]/i")
    WebElement shareIconWE1;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[2]/div[2]/div/span[1]/i")
    WebElement shareIconWE2;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[3]/div[2]/div/span[1]/i")
    WebElement shareIconWE3;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[1]/div[2]/div/span[2]/i")
    WebElement deleteBtn1;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[2]/div[2]/div/span[2]/i")
    WebElement deleteBtn2;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[3]/div[2]/div/span[2]/i")
    WebElement deleteBtn3;

    @FindBy(xpath = "//*[@id='my-query-table']/div[5]/div/div[4]/div[2]/div/span[2]/i")
    WebElement deleteBtn4;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1 cell-title savedquery')]/a")
    WebElement savedQuery_DataTableWE;

    By saveQueryRow = By.xpath(" //*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'view-saved-query')]");
    By viewSavedQ = By.xpath("//*[@id='my-query-table']//*[@class='view-saved-query']");
    By savedQuery_DataTable = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1 cell-title savedquery')]/a");
    By shareIcon1 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[1]/div[2]/div/span[1]/i");
    By shareIcon2 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[2]/div[2]/div/span[1]/i");
    By shareIcon3 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[3]/div[2]/div/span[1]/i");
    By deleteIcon1 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[1]/div[2]/div/span[2]/i");
    By deleteIcon2 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[2]/div[2]/div/span[2]/i");
    By deleteIcon3 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[3]/div[2]/div/span[2]/i");
    By deleteIcon4 = By.xpath("//*[@id='my-query-table']/div[5]/div/div[4]/div[2]/div/span[2]/i");

    public SaveQueryPage(WebDriver driver) {
        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 30);

        PageFactory.initElements(driver, this);

    }

    //functions/methods:
    public void clickSaveQuery() {

        wait.until(ExpectedConditions.elementToBeClickable(savequery));

        try {
            if(savequery.isDisplayed()){
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", savequery);
                action.click(savequery);
                System.out.println("Triggered save query button");
                waitforPageLoaded();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
   }

    public void clickEllipsisMenu() {

        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", ellipsisMenu);
        action.click(ellipsisMenu);

        System.out.println("Triggered ellipsis menu for Saved Query");

        if (saveQueriesHeader.isDisplayed()) {
            Assert.assertTrue("Dashboard - Saved Queries is displayed", true);
            System.out.println("Dashboard - Saved Queries is displayed");
        }

    }

    public void saveASharedQuery(String queryName) throws IOException {

        wait.until(ExpectedConditions.elementToBeClickable(saveQueryInputField));
        saveQueryInputField.sendKeys(queryName);
        System.out.println("Entered save query name is: " + queryName);

        //Click share
        if(shareBtn.isDisplayed()){
            System.out.println(wait.until(ExpectedConditions.visibilityOf(shareBtn)));
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", shareBtn);
            action.click(shareBtn);
            System.out.println("Triggered share button...");

            //Click save button
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", saveBtn);
            action.click(saveBtn);
            System.out.println("Triggered save button");

            try {
                //Verify message:
                if (querySavedMessage.getText().equalsIgnoreCase("Query saved.")) {
                    Assert.assertTrue("Query saved.", true);
                    System.out.println("Query saved.");

                    //Click ok button
                    wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                    try {
                        if (okBtn.getAttribute("aria-label").equals("Close")) {

                            //js.executeScript("arguments[0].click;", okBtn);
                            action.doubleClick(okBtn);
                            //okBtn.click();
                            System.out.println("Triggered ok button");
                            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                        }
                    } catch (Exception e) {

                        System.out.println("Can't click element");
                    }
                }
            } catch (Exception e) {

                Assert.fail("Unable to save query!");
                takeScreenshot(driver);
                System.out.println("Unable to save query!");

            }


        }

    }

    public void deleteQuery(String queryName) {

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery));

        try {
            if ((viewSavedQuery.getText().equals(queryName))) {

                //click share button
                if (isElementPresent(deleteIcon1)) {

                    Thread.sleep(1000);
                    deleteBtn1.click();
                    System.out.println("Triggered delete button");

                    wait.until(ExpectedConditions.elementToBeClickable(deleteQueryBtn));
                    deleteQueryBtn.click();
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Deleted...", true);
                        System.out.println("Query Deleted....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                    } else {
                        Assert.fail("Query not found!");
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }


    }

    public void deleteSecondQuery(String queryName) {

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery2));

        try {
            if ((viewSavedQuery2.getText().equals(queryName))) {

                //click share button
                if (isElementPresent(deleteIcon2)) {

                    Thread.sleep(1000);
                    deleteBtn2.click();
                    System.out.println("Triggered delete button");

                    wait.until(ExpectedConditions.elementToBeClickable(deleteQueryBtn));
                    deleteQueryBtn.click();
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Deleted...", true);
                        System.out.println("Query Deleted....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                    } else {
                        Assert.fail("Query not found!");
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }

    }

    public void deleteThirdQuery(String queryName) {

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery3));

        try {
            if ((viewSavedQuery3.getText().equals(queryName))) {

                //click share button
                if (isElementPresent(deleteIcon3)) {

                    Thread.sleep(1000);
                    deleteBtn3.click();
                    System.out.println("Triggered delete button");

                    wait.until(ExpectedConditions.elementToBeClickable(deleteQueryBtn));
                    deleteQueryBtn.click();
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Deleted...", true);
                        System.out.println("Query Deleted....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                    } else {
                        Assert.fail("Query not found!");
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }


    }

    public void deleteFourthQuery(String queryName) {
        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery4));

        try {
            if ((viewSavedQuery4.getText().equals(queryName))) {

                //click share button
                if (isElementPresent(deleteIcon4)) {

                    Thread.sleep(1000);
                    deleteBtn4.click();
                    System.out.println("Triggered delete button");

                    wait.until(ExpectedConditions.elementToBeClickable(deleteQueryBtn));
                    deleteQueryBtn.click();
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Deleted...", true);
                        System.out.println("Query Deleted....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                    } else {
                        Assert.fail("Query not found!");
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }


    }

    public void setQueryAsShared(String queryName) {

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery));
        //action.moveToElement(shareIconWE).build().perform();

        try {
            if ((viewSavedQuery.getText().equals(queryName))) {

                //click share button
                if (isElementPresent(shareIcon1)) {
                    Thread.sleep(1000);
                    //((JavascriptExecutor) driver).executeScript("arguments[0].click();", shareIconWE);
                    shareIconWE1.click();
                    System.out.println("Triggered share button");

                    wait.until(ExpectedConditions.elementToBeClickable(submitShareBtn));
                    submitShareBtn.click();
                    //js.executeScript("arguments[0].click;", submitShareBtn);
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Saved...", true);
                        System.out.println("Query Saved....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                        //verify shared indicator...
                        if (sharedQueryIndicator.isDisplayed()) {
                            Assert.assertTrue("Query shared successfully...", true);
                            System.out.println("Query shared successfully...");
                        } else {
                            Assert.fail("Can't share the query!");
                            System.out.println("Can't share the query!");
                        }
                    } else {
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }
    }

    public void setSecondQueryAsShared(String queryName) {

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery2));
        //action.moveToElement(shareIconWE).build().perform();

        try {
            if ((viewSavedQuery2.getText().equals(queryName))) {

                //click share button
                wait.until(ExpectedConditions.visibilityOf(shareIconWE2));
                System.out.println(wait.until(ExpectedConditions.visibilityOf(shareIconWE2)));
                if (isElementPresent(shareIcon2)) {
                    //Thread.sleep(1000);
                    Thread.sleep(1000);
                    //((JavascriptExecutor) driver).executeScript("arguments[0].click();", shareIconWE);
                    shareIconWE2.click();
                    System.out.println("Triggered share button");

                    wait.until(ExpectedConditions.elementToBeClickable(submitShareBtn));
                    submitShareBtn.click();
                    //js.executeScript("arguments[0].click;", submitShareBtn);
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Saved...", true);
                        System.out.println("Query Saved....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                        //verify shared indicator...
                        if (sharedQueryIndicator.isDisplayed()) {
                            Assert.assertTrue("Query shared successfully...", true);
                            System.out.println("Query shared successfully...");
                        } else {
                            Assert.fail("Can't share the query!");
                            System.out.println("Can't share the query!");
                        }
                    } else {
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }
    }

    public void setSharedQueries(String query1, String query2) {

        String[] query = new String[]{
                query1,
                query2
        };

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery));
        for (String queries : query) {


        }


    }

    public void setThirdQueryAsShared(String queryName) {

        try {
            if ((viewSavedQuery3.getText().equals(queryName))) {

                //click share button
                if (isElementPresent(shareIcon3)) {
                    Thread.sleep(1000);
                    //((JavascriptExecutor) driver).executeScript("arguments[0].click();", shareIconWE);
                    shareIconWE3.click();
                    System.out.println("Triggered share button");

                    wait.until(ExpectedConditions.elementToBeClickable(submitShareBtn));
                    submitShareBtn.click();
                    //js.executeScript("arguments[0].click;", submitShareBtn);
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Saved...", true);
                        System.out.println("Query Saved....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                        //verify shared indicator...
                        if (sharedQueryIndicator.isDisplayed()) {
                            Assert.assertTrue("Query shared successfully...", true);
                            System.out.println("Query shared successfully...");
                        } else {
                            Assert.fail("Can't share the query!");
                            System.out.println("Can't share the query!");
                        }
                    } else {
                        System.out.println("Query not found!");
                    }

                }


            }
        } catch (Exception e) {

            Assert.fail("Query did not matched!");
            System.out.println("Query did not matched!");
        }
    }


    public void setAsShared_MultipleQueries(String queryName1, String queryName2) {

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery));

        try {
            if (secondQuery.isDisplayed()) {
                secondQuery.click();
                System.out.println("Triggered share button");

                //Click yes button
                wait.until(ExpectedConditions.elementToBeClickable(submitShareBtn));
                js.executeScript("arguments[0].click;", submitShareBtn);
                submitShareBtn.click();
                //action.doubleClick(submitShareBtn);
                System.out.println("Triggered yes button");

                wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                if (querySavedMessage.isDisplayed()) {
                    Assert.assertTrue("Query Saved...", true);
                    System.out.println("Query Saved....");

                    //Click ok button
                    wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                    js.executeScript("arguments[0].click;", okBtn);
                    okBtn.click();
                    System.out.println("Triggered okay button...");

                    //verify shared indicator...
                    if (sharedQueryIndicator.isDisplayed()) {
                        Assert.assertTrue("Query shared successfully...", true);
                        System.out.println("Query shared successfully...");
                    } else {
                        Assert.fail("Can't share the query!");
                        System.out.println("Can't share the query!");
                    }
                }
                if (thirdQuery.isDisplayed()) {
                    thirdQuery.click();
                    System.out.println("Triggered share button");

                    //Click yes button
                    wait.until(ExpectedConditions.elementToBeClickable(submitShareBtn));
                    js.executeScript("arguments[0].click;", submitShareBtn);
                    submitShareBtn.click();
                    //action.doubleClick(submitShareBtn);
                    System.out.println("Triggered yes button");

                    wait.until(ExpectedConditions.visibilityOf(querySavedMessage));

                    if (querySavedMessage.isDisplayed()) {
                        Assert.assertTrue("Query Saved...", true);
                        System.out.println("Query Saved....");

                        //Click ok button
                        wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                        js.executeScript("arguments[0].click;", okBtn);
                        okBtn.click();
                        System.out.println("Triggered okay button...");

                        //verify shared indicator...
                        if (sharedQueryIndicator.isDisplayed()) {
                            Assert.assertTrue("Query shared successfully...", true);
                            System.out.println("Query shared successfully...");
                        } else {
                            Assert.fail("Can't share the query!");
                            System.out.println("Can't share the query!");
                        }
                    }

                }


            }
        } catch (Exception e) {
            System.out.println("Unable to click element!");
        }

    }

    public void selectFromSecondQuery(String queryName) {

        clickEllipsisMenu();
        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery2));
        try {
            System.out.println(viewSavedQuery2.getText().equals(queryName));
            System.out.println(viewSavedQuery2.getText());
            System.out.println(queryName);
            if (viewSavedQuery2.getAttribute("data-row").equalsIgnoreCase("1")) {
                js.executeScript("arguments[0].click;", viewSavedQuery2);
                viewSavedQuery2.click();
                System.out.println("Selected Query: " + queryName);
            } else {
                Assert.fail("Query not existing in dashboard...");
                System.out.println("Query not existing in dashboard...");
            }
        } catch (Exception e) {

            Assert.fail("Can't find the query: " + queryName);
            System.out.println("Can't find the query: " + queryName);

        }


    }

    public void selectFromThirdQuery(String queryName) {

        clickEllipsisMenu();
        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery3));
        try {
            System.out.println(viewSavedQuery3.getText().equals(queryName));
            if (viewSavedQuery3.getText().equals(queryName)) {
                js.executeScript("arguments[0].click;", viewSavedQuery3);
                viewSavedQuery3.click();
                System.out.println("Selected Query: " + queryName);
            }
        } catch (Exception e) {

            Assert.fail("Can't find the query: " + queryName);
            System.out.println("Can't find the query: " + queryName);

        }

    }

    public void selectFromFourthQuery(String queryName) {

        clickEllipsisMenu();
        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery4));
        try {
            System.out.println(viewSavedQuery4.getText().equals(queryName));
            if (viewSavedQuery4.getText().equals(queryName)) {
                js.executeScript("arguments[0].click;", viewSavedQuery4);
                viewSavedQuery4.click();
                System.out.println("Selected Query: " + queryName);
            }
        } catch (Exception e) {

            Assert.fail("Can't find the query: " + queryName);
            System.out.println("Can't find the query: " + queryName);

        }
    }


    public void selectFromSavedQuery(String queryName) {

        clickEllipsisMenu();
        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery));
        try {
            System.out.println(viewSavedQuery.getText().equals(queryName));
            System.out.println(viewSavedQuery.getText());
            System.out.println(queryName);
            if (viewSavedQuery.getAttribute("data-row").equalsIgnoreCase("0")) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", viewSavedQuery);
                action.click(viewSavedQuery);
                System.out.println("Selected Query: " + queryName);
            } else {
                Assert.fail("Query not existing in dashboard...");
                System.out.println("Query not existing in dashboard...");
            }
        } catch (Exception e) {

            Assert.fail("Can't find the query: " + queryName);
            System.out.println("Can't find the query: " + queryName);

        }

    }


    public void saveQuery(String queryName) throws IOException {

        //String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm").format(new Date());

        wait.until(ExpectedConditions.elementToBeClickable(saveQueryInputField));
        saveQueryInputField.sendKeys(queryName);
        System.out.println("Entered save query name is: " + queryName);

        //Click save button
        wait.until(ExpectedConditions.elementToBeClickable(saveBtn));
        //js.executeScript("arguments[0].click;", saveBtn);
        saveBtn.click();
        System.out.println("Triggered save button");

        wait.until(ExpectedConditions.visibilityOf(querySavedMessage));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        try {
            //Verify message:
            if (querySavedMessage.getText().equalsIgnoreCase("Query saved.")) {
                Assert.assertTrue("Query saved.", true);
                System.out.println("Query saved.");

                //Click ok button
                wait.until(ExpectedConditions.elementToBeClickable(okBtn));
                try {
                    if (okBtn.getAttribute("aria-label").equals("Close")) {

                        js.executeScript("arguments[0].click;", okBtn);
                        action.doubleClick(okBtn);
                        //okBtn.click();
                        System.out.println("Triggered ok button");
                        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
                    }
                } catch (Exception e) {

                    System.out.println("Can't click element");
                }
            }
        } catch (Exception e) {

            Assert.fail("Unable to save query!");
            takeScreenshot(driver);
            System.out.println("Unable to save query!");

        }

    }

    public void verifyMyQueries(String queryName) {

        //click ellipsis menu
        clickEllipsisMenu();

        wait.until(ExpectedConditions.visibilityOf(viewSavedQuery));
        if (viewSavedQuery.getText().equals(queryName)) {
            Assert.assertTrue("Query is present under Saved Queries", true);
            System.out.println("Query is present under Saved Queries");
        } else {
            Assert.fail("Saved query is not present under Saved Queries!");
            System.out.println("Query is present under Saved Queries");
        }

    }

    public void verifyDataTable(String queryName) {

        List<WebElement> rows = driver.findElements(viewSavedQ);
        Iterator<WebElement> i = rows.iterator();
        while (i.hasNext()) {
            WebElement row = i.next();
            System.out.println(row.getText());
            if (row.getText().equalsIgnoreCase(queryName)) {
                Assert.assertTrue("Query is present under Saved Queries", true);
                System.out.println("Query is present under Saved Queries");
            } else {
                Assert.fail("Saved query is not present under Saved Queries!");
                System.out.println("Query is present under Saved Queries");
            }
        }


    }

    public void verifySharedSavedQuery_DataTable(String queryName) {

        clickEllipsisMenu();

        if (sharedQueryIndicator.isDisplayed()) {
            System.out.println("Shared Indicator is present..Verifying saved queries...");

            List<WebElement> td_collection = driver.findElements(viewSavedQ);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(queryName)) {
                        System.out.println(webElement.getText().equals(queryName));
                        Assert.assertTrue("Query:  " + queryName + " is present on Saved Queries...", true);
                        System.out.println("Query:  " + queryName + " is present on Saved Queries...");
                    }
                } catch (Exception e) {

                    Assert.fail("QUERY IS NOT PRESENT ON SAVED QUERIES!");
                    System.out.println("QUERY IS NOT PRESENT ON SAVED QUERIES!");
                }

        }


    }

    public void selectFromSavedQuery_DataTable(String queryName) {

        clickEllipsisMenu();
        List<WebElement> rows_table = saveQueriesTable.findElements(savedQuery_DataTable);
        int rows_count = rows_table.size();
        System.out.println("Number of Rows present: " + rows_table.size());

        for (WebElement webElement : rows_table) {

            String rowText = webElement.getText();

            try {
                if (rowText.equals(queryName)) {
                    System.out.println(rowText.equals(queryName));
                    Assert.assertTrue("Query:  " + queryName + " is present on Saved Queries...", true);
                    System.out.println("Query:  " + queryName + " is present on Saved Queries...");
                   /* js.executeScript("arguments[0].click;", savedQuery_DataTableWE);
                    savedQuery_DataTableWE.click();
                    System.out.println("Selected Query: " + queryName);*/

                }
            } catch (Exception e) {

                Assert.fail("QUERY IS NOT PRESENT ON SAVED QUERIES!");
                System.out.println("QUERY IS NOT PRESENT ON SAVED QUERIES!");
            }

        }

    }


    public void verifySavedQueryData_DataTable(String queryName) {

        clickEllipsisMenu();

        List<WebElement> td_collection = driver.findElements(viewSavedQ);
        for (WebElement webElement : td_collection)

            try {
                if (webElement.getText().equals(queryName)) {
                    System.out.println(webElement.getText().equals(queryName));
                    Assert.assertTrue("Query:  " + queryName + " is present on Saved Queries...", true);
                    System.out.println("Query:  " + queryName + " is present on Saved Queries...");
                }
            } catch (Exception e) {

                Assert.fail("QUERY IS NOT PRESENT ON SAVED QUERIES!");
                System.out.println("QUERY IS NOT PRESENT ON SAVED QUERIES!");
            }


    }


}
