package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class WatchListPage extends BaseUtil {


    public JavascriptExecutor js = (JavascriptExecutor) driver;
    public String storeItem1, storeItem2, storeItem3, storeItem4, storeItem5, storeItem6,storeItem7,storeItem8,storeItem9,storeItem10;
    public String storeItem1WatchList, storeItem2WatchList, storeItem3WatchList, storeItem4WatchList, storeItem5WatchList, storeItem6WatchList;
    public Actions action = new Actions(driver);
    //initialize page objects:
    MainPage main = new MainPage(driver);

    //elements:
    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[1]/div[1]/div/input")
    WebElement checkItem1;


    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[2]/div[1]/div/input")
    WebElement checkItem2;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[3]/div[1]/div/input")
    WebElement checkItem3;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[4]/div[1]/div/input")
    WebElement checkItem4;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[5]/div[1]/div/input")
    WebElement checkItem5;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[6]/div[1]/div/input")
    WebElement checkItem6;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[1]/div[1]/div/input")
    WebElement selectWatchListItem1;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[2]/div[1]/div/input")
    WebElement selectWatchListItem2;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[3]/div[1]/div/input")
    WebElement selectWatchListItem3;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[4]/div[1]/div/input")
    WebElement selectWatchListItem4;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[5]/div[1]/div/input")
    WebElement selectWatchListItem5;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[6]/div[1]/div/input")
    WebElement selectWatchListItem6;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[1]/div[2]/span")
    WebElement isin1;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[2]/div[2]/span")
    WebElement isin2;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[3]/div[2]/span")
    WebElement isin3;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[4]/div[2]/span")
    WebElement isin4;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[5]/div[2]/span")
    WebElement isin5;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[6]/div[2]/span")
    WebElement isin6;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[1]/div[2]/span")
    WebElement isin1WatchList;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[2]/div[2]/span")
    WebElement isin2WatchList;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[3]/div[2]/span")

    WebElement isin3WatchList;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[4]/div[2]/span")

    WebElement isin4WatchList;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[5]/div[2]/span")

    WebElement isin5WatchList;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[6]/div[2]/span")

    WebElement isin6WatchList;

    @FindBy(xpath = "//*[contains(@title,'Add to Watch List')]")

    WebElement addToWatchList;

    @FindBy(xpath = "//*[contains(@title,'Remove from Watch List')]")

    WebElement removeToWatchList;

    @FindBy(xpath = "//*[@id='content']/div[1]/h4")

    WebElement watchListHeader;

    @FindBy(xpath = "//*[@id='watchlist-status-modal-message']/p[1]")

    WebElement watchListUpdateMessage;

    @FindBy(xpath = "//*[@id='watchlistUpdateModal']//*[contains(text(),'OK')]")

    WebElement okToWatchList;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[1]/input[@name='sec_id']")

    WebElement secIDInputField;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[2]/input[@name='sec_name']")

    WebElement secNameInputField;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[6]/div/div[2]/input[@name='entity_name']")

    WebElement entityNameInputField;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/button[@class='multiselect dropdown-toggle btn btn-default ccy-multiselect']")

    WebElement ccyDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[1]/a/label/input[@value='all']")

    WebElement ccyDropdown_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[3]/a/label/input[@value='EUR_top']")

    WebElement ccyDropdown_EUR;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/button[@class='multiselect dropdown-toggle sector-multiselect btn btn-default']")

    WebElement sectorDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement sectorDropdown_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[4]/a/label/input[@value='Banks']")

    WebElement sectorDropdown_Banks;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[5]/a/label/input[@value='Basic Resources']")

    WebElement sectorDropdown_Basic;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[8]/a/label/input[@value='Covered']")

    WebElement sectorDropdown_Covered;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[11]/a/label/input[@value='Health Care']")

    WebElement sectorDropdown_Healthcare;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[4]/a/label/input[@value='USD_top']")

    WebElement ccyDropdown_USD;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[5]/a/label/input[@value='GBP_top']")

    WebElement ccyDropdown_GBP;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/button[@class='multiselect dropdown-toggle btn btn-default']")

    WebElement ctryofIssuerDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[2]/a/label/input[@value='ALL']")

    WebElement ctryofIssuerDropdown_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[1]/div/input[@placeholder='Search']")

    WebElement ctryofIssuer_Input;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[197]/a/label/input[@value='United States']")

    WebElement ctryofIssuer_USA;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[196]/a/label/input[@value='United Kingdom']")

    WebElement ctryofIssuer_UK;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[13]/a/label/input[@value='Australia']")

    WebElement ctryofIssuer_AU;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/button")

    WebElement ctryofRiskDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[2]/a/label/input[@value='ALL']")

    WebElement ctryofRiskDropdown_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[1]/div/input[@placeholder='Search']")

    WebElement ctryofRisk_Input;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[13]/a/label/input[@value='Australia']")

    WebElement ctryofRisk_AU;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[197]/a/label/input[@value='United States']")

    WebElement ctryofRisk_USA;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[196]/a/label/input[@value='United Kingdom']")

    WebElement ctryofRisk_UK;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div/input[@value='Asset Class (ALL)']")

    WebElement assetClassDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[2]/ul/li[1]/span/input[@class='Asset Class-All']")

    WebElement assetClass_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[2]/ul/li[3]/span/input[@class='asset Agency']")

    WebElement assetClass_Agency;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[2]/ul/li[6]/span/input[@class='asset Financial']")

    WebElement assetClass_Financial;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[2]/ul/li[4]/span/input[@class='asset Corporate']")

    WebElement assetClass_Corporate;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[14]/div/div[2]/div/button[@class='multiselect dropdown-toggle btn btn-default']")

    WebElement creditRatingDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[14]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement creditRating_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[14]/div/div[2]/div/ul/li[2]/a/label/input[@value='AAA']")

    WebElement creditRating_AAA;

    @FindBy(xpath = "")
    WebElement creditRating_AAplus;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div//*[contains(@class,'ui-widget-content slick-row')]")
    WebElement gridDataChecking;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[15]/div/div/button[1]")
    WebElement clearBtn;

    @FindBy(xpath = "//*[@id='watchlist-tab']//*[contains(@class,'btn btn-warning btn-clear was-clear-search-watchlist')]")
    WebElement clearBtn_WatchListAlert;

    @FindBy(xpath = "//div[@class='slick-cell l1 r1']/span")
    WebElement isinField;

    @FindBy(xpath = "//*[@id='nav-tabs-watchlist']/a")
    WebElement watchListTab;

    @FindBy(xpath = "//*[@id='header-left-nav'/div/li/a")
    WebElement sideMenu;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[6]/button[@title='Expand Table']")
    WebElement expandBtn;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/div/a")
    WebElement sideDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[2]/div/a")
    WebElement typeDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/button[@class='multiselect dropdown-toggle region-multiselect btn btn-default']")

    WebElement regionDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[3]/a/label/input")

    WebElement regionDropdown_Asia;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[7]/a/label/input[@value='Europe']")

    WebElement regionDropdown_Europe;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[5]/a/label/input[@value='C.Amer']")

    WebElement regionDropdown_CAmer;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement regionDropdown_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/button/span")

    WebElement seniorityDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement seniorityDropdown_ALL;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[11]/a/label/input[@value='Senior']")

    WebElement seniority_Senior;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[20]/a/label/input[@value='Unsubordinated']")

    WebElement seniority_Unsubordinated;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[15]/a/label/input[@value='Subordinated']")

    WebElement seniority_Subordinated;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[12]/div/div[1]/div")

    WebElement emDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[12]/div/div[2]/div")

    WebElement frnDropdown;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[1]/div[2]/div/form[2]/div[2]/div/div[1]/div[6]/div/div[1]/div/label[2]/input[@name='excl_cancels']")

    WebElement exclcancelBtn;

    @FindBy(xpath = "//*[@id='content']/div[1]/div[3]/button")

    WebElement watchAlertSettings;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[3]/div/div/div/section/div[2]/div[1]/div[2]/span/ul/li[1]/button[1]")

    WebElement addSecuritiesBtn;


    @FindBy(xpath = "//*[@id='addWatchlistModal']/div/div/form/div[1]/div[2]/div/input")

    WebElement secIDInput_AlertSettings;

    @FindBy(xpath = "//*[@id='submit-add-watchlist']")

    WebElement addBtn_AlertSettings;

    @FindBy(xpath = "//*[@id='cancel-add-watchlist']")

    WebElement cancelBtn_AlertSettings;

    @FindBy(xpath = "//*[@id='watchlist-tab']//*[contains(@class,'form-control was-search-watchlist')]")

    WebElement secIDNameInputField;

    @FindBy(xpath = "//*[@id='watchlist-alert-table']//*[contains(@name,'deleteCheckbox')]")

    WebElement deletecheckbox_WatchListAlert;

    @FindBy(xpath = "//*[@id='delete-column']")

    WebElement deleteButton_WatchListAlert;

    @FindBy(xpath = "//*[@id='watchlist-alert-table']/tbody/tr/td[3]")
    WebElement isin_TD;

    @FindBy(xpath = "//*[@id='watchlist-alert-table']//td[@class='sorting_1']")
    WebElement td_SecName;

    @FindBy(xpath = "//*[@id='watchlist-alert-table']/tbody/tr/td/following::div/input[@data-column='alert']")
    WebElement alertToggle;

    @FindBy(xpath = "//*[@id='watchlist-alert-table']/tbody/tr/td/following::div/input[@data-column='newAxe']")
    WebElement newAxeToggle;

    @FindBy(xpath = "//*[@id='watchlist-alert-table']/tbody/tr/td/following::div/input[@data-column='axeUpdated']")
    WebElement axeUpdatedToggle;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[3]/div/div/div/section/div[2]/div[3]/div/button[1]")

    WebElement saveChangesBtn;

    @FindBy(xpath = "//*[@id='watchlist-tab']/div/div[3]/div/div/div/section/div[1]/h4/button")

    WebElement closeBtn_WatchListAlert;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell')]/div/input[@type='checkbox']")
    WebElement isinCheckBox;

    By isinColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'ISIN')]");
    By isinTD = By.xpath("//*[@id='watchlist-alert-table']/tbody/tr/td[3]");
    By isinRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]/span");
    By secIDRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l3 r3')]/span");
    //By isinCheckBox = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell')]/div/input[@type='checkbox']");

    public WatchListPage(WebDriver driver) {
        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 30);
        //constructor
        PageFactory.initElements(driver, this);
    }

    public void clickWatchList() {
        try {
            if (watchListTab.isDisplayed()) {
                js.executeScript("arguments[0].click;", watchListTab);
                watchListTab.click();
                System.out.println("Triggered Watch List tab");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can't click element!");
        }
        //wait.until(ExpectedConditions.visibilityOf(watchListTab));
    }

    public void clickWatchListAlertSettings() {

        wait.until(ExpectedConditions.elementToBeClickable(watchAlertSettings));
        js.executeScript("arguments[0].click;", watchAlertSettings);
        watchAlertSettings.click();
        System.out.println("Triggered watch list alert settings button");

    }

    public void addMultipleSecIDs_To_WatchListAlert_Settings(String secID1, String secID2) {

        wait.until(ExpectedConditions.elementToBeClickable(addSecuritiesBtn));
        js.executeScript("arguments[0].click;", addSecuritiesBtn);
        addSecuritiesBtn.click();
        System.out.println("Triggered add securities button..Opening pop up page.");

        String[] secIDs = new String[]{
                secID1,
                secID2,

        };

        for (String secID : secIDs) {

            ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", secIDInput_AlertSettings);
            secIDInput_AlertSettings.sendKeys(secID);
            secIDInput_AlertSettings.sendKeys(Keys.TAB);
            ((JavascriptExecutor) driver).executeScript("arguments[0].click;", secIDInput_AlertSettings);
            System.out.println("Sec ID : " + secID);
        }
        addBtn_AlertSettings.click();
        System.out.println("Triggered add button");

        waitforPageLoaded();

    }

    public void verifyMultipleSecurities_AlertTable(String secID1, String secID2) {

        String[] secIDs = new String[]
                {
                        secID1,
                        secID2,

                };
        for (String secID : secIDs) {

            try {

                if (isin_TD.getText().equals(secID)) {

                    Assert.assertTrue("Sec ID : " + secID + " found in Watch List Alert Settings Table.. ", true);
                    System.out.println("Sec ID : " + secID + " found in Watch List Alert Settings Table.. ");

                    //check security toggle default configuration:
                    System.out.println("=============Checking of Security Toggle Configuration============");

                    try {
                        if (alertToggle.getAttribute("data-value").equalsIgnoreCase("1")) {
                            Assert.assertTrue("Alert Toggle is set to Yes by default", true);
                            System.out.println("Alert Toggle is set to Yes by default");
                        }
                        if (newAxeToggle.getAttribute("data-value").equalsIgnoreCase("1")) {
                            Assert.assertTrue("New Axe Toggle is set to Yes by default", true);
                            System.out.println("New Axe Toggle is set to Yes by default");
                        }
                        if (axeUpdatedToggle.getAttribute("data-value").equalsIgnoreCase("0")) {
                            Assert.assertTrue("Axe Updated Toggle is set to No by default", true);
                            System.out.println("Axe Updated Toggle is set to No by default");
                        }
                    } catch (Exception e) {

                        Assert.fail("Security Toggle is not set by default!");
                        System.out.println("Security Toggle is not set by default!");
                    }
                }


            } catch (Exception e) {

                Assert.fail("Sec ID : " + secID + " NOT FOUND in Watch List Alert Settings Table...");
                System.out.println("Sec ID : " + secID + " NOT FOUND in Watch List Alert Settings Table...");

            }
        }

    }


    public void verifyMultipleSecurities_SecurityToggleConfiguration(String secID1, String secID2) throws InterruptedException {

        String[] secIDs = {secID1, secID2};

        wait.until(ExpectedConditions.elementToBeClickable(secIDNameInputField));
        for (String secID : secIDs) {

            ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", secIDNameInputField);
            secIDNameInputField.clear();
            secIDNameInputField.sendKeys(secID);
            driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            System.out.println("Entered SEC ID: " + secID);
        }
    }

    public void addSecID_To_WatchList_Alert_Settings(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(addSecuritiesBtn));
        js.executeScript("arguments[0].click;", addSecuritiesBtn);
        addSecuritiesBtn.click();
        System.out.println("Triggered add securities button..Opening pop up page.");

        secIDInput_AlertSettings.sendKeys(value);
        System.out.println("Entered value is: " + value);

        addBtn_AlertSettings.click();
        js.executeScript("arguments[0].click;", addBtn_AlertSettings);
        System.out.println("Added Sec ID : " + value);

    }

    public void removeSecID_To_WatchList_Alert_Settings(String value) {


        wait.until(ExpectedConditions.elementToBeClickable(secIDInput_AlertSettings));

        secIDInput_AlertSettings.sendKeys(value);
        System.out.println("Entered value is: " + value);

        wait.until(ExpectedConditions.elementToBeClickable(deletecheckbox_WatchListAlert));
        js.executeScript("arguments[0].click;", deletecheckbox_WatchListAlert);
        deletecheckbox_WatchListAlert.click();
        System.out.println("Checked security...");

        wait.until(ExpectedConditions.elementToBeClickable(deleteButton_WatchListAlert));
        js.executeScript("arguments[0].click;", deleteButton_WatchListAlert);
        deleteButton_WatchListAlert.click();
        System.out.println("Triggered delete button...");


    }

    public void test(String secID1, String secID2) {

        String[] secIDs = {secID1, secID2};

        for (String secID : secIDs) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", secIDNameInputField);
            secIDNameInputField.sendKeys(secID);
            clearBtn_WatchListAlert.click();
            System.out.println("Cleared previous entry...");
/*            waitforPageLoaded();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            System.out.println("Entered SEC ID: " + secID);
            wait.until(ExpectedConditions.visibilityOf(isin_TD));
            System.out.println("check if visible..->>" + wait.until(ExpectedConditions.visibilityOf(isin_TD)));*/
            System.out.println("check if true... ->>" + isin_TD.getText().equals(secID));
        }

    }

    public void saveMultipleSecIDs_To_WatchList(String secID1, String secID2, String secName1, String secName2) {

        String[] secIDs = {secID1, secID2};
        String[] secNames = {secName1, secName2};

        //Click save changes to verify display sec name for the added SEC ID
        wait.until(ExpectedConditions.elementToBeClickable(saveChangesBtn));
        js.executeScript("arguments[0].click;", saveChangesBtn);
        saveChangesBtn.click();
        System.out.println("Triggered save changes button");

        waitforPageLoaded();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        for (String secID : secIDs) {

            try {
                if (watchListUpdateMessage.isDisplayed()) {
                    Assert.assertTrue("Your watch list has been updated", true);
                    System.out.println("Your watch list has been updated");

                    //click ok button
                    okToWatchList.click();
                    System.out.println("Triggered ok button");

                    secIDNameInputField.sendKeys(secID1);
                    System.out.println("Entered SEC ID: " + secID1);
                }
            } catch (Exception e) {

                Assert.fail("Unable to save your WatchList!");
                System.out.println("Unable to save your WatchList!");

            }

        }

        for (String secName : secNames) {

            try {
                if (td_SecName.getText().equalsIgnoreCase(secName)) {

                    Assert.assertTrue("Sec Name " + secName + "is present", true);
                    System.out.println("Sec Name " + secName + "is present");

                    //Click close
                    wait.until(ExpectedConditions.elementToBeClickable(closeBtn_WatchListAlert));
                    closeBtn_WatchListAlert.click();
                    System.out.println("Triggered close button.");
                }

            } catch (Exception e) {

                Assert.fail("SEC name did not show up!");
                System.out.println("SEC name did not show up!");

            }

        }

        /*try {
            if (watchListUpdateMessage.isDisplayed()) {
                Assert.assertTrue("Your watch list has been updated", true);
                System.out.println("Your watch list has been updated");

                //click ok button
                okToWatchList.click();
                System.out.println("Triggered ok button");

                secIDNameInputField.sendKeys(secID1);
                System.out.println("Entered SEC ID: " + secID1);

                if (td_SecName.getText().equalsIgnoreCase(secName)) {

                    Assert.assertTrue("Sec Name " + secName + "is present for: " + value + " Sec ID", true);
                    System.out.println("Sec Name " + secName + "is present for: " + value + " Sec ID");

                    //Click close
                    wait.until(ExpectedConditions.elementToBeClickable(closeBtn_WatchListAlert));
                    closeBtn_WatchListAlert.click();
                    System.out.println("Triggered close button.");

                } else {
                    Assert.fail("SEC name did not show up!");
                    System.out.println("SEC name did not show up!");
                }

            }
        } catch (Exception e) {

            Assert.fail("Unable to save your watchlist!");
            System.out.println("Unable to save your watchlist!");
        }*/


    }


    public void saveChangesToWatchList(String value, String secName) {

        //Click save changes to verify display sec name for the added SEC ID
        wait.until(ExpectedConditions.elementToBeClickable(saveChangesBtn));
        js.executeScript("arguments[0].click;", saveChangesBtn);
        saveChangesBtn.click();
        System.out.println("Triggered save changes button");

        waitforPageLoaded();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        try {

            if (watchListUpdateMessage.isDisplayed()) {
                Assert.assertTrue("Your watch list has been updated", true);
                System.out.println("Your watch list has been updated");

                //click ok button
                okToWatchList.click();
                System.out.println("Triggered ok button");

                secIDNameInputField.sendKeys(value);
                System.out.println("Entered SEC ID: " + value);

                if (td_SecName.getText().equalsIgnoreCase(secName)) {

                    Assert.assertTrue("Sec Name " + secName + "is present for: " + value + " Sec ID", true);
                    System.out.println("Sec Name " + secName + "is present for: " + value + " Sec ID");

                    //Click close
                    wait.until(ExpectedConditions.elementToBeClickable(closeBtn_WatchListAlert));
                    closeBtn_WatchListAlert.click();
                    System.out.println("Triggered close button.");

                } else {
                    Assert.fail("SEC name did not show up!");
                    System.out.println("SEC name did not show up!");
                }

            }
        } catch (Exception e) {

            Assert.fail("Unable to save your watchlist!");
            System.out.println("Unable to save your watchlist!");
        }

    }

    public void verifySecurityToggleConfiguration(String value, String secName) {

        wait.until(ExpectedConditions.elementToBeClickable(secIDNameInputField));

        secIDNameInputField.sendKeys(value);
        System.out.println("Entered SEC ID: " + value);

        try {
            if (isin_TD.getText().equalsIgnoreCase(value)) {
                Assert.assertTrue("Sec ID : " + value + " found in Watch List Alert Settings Table.. ", true);
                System.out.println("Sec ID : " + value + " found in Watch List Alert Settings Table.. ");

                //check security toggle default configuration:
                System.out.println("=============Checking of Security Toggle============");

                try {
                    if (alertToggle.getAttribute("data-value").equalsIgnoreCase("1")) {
                        Assert.assertTrue("Alert Toggle is set to Yes by default", true);
                        System.out.println("Alert Toggle is set to Yes by default");
                    }
                    if (newAxeToggle.getAttribute("data-value").equalsIgnoreCase("1")) {
                        Assert.assertTrue("New Axe Toggle is set to Yes by default", true);
                        System.out.println("New Axe Toggle is set to Yes by default");
                    }
                    if (axeUpdatedToggle.getAttribute("data-value").equalsIgnoreCase("0")) {
                        Assert.assertTrue("Axe Updated Toggle is set to No by default", true);
                        System.out.println("Axe Updated Toggle is set to No by default");
                    }
                } catch (Exception e) {

                    Assert.fail("Security Toggle is not set by default");
                    System.out.println("Security Toggle is not set by default");

                }


            }


        } catch (Exception e) {

            Assert.fail("Sec ID : " + value + " NOT FOUND in Watch List Alert Settings Table...");
            System.out.println("Sec ID : " + value + " NOT FOUND in Watch List Alert Settings Table...");

        }

    }


    public void clickExcludeCancels() {

        try {
            if (exclcancelBtn.isDisplayed()) {
                //js.executeScript("arguments[0].click;", exclcancelBtn);
                exclcancelBtn.click();
                System.out.println("Unchecked exclude cancels checkbox");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can't click element!");
        }

    }

    public void enterSecID(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(secIDInputField));
        secIDInputField.clear();
        secIDInputField.sendKeys(value);
        System.out.println("Entered " + value + " on SEC ID input field...");
    }

    public void enterSecName(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(secNameInputField));
        secNameInputField.clear();
        secNameInputField.sendKeys(value);
        System.out.println("Entered " + value + " on SEC Name input field...");

    }

    public void enterEntityName(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(entityNameInputField));
        entityNameInputField.clear();
        entityNameInputField.sendKeys(value);
        System.out.println("Entered " + value + " on Entity Name input field...");

    }

    public void selectFromEM(String value) {

        wait.until(ExpectedConditions.visibilityOf(emDropdown));

        try {

            Robot robot = new Robot();
            if (value.equalsIgnoreCase("EM (Only)")) {

                emDropdown.click();
                System.out.println("Triggered em drop down...");

                //key press:
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

                System.out.println("Key press complete...");
            }
            if (value.equalsIgnoreCase("EM (Excluded)")) {

                emDropdown.click();
                System.out.println("Triggered em drop down...");

                //key press:
                for (int i = 0; i < 3; i++) {

                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                }
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

                System.out.println("Key press complete...");

            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't select value from dropdown list...");


        }


    }

    public void selectFromFRN(String value) {

        wait.until(ExpectedConditions.visibilityOf(frnDropdown));

        try {

            Robot robot = new Robot();
            if (value.equalsIgnoreCase("FRN (Only)")) {

                frnDropdown.click();
                System.out.println("Triggered frn drop down...");

                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

                System.out.println("Key press complete...");
            }
            if (value.equalsIgnoreCase("FRN (Excluded)")) {
                js.executeScript("arguments[0].click;", frnDropdown);
                frnDropdown.click();
                System.out.println("Triggered frn drop down...");

                //key press:
                for (int i = 0; i < 3; i++) {

                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                }
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

                System.out.println("Key press complete...");

            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't select value from dropdown list...");
        }
    }

    public void selectRegion(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(regionDropdown));
        js.executeScript("arguments[0].click;", regionDropdown);
        regionDropdown.click();
        System.out.println("Triggered region drop down...");

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        js.executeScript("arguments[0].click;", regionDropdown_ALL);
        regionDropdown_ALL.click();
        System.out.println("Unchecked Region - ALL");

        try {
            if (value.equalsIgnoreCase("Single")) {
                wait.until(ExpectedConditions.elementToBeClickable(regionDropdown_Asia));
                js.executeScript("arguments[0].click;", regionDropdown_Asia);
                regionDropdown_Asia.click();
                System.out.println("Checked Asia checkbox");
            }

            if (value.equalsIgnoreCase("Multiple")) {
                wait.until(ExpectedConditions.elementToBeClickable(regionDropdown_Europe));
                js.executeScript("arguments[0].click;", regionDropdown_Europe);
                regionDropdown_Europe.click();
                System.out.println("Checked Europe checkbox");

                wait.until(ExpectedConditions.elementToBeClickable(regionDropdown_CAmer));
                js.executeScript("arguments[0].click;", regionDropdown_CAmer);
                regionDropdown_CAmer.click();
                System.out.println("Checked C.Amer checkbox");
            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }

    }

    public void selectAssetClass(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(assetClassDropdown));
        js.executeScript("arguments[0].click;", assetClassDropdown);
        assetClassDropdown.click();
        System.out.println("Triggered asset class drop down...");

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        js.executeScript("arguments[0].click;", assetClass_ALL);
        assetClass_ALL.click();
        System.out.println("Unchecked Asset Class - ALL");

        try {
            if (value.equalsIgnoreCase("Single")) {
                wait.until(ExpectedConditions.elementToBeClickable(assetClass_Corporate));
                //js.executeScript("arguments[0].click;", assetClass_Agency);
                assetClass_Corporate.click();
                System.out.println("Checked asset class corporate checkbox");
            }

            if (value.equalsIgnoreCase("Multiple")) {
                wait.until(ExpectedConditions.elementToBeClickable(assetClass_Agency));
                //js.executeScript("arguments[0].click;", assetClass_Corporate);
                assetClass_Agency.click();
                System.out.println("Checked asset class - corporate checkbox");

                wait.until(ExpectedConditions.elementToBeClickable(assetClass_Financial));
                //js.executeScript("arguments[0].click;", assetClass_Financial);
                assetClass_Financial.click();
                System.out.println("Checked asset class - financial checkbox");
            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }

    }

    public void selectSeniority(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(seniorityDropdown));
        js.executeScript("arguments[0].click;", seniorityDropdown);
        seniorityDropdown.click();
        System.out.println("Triggered seniority drop down...");

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        js.executeScript("arguments[0].click;", seniorityDropdown_ALL);
        seniorityDropdown_ALL.click();
        System.out.println("Unchecked Seniority - ALL");

        try {
            if (value.equalsIgnoreCase("Single")) {
                wait.until(ExpectedConditions.elementToBeClickable(seniority_Senior));
                js.executeScript("arguments[0].click;", seniority_Senior);
                seniority_Senior.click();
                System.out.println("Checked seniority - senior checkbox");
            }

            if (value.equalsIgnoreCase("Multiple")) {
                wait.until(ExpectedConditions.elementToBeClickable(seniority_Subordinated));
                js.executeScript("arguments[0].click;", seniority_Subordinated);
                seniority_Subordinated.click();
                System.out.println("Checked seniority - subordinated checkbox");

                wait.until(ExpectedConditions.elementToBeClickable(seniority_Unsubordinated));
                js.executeScript("arguments[0].click;", seniority_Unsubordinated);
                seniority_Unsubordinated.click();
                System.out.println("Checked seniority - unsubordinated checkbox");
            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }
    }

    public void selectSingleIssuer(String country_code) {

        wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuerDropdown));
        js.executeScript("arguments[0].click;", ctryofIssuerDropdown);
        ctryofIssuerDropdown.click();
        System.out.println("Triggered ctry.of issuer drop down...");

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        js.executeScript("arguments[0].click;", ctryofIssuerDropdown_ALL);
        ctryofIssuerDropdown_ALL.click();
        System.out.println("Unchecked ctry.of issuer - ALL");

        try {
            if (country_code.equalsIgnoreCase("USA")) {

                wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuer_USA));
                js.executeScript("arguments[0].click;", ctryofIssuer_USA);
                ctryofIssuer_USA.click();
                System.out.println("Checked USA checkbox");
            }
            if (country_code.equalsIgnoreCase("GBR")) {

                wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuer_UK));
                js.executeScript("arguments[0].click;", ctryofIssuer_UK);
                ctryofIssuer_UK.click();
                System.out.println("Checked UK checkbox");
            }
            if (country_code.equalsIgnoreCase("AUS")) {
                wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuer_AU));
                js.executeScript("arguments[0].click;", ctryofIssuer_AU);
                ctryofIssuer_AU.click();
                System.out.println("Checked UK checkbox");
            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item..");

        }
    }

    public void selectCtryofRisk(String value, String country_code) {

        js.executeScript("arguments[0].focus;", ctryofRiskDropdown);

        try {
            if (value.equalsIgnoreCase("Multiple")) {

                wait.until(ExpectedConditions.elementToBeClickable(ctryofRiskDropdown));
                js.executeScript("arguments[0].click;", ctryofRiskDropdown);
                ctryofRiskDropdown.click();
                System.out.println("Triggered ctry of risk drop down");

                wait.until(ExpectedConditions.elementToBeClickable(ctryofRiskDropdown_ALL));
                js.executeScript("arguments[0].click;", ctryofRiskDropdown_ALL);
                ctryofRiskDropdown_ALL.click();
                System.out.println("Checked ALL checkbox");

                if (country_code.equalsIgnoreCase("United")) {

                    wait.until(ExpectedConditions.visibilityOf(ctryofRisk_Input));
                    ctryofRisk_Input.sendKeys(country_code);
                    System.out.println("Entered country code is: " + country_code);

                    wait.until(ExpectedConditions.elementToBeClickable(ctryofRisk_USA));
                    js.executeScript("arguments[0].click;", ctryofRisk_USA);
                    ctryofRisk_USA.click();
                    System.out.println("Checked USA checkbox");

                    wait.until(ExpectedConditions.elementToBeClickable(ctryofRisk_UK));
                    js.executeScript("arguments[0].click;", ctryofRisk_UK);
                    ctryofRisk_UK.click();
                    System.out.println("Checked UK checkbox");
                }


            }

            if (value.equalsIgnoreCase("Single")) {

                wait.until(ExpectedConditions.elementToBeClickable(ctryofRiskDropdown));
                js.executeScript("arguments[0].click;", ctryofRiskDropdown);
                ctryofRiskDropdown.click();
                System.out.println("Triggered ctry.of risk drop down...");

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                js.executeScript("arguments[0].click;", ctryofRiskDropdown_ALL);
                ctryofRiskDropdown_ALL.click();
                System.out.println("Unchecked ctry.of risk - ALL");


                if (country_code.equalsIgnoreCase("AUS")) {

                    wait.until(ExpectedConditions.visibilityOf(ctryofRisk_Input));
                    ctryofRisk_Input.sendKeys(country_code);
                    System.out.println("Entered country code is: " + country_code);

                    wait.until(ExpectedConditions.elementToBeClickable(ctryofRisk_AU));
                    js.executeScript("arguments[0].click;", ctryofRisk_AU);
                    ctryofRisk_AU.click();
                    System.out.println("Checked AUS checkbox");
                }

            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }


    }


    public void selectCtryofIssuer(String value, String country_code) {

        js.executeScript("arguments[0].focus;", ctryofIssuerDropdown);

        try {
            if (value.equalsIgnoreCase("Multiple")) {

                wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuerDropdown));
                js.executeScript("arguments[0].click;", ctryofIssuerDropdown);
                ctryofIssuerDropdown.click();
                System.out.println("Triggered ctry of issuer drop down");

                wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuerDropdown_ALL));
                js.executeScript("arguments[0].click;", ctryofIssuerDropdown_ALL);
                ctryofIssuerDropdown_ALL.click();
                System.out.println("Checked ALL checkbox");

                if (country_code.equalsIgnoreCase("United")) {

                    wait.until(ExpectedConditions.visibilityOf(ctryofIssuer_Input));
                    ctryofIssuer_Input.sendKeys(country_code);
                    System.out.println("Entered country code is: " + country_code);

                    wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuer_USA));
                    js.executeScript("arguments[0].click;", ctryofIssuer_USA);
                    ctryofIssuer_USA.click();
                    System.out.println("Checked USA checkbox");

                    wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuer_UK));
                    js.executeScript("arguments[0].click;", ctryofIssuer_UK);
                    ctryofIssuer_UK.click();
                    System.out.println("Checked UK checkbox");
                }


            }

            if (value.equalsIgnoreCase("Single")) {

                wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuerDropdown));
                js.executeScript("arguments[0].click;", ctryofIssuerDropdown);
                ctryofIssuerDropdown.click();
                System.out.println("Triggered ctry.of issuer drop down...");

                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                js.executeScript("arguments[0].click;", ctryofIssuerDropdown_ALL);
                ctryofIssuerDropdown_ALL.click();
                System.out.println("Unchecked ctry.of issuer - ALL");


                if (country_code.equalsIgnoreCase("AUS")) {

                    wait.until(ExpectedConditions.visibilityOf(ctryofIssuer_Input));
                    ctryofIssuer_Input.sendKeys(country_code);
                    System.out.println("Entered country code is: " + country_code);

                    wait.until(ExpectedConditions.elementToBeClickable(ctryofIssuer_AU));
                    js.executeScript("arguments[0].click;", ctryofIssuer_AU);
                    ctryofIssuer_AU.click();
                    System.out.println("Checked UK checkbox");
                }


            }

        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }


    }


    public void selectCCY(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(ccyDropdown));
        js.executeScript("arguments[0].click;", ccyDropdown);
        ccyDropdown.click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        js.executeScript("arguments[0].click;", ccyDropdown_ALL);
        ccyDropdown_ALL.click();
        System.out.println("Unchecked currency - ALL");

        try {
            if (value.equalsIgnoreCase("Single")) {
                wait.until(ExpectedConditions.elementToBeClickable(ccyDropdown_EUR));
                js.executeScript("arguments[0].click;", ccyDropdown_EUR);
                ccyDropdown_EUR.click();
                System.out.println("Triggered EUR checkbox");
            }
            if (value.equalsIgnoreCase("Multiple")) {
                wait.until(ExpectedConditions.elementToBeClickable(ccyDropdown_USD));
                js.executeScript("arguments[0].click;", ccyDropdown_USD);
                ccyDropdown_USD.click();
                System.out.println("Triggered USD checkbox");

                wait.until(ExpectedConditions.elementToBeClickable(ccyDropdown_GBP));
                js.executeScript("arguments[0].click;", ccyDropdown_GBP);
                ccyDropdown_GBP.click();
                System.out.println("Triggered GBP checkbox");
            }
        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }

    }

    public void clickExpandBtn() {

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", expandBtn);
        expandBtn.click();
        System.out.println("Triggered expand button");

    }

    public void selectSector(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(sectorDropdown));
        js.executeScript("arguments[0].click;", sectorDropdown);
        sectorDropdown.click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        js.executeScript("arguments[0].click;", sectorDropdown_ALL);
        sectorDropdown_ALL.click();
        System.out.println("Unchecked sector - ALL");

        try {
            if (value.equalsIgnoreCase("Single")) {
                wait.until(ExpectedConditions.elementToBeClickable(sectorDropdown_Basic));
                //js.executeScript("arguments[0].click;", sectorDropdown_Banks);
                sectorDropdown_Basic.click();
                System.out.println("Triggered Sector - Basic Resources checkbox");
            }
            if (value.equalsIgnoreCase("Multiple")) {
                wait.until(ExpectedConditions.elementToBeClickable(sectorDropdown_Banks));
                //js.executeScript("arguments[0].click;", sectorDropdown_Banks);
                sectorDropdown_Banks.click();
                System.out.println("Triggered Sector - Covered checkbox");

                wait.until(ExpectedConditions.elementToBeClickable(sectorDropdown_Covered));
                //js.executeScript("arguments[0].click;", sectorDropdown_Covered);
                sectorDropdown_Covered.click();
                System.out.println("Triggered Sector - Banks checkbox");
            }
        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check item!");

        }


    }


    public void enterValueinInputField(String value, WebElement element) {

        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.clear();
        element.sendKeys(value);
        System.out.println("Entered " + value + " on input field...");

    }


    public void enterMultipleSecID(String value1, String value2) {

        wait.until(ExpectedConditions.elementToBeClickable(secIDInputField));

        String[] sec_IDs = new String[]{
                value1,
                value2
        };

        for (String secID : sec_IDs) {

            ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", secIDInputField);
            secIDInputField.sendKeys(sec_IDs);
            System.out.println("Entered value..");

        }

    }

    public void selectFromSide(String value) throws AWTException {
        wait.until(ExpectedConditions.visibilityOf(sideDropdown));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click;", sideDropdown);
        sideDropdown.click();
        System.out.println("Triggered side drop down list");
        Robot robot = new Robot();
        try {
            if (value.equals("BID")) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }
            if (value.equals("ASK")) {
                for (int i = 0; i < 2; i++) {
                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                }
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectFromType(String value) throws AWTException {

        wait.until(ExpectedConditions.visibilityOf(typeDropdown));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click;", typeDropdown);
        typeDropdown.click();
        System.out.println("Triggered type drop down list");
        Robot robot = new Robot();
        try {
            if (value.equalsIgnoreCase("AXE")) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }
            if (value.equalsIgnoreCase("INV")) {
                for (int i = 0; i < 2; i++) {
                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                }
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void checkSecurities_FromDashboard() {

        wait.until(ExpectedConditions.visibilityOf(checkItem1));
        try {
            if (addToWatchList.isDisplayed()) {


                for(int i = 0; i<=6;i++){

                }







                checkItem1.click();
                storeItem1 = isin1.getText();
                System.out.println("Check item 1 from the list");
                System.out.println("Stored: " + storeItem1 + " value for watch list checking");
                checkItem2.click();
                storeItem2 = isin2.getText();
                System.out.println("Stored: " + storeItem2 + " value for watch list checking");
                System.out.println("Check item 2 from the list");
                checkItem3.click();
                storeItem3 = isin3.getText();
                System.out.println("Stored: " + storeItem3 + " value for watch list checking");
                System.out.println("Check item 3 from the list");
                checkItem4.click();
                storeItem4 = isin4.getText();
                System.out.println("Check item 4 from the list");
                System.out.println("Stored: " + storeItem4 + " value for watch list checking");
                checkItem5.click();
                storeItem5 = isin5.getText();
                System.out.println("Stored: " + storeItem5 + " value for watch list checking");
                System.out.println("Check item 5 from the list");
                checkItem6.click();
                storeItem6 = isin6.getText();
                System.out.println("Stored: " + storeItem6 + " value for watch list checking");
                System.out.println("Check item 6 from the list");

            }
        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("can't check item from the list!");

        }

    }

    public void checkItemsFrom_DashBoard() {





    }

    public void checkSecurities_FromWatchList() {

        wait.until(ExpectedConditions.visibilityOf(selectWatchListItem1));
        try {
            if (removeToWatchList.isDisplayed()) {
                selectWatchListItem1.click();
                storeItem1WatchList = isin1WatchList.getText();
                System.out.println("Checked item 1 from the list");
                System.out.println("Stored: " + storeItem1WatchList + " value for watch list checking");
                selectWatchListItem2.click();
                storeItem2WatchList = isin2WatchList.getText();
                System.out.println("Stored: " + storeItem2WatchList + " value for watch list checking");
                System.out.println("Checked item 2 from the list");
                selectWatchListItem3.click();
                storeItem3WatchList = isin3WatchList.getText();
                System.out.println("Stored: " + storeItem3WatchList + " value for watch list checking");
                System.out.println("Checked item 3 from the list");
                selectWatchListItem4.click();
                storeItem4WatchList = isin4WatchList.getText();
                System.out.println("Stored: " + storeItem4WatchList + " value for watch list checking");
                System.out.println("Checked item 4 from the list");
                selectWatchListItem5.click();
                storeItem5WatchList = isin5WatchList.getText();
                System.out.println("Stored: " + storeItem5WatchList + " value for watch list checking");
                System.out.println("Checked item 5 from the list");
                selectWatchListItem6.click();
                storeItem6WatchList = isin6WatchList.getText();
                System.out.println("Stored: " + storeItem6WatchList + " value for watch list checking");
                System.out.println("Checked item 6 from the list");
            }
        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("can't check item from the list!");

        }

    }

    public void addSecurities() {
        wait.until(ExpectedConditions.elementToBeClickable(addToWatchList));
        addToWatchList.click();
        System.out.println("Triggered add to watchlist...");
    }

    public void removeSecurities() {

        wait.until(ExpectedConditions.elementToBeClickable(removeToWatchList));
        removeToWatchList.click();
        System.out.println("Triggered delete to watchlist...");

    }

    public void verifyWatchListMessage() {
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.visibilityOf(watchListUpdateMessage));

        try {
            if (watchListUpdateMessage.isDisplayed()) {
                Assert.assertTrue("Your watch list has been updated", true);
                System.out.println("Your watch list has been updated");

                //Click ok button to navigate to Watch List...
                wait.until(ExpectedConditions.visibilityOf(okToWatchList));
                okToWatchList.click();
                System.out.println("Triggered OK button..Redirecting to WatchList tab");
            }
        } catch (Exception e) {

            e.printStackTrace();
            Assert.fail("No securities added or removed to Watch list!");
            System.out.println("No securities added or removed to Watch list!");

        }

    }

    public void verifyTitlePage() {

        String title = "Watch List";

        try {
            if (title.equalsIgnoreCase(watchListHeader.getText())) {
                Assert.assertTrue("Page successfully navigated to: " + title, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Page did not load successfully");
        }


        if (title.equalsIgnoreCase(watchListHeader.getText())) {
            Assert.assertTrue("Successfully navigated to Watch List page...", true);
            System.out.println("Successfully navigated to Watch List page...");
        }

    }

    public void verifyRemovedSecuritiesWatchList() {
        String[] isins = new String[]{
                storeItem1WatchList,
                storeItem2WatchList,
                storeItem3WatchList,
                storeItem4WatchList,
                storeItem5WatchList,
                storeItem6WatchList
        };

        for (String isin : isins) {

            ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", secIDInputField);
            secIDInputField.sendKeys(isin);

            //Check if data is existing or not:

            try {
                if (gridDataChecking.isEnabled()) {
                    Assert.fail("Data is still existing on the grid!");
                    System.out.println("Data is still existing on the grid!");
                }
            } catch (NoSuchElementException e) {

                Assert.assertTrue(
                        "Value is no longer present in Watch List Tab", true);
                System.out.println("Value is no longer present in Watch List tab");

                clearBtn.click();
                System.out.println("Triggered clear button");
            }


        }


    }


    public void clickClearBtn() {

        wait.until(ExpectedConditions.elementToBeClickable(clearBtn));
        clearBtn.click();
        System.out.println("Triggered clear button");

    }

    public void checkIfInputFieldIsEmpty() {

        wait.until(ExpectedConditions.visibilityOf(secNameInputField));

        if (secNameInputField.getAttribute("value").isEmpty()) {
            Assert.assertTrue("Data has been cleared...", true);
            System.out.println("attribute" + secNameInputField.getAttribute("value"));
            System.out.println("Data has been cleared...");
        } else {
            Assert.fail("Data was not cleared!");
            System.out.println("Data was not cleared!");
        }


    }


    public void verifyAddedSecuritiesWatchList() throws InterruptedException {

        String[] isins = new String[]{
                storeItem1,
                storeItem2,
                storeItem3,
                storeItem4,
                storeItem5,
                storeItem6
        };

        for (String isin : isins) {

            //waitforPageLoaded();
            //wait.until(ExpectedConditions.elementToBeClickable(secIDInputField));
            ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", secIDInputField);
            secIDInputField.sendKeys(isin);

            Assert.assertTrue(
                    "Value is present in Watch List Tab", true);
            System.out.println("Value is present in Watch List tab");
            clearBtn.click();
            System.out.println("Trigged clear button");


            /*for (String test: isins){
                if(isinField.getText().equalsIgnoreCase(Arrays.toString(isins))){
                    Assert.assertTrue(
                            "Value is present in Watch List Tab", true);
                    System.out.println("Value is present in Watch List tab");

                    clearBtn.click();
                }

            }*/


           /* //driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
           // wait.until(ExpectedConditions.visibilityOf(isinRow));
            List<WebElement> isinData = driver.findElements(isinRow);

            for (WebElement webElement : isinData) {
                Thread.sleep(6000);
                try {
                    if (webElement.getText().equalsIgnoreCase(String.valueOf(isin))) {
                        System.out.println(String.valueOf(isin));
                        Assert.assertTrue(
                                "Value is present in Watch List Tab", true);
                        System.out.println("Value is present in Watch List tab");
                        clearBtn.click();
                    }
                }catch (Exception e){

                    e.printStackTrace();
                    Assert.fail("Value is not present in Watch List Tab!");
                    System.out.println("Value is not present in Watch List Tab!");

                }

            }*/


        }


    }

}
