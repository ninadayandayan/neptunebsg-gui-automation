package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class QuickSearchPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    public Robot robot = new Robot();
    public Actions action = new Actions(driver);

    AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);

    //elements:
    @FindBy(xpath = "//*[@id='simple-search-form']//*[@class='tt-input']")
    //*[@id='simple-search-form']/form/ul/li[2]/div/div/span/input[@placeholder='Quick search for Security']
    WebElement quickSearchInput;

    //widget:
    @FindBy(xpath = "//*[@id='panel_currency']")
    WebElement widget_Currency;

    @FindBy(xpath = "//div/p[@id='generic-modal-body-text']")
    WebElement dataCheck;

    @FindBy(xpath = "//*[@id='header-session-items']/ul/li[3]/a")
    WebElement userMenu;

    @FindBy(xpath = "//*[@id='ccy0']")
    WebElement panel_Currency_EUR;

    @FindBy(xpath = "//*[@id='ccy1']")
    WebElement panel_Currency_USD;

    @FindBy(xpath = "//*[@id='panel_currency']/div/header/h5/span/button[2]/i[@class='fa fa-chevron-up']")
    WebElement collapseUp;

    @FindBy(xpath = "//*[@id='panel_currency']/div/header/h5/span/button[2]/i[@class='fa fa-chevron-down']")
    WebElement collapseDown;

    @FindBy(xpath = "//ul[@id='facet-widget']/li[2]")

    WebElement rearrangedCurrency;
    @FindBy(xpath = "//*[@id='panel_currency']/div/header/h5/span/button[@class='btn btn-link btn-facet btn-facet-close']")

    WebElement closeWidget_currency;
    @FindBy(xpath = "//div/ul/li[@class='aside-widget2 aside-widget-facet hidden' and @id='panel_currency']")

    WebElement hiddenWidget_currency;
    @FindBy(xpath = "//*[@id='panel_currency']")

    WebElement sortable_currency;
    @FindBy(xpath = "//*[@id='panel_maturity']")

    WebElement sortable_maturity;

    @FindBy(xpath = "//*[@id='simple-search-form']/form/ul/li[2]/div/div/span[1]")
    WebElement quickSearchAutoCompleteVal;

    public QuickSearchPage(WebDriver driver) throws AWTException {
        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 10);
        //Initialize advanced search page object
        PageFactory.initElements(driver, this);

    }
    public void getValueFromSecID(String secID){

        quickSearchAutoCompleteVal.getText();
        if(quickSearchAutoCompleteVal.getText().equals(secID)){
            Assert.assertTrue("Value is a matched..",true);
            System.out.println("Value is a matched " + quickSearchAutoCompleteVal.getText());
        }

    }

    //functions:
    public void enterSecurityID(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        //key press:
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        waitforPageLoaded();
        System.out.println("Entered Sec ID: " + value);
        System.out.println("Triggered entered button from keypress");

        if(dataCheck.isDisplayed()){

            Assert.fail("No current axes/inventory on these securities.");
            System.out.println("No current axes/inventory on these securities.");

        }



        else  {

            Assert.assertTrue("Data is successfully retrieved!",true);
            System.out.println("Data is successfully retrieved!");
            waitforPageLoaded();

        }


    }

    public void enterAsSecName(String value) throws AWTException {

        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        for (int i = 0; i <= 2; i++) {
            robot.keyPress(KeyEvent.VK_DOWN);
            robot.keyRelease(KeyEvent.VK_DOWN);

        }
        System.out.println("Key press down complete!");

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        System.out.println("Key press enter complete!");

        if(dataCheck.isDisplayed()){

            Assert.fail("No current axes/inventory on these securities.");
            System.out.println("No current axes/inventory on these securities.");

        }
        else  {

            Assert.assertTrue("Data is successfully retrieved!",true);
            System.out.println("Data is successfully retrieved!");
            waitforPageLoaded();

        }


    }


    public void enterValueAsAutoCompleteinQuickSearch(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 3; i++) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

            }
            System.out.println("Key press down complete!");

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            waitforPageLoaded();

            System.out.println("Key press enter complete!");

            //check if data is existing or not..

            if(dataCheck.isDisplayed()){

                Assert.fail("No current axes/inventory on these securities.");
                System.out.println("No current axes/inventory on these securities.");

            }
            else  {

                Assert.assertTrue("Data is successfully retrieved!",true);
                System.out.println("Data is successfully retrieved!");
                waitforPageLoaded();

            }


        } catch (AWTException e1) {
            e1.printStackTrace();
        }

    }

    public void enterValueInQuickSearch(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        //key press:
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        waitforPageLoaded();
        System.out.println("Entered : " + value + " in Quick Search input field...");
        System.out.println("Triggered entered button from keypress");


        if(dataCheck.isDisplayed()){

            Assert.fail("No current axes/inventory on these securities.");
            System.out.println("No current axes/inventory on these securities.");

        }
        else  {

            Assert.assertTrue("Data is successfully retrieved!",true);
            System.out.println("Data is successfully retrieved!");
            waitforPageLoaded();

        }



    }
    public void enterValueasAutoCompleteHint3(String value){

        quickSearchInput.sendKeys(value);

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 3; i++) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

            }
            //System.out.println("Key press down complete!");

            for (int i = 0; i <= 2; i++) {

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
                //System.out.println("Key press enter complete!");

            }

            if(dataCheck.isDisplayed()){

                Assert.fail("No current axes/inventory on these securities.");
                System.out.println("No current axes/inventory on these securities.");

            }
            else  {

                Assert.assertTrue("Data is successfully retrieved!",true);
                System.out.println("Data is successfully retrieved!");
                waitforPageLoaded();

            }





            waitforPageLoaded();

        } catch (AWTException e1) {
            e1.printStackTrace();
        }



    }

    public void enterValueasAutoCompleteHint4(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 4; i++) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

            }
            //System.out.println("Key press down complete!");

            for (int i = 0; i <= 2; i++) {

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
                //System.out.println("Key press enter complete!");

            }

            if(dataCheck.isDisplayed()){

                Assert.fail("No current axes/inventory on these securities.");
                System.out.println("No current axes/inventory on these securities.");

            }
            else  {

                Assert.assertTrue("Data is successfully retrieved!",true);
                System.out.println("Data is successfully retrieved!");
                waitforPageLoaded();

            }





            waitforPageLoaded();

        } catch (AWTException e1) {
            e1.printStackTrace();
        }


    }

    public void enterValueAsAutoCompleteHint2(String value) {


        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        //js.executeScript("arguments[0].focus;", value);


        try {
            Robot robot = new Robot();

            for (int i = 0; i < 2; i++) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

            }
            System.out.println("Key press down complete!");

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            waitforPageLoaded();

            System.out.println("Key press enter complete!");

            if(dataCheck.isDisplayed()){

                Assert.fail("No current axes/inventory on these securities.");
                System.out.println("No current axes/inventory on these securities.");

            }
            else  {

                Assert.assertTrue("Data is successfully retrieved!",true);
                System.out.println("Data is successfully retrieved!");
                waitforPageLoaded();

            }



        } catch (AWTException e1) {
            e1.printStackTrace();
        }
    }
    public void enterValueasAutoCompletedHint7(String value){


        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 7; i++) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

            }
            System.out.println("Key press down complete!");

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            waitforPageLoaded();

            System.out.println("Key press enter complete!");

            //check if data is existing or not...

            if(dataCheck.isDisplayed()){

                Assert.fail("No current axes/inventory on these securities.");
                System.out.println("No current axes/inventory on these securities.");

            }
            else  {

                Assert.assertTrue("Data is successfully retrieved!",true);
                System.out.println("Data is successfully retrieved!");
                waitforPageLoaded();

            }




        } catch (AWTException e1) {
            e1.printStackTrace();
        }




    }

    public void enterValueAsAutoCompleteHint5(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(quickSearchInput));
        quickSearchInput.sendKeys(value);

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 5; i++) {
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

            }
            System.out.println("Key press down complete!");

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);

            waitforPageLoaded();

            System.out.println("Key press enter complete!");

            //check if data is existing or not...

            if(dataCheck.isDisplayed()){

                Assert.fail("No current axes/inventory on these securities.");
                System.out.println("No current axes/inventory on these securities.");

            }
            else  {

                Assert.assertTrue("Data is successfully retrieved!",true);
                System.out.println("Data is successfully retrieved!");
                waitforPageLoaded();

            }




        } catch (AWTException e1) {
            e1.printStackTrace();
        }

    }


    public void keypressEnter() {

        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", quickSearchInput);

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        System.out.println("Triggered enter in Quick Search input field");

        waitforPageLoaded();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        wait.until(ExpectedConditions.visibilityOf(advancedSearchPage.grid_SecID));
        if (advancedSearchPage.grid_SecID.isDisplayed()) {
            Assert.assertTrue("Search thru Key Press Enter is Complete", true);
            System.out.println("Search thru Key Press Enter is Complete");
        } else {
            Assert.fail("Data not Found!");
            System.out.println("Data not Found!");
        }

    }


    //widget functions:

    public void checkValueinWidget(String value) {

        wait.until(ExpectedConditions.visibilityOf(widget_Currency));

        try {
            if (value.equalsIgnoreCase("USD")) {

                panel_Currency_USD.click();
                System.out.println("Checked USD from Widget Currency");
            }
            if (value.equalsIgnoreCase("EUR")) {

                panel_Currency_EUR.click();
                System.out.println("Checked GBP from Widget Currency");
            }
        } catch (Exception e) {

            e.printStackTrace();
            System.out.println("Can't check element!");

        }

    }

    public void rearrangeWidget() {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        action.moveToElement(sortable_currency).build().perform();
        wait.until(ExpectedConditions.visibilityOf(sortable_currency));

        int xOffsetA = sortable_currency.getLocation().getX();
        int yOffsetA = sortable_currency.getLocation().getX();

        System.out.println("xoffsetA" + xOffsetA + "yoffsetA" + yOffsetA);

        int xOffsetB = sortable_maturity.getLocation().getX();
        int yOffsetB = sortable_maturity.getLocation().getY();

        System.out.println("xoffsetB" + xOffsetB + "yoffsetB" + yOffsetB);

        xOffsetB = (xOffsetB - xOffsetA) + 10;
        yOffsetB = (yOffsetB - yOffsetA) + 20;


        (new Actions(driver)).dragAndDropBy(sortable_currency, xOffsetB, yOffsetB).perform();
        System.out.println("Drag and Drop element");

        /*try{
            if(rearrangedCurrency.isDisplayed()){
                System.out.println("Successfully rearranged element");
            }
        }catch (Exception e){
            Assert.fail("Didn't rearranged element!");

        }*/

    }

    public void closeWidget() {
        wait.until(ExpectedConditions.visibilityOf(closeWidget_currency));

        closeWidget_currency.click();
        if (hiddenWidget_currency.isDisplayed()) {
            Assert.assertTrue("Closed Widget", true);
            System.out.println("Closed widget");

        } else {
            Assert.fail("Can't close widget!");
            System.out.println("Can't close widget!");
        }

    }

    public void collapseWidget() {

        action.moveToElement(collapseUp).build().perform();
        wait.until(ExpectedConditions.visibilityOf(collapseUp));
        collapseUp.click();
        System.out.println("Collapsed Widget");
    }

}


