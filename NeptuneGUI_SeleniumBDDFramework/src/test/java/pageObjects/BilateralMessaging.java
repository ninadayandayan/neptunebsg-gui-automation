package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BilateralMessaging extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    public Actions action = new Actions(driver);

    //WebElements:
    @FindBy(xpath = "//*[@id='generic-modal-body-text']")
    WebElement bmMessage;

    @FindBy(xpath = "//*[@id='content']/div[1]/h4")
    WebElement bmHeader;

    @FindBy(xpath = "//*[@id='nav-tabs-blotter']/a")
    WebElement bilateralMessagingTab;

    public BilateralMessaging(WebDriver driver) {
        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 30);

        PageFactory.initElements(driver, this);
    }

    //Functions:
    public void validateHeader(String title){
        wait.until(ExpectedConditions.visibilityOf(bmHeader));

        if(bmHeader.getText().equals(title)){

            Assert.assertTrue("Messaging History page is loaded..",true);
            System.out.println("Messaging History page is loaded..");
        }
        else
        {
            Assert.fail("Messaging History did not load!");
            System.out.println("Messaging History did not load!");
        }
    }

    public void validateMessage(String message) {

        wait.until(ExpectedConditions.visibilityOf(bmMessage));

        try {
            if (bmMessage.getText().equals(message)) {
                Assert.assertTrue("No BM Negotations found!", true);
                System.out.println("No BM Negotations found!");
            }
        } catch (Exception e) {
            Assert.fail("Error message was not displayed!");
            System.out.println("Error message was not displayed");
        }

    }

    public void navigateToBM(){
        try {
           if(bilateralMessagingTab.isDisplayed()){
                js.executeScript("arguments[0].click();", bilateralMessagingTab);
                bilateralMessagingTab.click();
            }
        }catch (Exception e){

            e.printStackTrace();
            System.out.println("Can't click element!");
        }
    }
}
