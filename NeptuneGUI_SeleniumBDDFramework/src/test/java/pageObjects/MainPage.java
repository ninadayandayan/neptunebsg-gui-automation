package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    public Actions action = new Actions(driver);

    @FindBy(xpath = "//div[@class='slick-cell l1 r1']/span")
    WebElement gridResult_SecID;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(text(),'BAYNGR')]")
    WebElement gridResult_SecName;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l18 r18')]")
    WebElement gridResult_Entity;

    @FindBy(xpath = "//div[@class='slick-cell l9 r9']/span")

    WebElement gridResult_SIDE;

    @FindBy(xpath = "//div[@class='slick-cell l15 r15 right-aligned']")

    WebElement gridResult_Yield;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(text(),'EUR')]")
    WebElement gridResult_CCY;

    @FindBy(xpath = "//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Entity Name')]")
    WebElement entityName;

    @FindBy(xpath = "//*[@id='generic-modal-body-text']")
    WebElement noAxesMessage;

    @FindBy(xpath = "//div[@class='slick-cell l13 r13']")
    WebElement gridResult_CCY_IE;

    @FindBy(xpath = "//div[@class='slick-cell l8 r8']/strong")
    WebElement gridResult_Dealer;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(text(),'Basic Resources')]")
    WebElement gridResult_Sector;

    @FindBy(xpath = "//div[@class='slick-cell l4 r4']")
    WebElement gridResult_EM;

    @FindBy(xpath = "//div[@class='slick-cell l5 r5']")
    WebElement gridResult_FRN;

    @FindBy(xpath = "//div[@class='slick-cell l26 r26']")
    WebElement gridResult_Region;

    @FindBy(xpath = "//div[@class='slick-cell l25 r25']")
    WebElement gridResult_CtryRisk;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(text(),'Australia')]")
    WebElement gridResult_CtryIssuer;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(text(),'Corporate')]")
    WebElement gridResult_AssetClass;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(text(),'Senior')]")
    WebElement gridResult_Seniority;

    @FindBy(xpath = "//div[@class='slick-cell l10 r10']")
    WebElement gridResult_Status;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[6]/button[@title='Fullscreen Table']")
    WebElement fullScreenbtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']//div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li/button[contains(@title,'Expand Table')]")
    WebElement expandTablebtn;

    @FindBy(xpath = "//button[@class='btn btn-link dropdown-toggle recent-searches']")
    WebElement recentSearchBtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[1]/input")
    WebElement securityIDField;

    @FindBy(xpath = "//div[contains(@id,'Yield')]/div[1]")
    WebElement sortYield;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[2]/div/section/div/div/div/ul/li[4]/span[@class='info']")
    WebElement marketDepth_info;

    @FindBy(xpath = "//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Trxn')]")
    WebElement statusColumn;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l10 r10')]/span")
    WebElement cancelStat;

    @FindBy(xpath = "//*[@id='dashboard-source-summary-region']/div/div[1]/div/form/div/div[6]/label/input[@name='excl_cancels']")
    WebElement cancelCheckBox_MarketDepth;

    @FindBy(xpath = "//*[@id='dashboard-source-summary-region']/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div/div[9]/span")
    WebElement cancelStat_youBuy;

    @FindBy(xpath = "//*[@id='dashboard-source-summary-region']/div/div[2]/div[1]/div/div[2]/div/div[5]/div/div/div[9]/span")
    WebElement cancelStat_youSell;

    @FindBy(xpath = "//*[@id='dashboard-instrument-history-region']/div/div[1]/div/form[3]//*[contains(@name,'excl_cancels')]")
    WebElement exclCancelsChkBox_IH;

    @FindBy(xpath = "//*[@id='dashboard-instrument-history-region']/div/div[2]/div/div[2]/div/div/div[5]/div/div/div[1]/span[@class='toggle expand']")
    WebElement expandToggle_IH;

    @FindBy(xpath = "//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l10 r10')]/span")
    WebElement cancelStatus_MarketDepth;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[2]/div/section/div/div/div/ul/li[2]/a")
    WebElement intradayTab;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div/div[7]")
    WebElement timeStampVal;

    @FindBy(xpath = "//*[@id='dashboard-instrument-history-region']/div/div[2]/div/div[2]/div/div/div[5]/div/div/div[2]")
    WebElement timeStamp_intradayVal;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div/div[13]")
    WebElement amountVal;

    @FindBy(xpath = "//*[@id='dashboard-instrument-history-region']/div/div[2]/div/div[2]/div/div/div[5]/div/div/div[5]")
    WebElement amount_intradayVal;

    @FindBy(xpath = "//div[@class='grid-canvas']//*[contains(@class,'ui-widget-content slick-row')]//div[@class='slick-cell l1 r1']")
    WebElement gridDataChecking;

    @FindBy(xpath = "//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Bench Spd')]/span")
    WebElement bench_WE;

    @FindBy(xpath = "//ul/li/a[@class='recent-search-link']")
    WebElement recentQuery_WE;

    //Columns - Data Table:
    By recentSearchResult = By.xpath("//ul/li/a[@class='recent-search-link']");
    By gridData = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]");
    By typeColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Type')]");
    By isinColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'ISIN')]");
    By secNameColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Sec Name')]");
    By emColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Emerging Market')]");
    By frnColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'FRN')]");
    By dealerColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Source Desc')]");
    By gridStatusColumn = By.xpath("//*[contains(@class,'ui-widget-content slick-row-odd')]//*[contains(@class,'slick-cell l1 r1')]");
    By statusColumn_Dashboard = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Trxn')]");
    By statusColumnMD = By.xpath("//*[@id='dashboard-source-summary-region']/div/div[2]/div[2]/div/div[2]/div//*[contains(@id,'Trxn')]");
    By sideColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Side')]");
    By ccyColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Ccy')]");
    By sectorColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Sector')]");
    By assetClassColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Asset Class')]");
    By seniorityColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Seniority')]");
    By ctryofRiskColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Country of Risk')]");
    By ctryofIssuerColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Country of Issuer')]");
    By regionColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Region')]");
    By entityNameColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[contains(@id,'Entity Name')]");
    //Rows - Data table:
    By isinRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]");
    By cusipRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l2 r2')]");
    By secNameRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l3 r3')]");
    By emRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l4 r4')]");
    By frnRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l5 r5')]");
    By creditRating = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l6 r6')]");
    By timestampRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l7 r7')]");
    By dealerRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l8 r8')]/strong");
    By typeRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l8 r8')]");
    By sideRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l9 r9')]");
    By statusRow_Dashboard = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l10 r10')]");
    By statusRowMD = By.xpath("//*[@id='dashboard-source-summary-region']//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l8 r8')]");
    By statusRowIH = By.xpath("//*[@id='dashboard-instrument-history-region']//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l3 r3')]");
    By ccyRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l11 r11')]");
    By amountRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l13 r13')]");
    By entityNameRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l18 r18')]");
    By sectorRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l21 r21')]");
    By assetClassRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l22 r22')]");
    By seniorityRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l23 r23')]");
    By ctryofRiskRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l24 r24')]");
    By ctryofIssuerRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l25 r25')]");
    By regionRow = By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l26 r26')]");


    public MainPage(WebDriver driver) {
        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 60);
        //initialize page object
        PageFactory.initElements(driver, this);
    }

    public void clickFUllScreen() throws InterruptedException {
        js.executeScript("arguments[0].click();", fullScreenbtn);
        fullScreenbtn.click();
        Thread.sleep(9000);
        System.out.println("Triggered Full Screen View");
    }

    public void clickExpand() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(expandTablebtn));
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", expandTablebtn);
        js.executeScript("arguments[0].click;", expandTablebtn);
        //expandTablebtn.click();
        System.out.println("Triggered expand table");
    }

    public void clickYield() {
        js.executeScript("arguments[0].click;", sortYield);
        sortYield.click();
        System.out.println("Triggered yield sort");
    }

    public void selectspecificIndication() {

        List<WebElement> element = driver.findElements(gridStatusColumn);
        System.out.println(element.get(1));
        element.get(1).click();
        System.out.println("Selected Indication...");
    }

    public void selectIndication() {
        wait.until(ExpectedConditions.elementToBeClickable(gridResult_SecID));
        js.executeScript("arguments[0].click;", gridResult_SecID);
        gridResult_SecID.click();
        System.out.println("Selected indication from the search result");

        //Validate market depth tab
        wait.until(ExpectedConditions.visibilityOf(marketDepth_info));
        if (marketDepth_info.isDisplayed()) {
            Assert.assertTrue("Market Depth has been loaded", true);
            System.out.println("Market Depth has been loaded");
        } else {
            Assert.fail("Market Depth DID not LOAD!");
            System.out.println("Market Depth DID not LOAD!");
        }

    }

    public void getValueinDashboard() {

        String timestamp = "";

    }


    public void selectIntradayHistory() {

        wait.until(ExpectedConditions.visibilityOf(intradayTab));
        intradayTab.click();
        System.out.println("Triggered Intraday Tab");

    }

    public void setExclCancelsChkBox_Intraday() {

        try {
            if (exclCancelsChkBox_IH.isDisplayed()) {
                wait.until(ExpectedConditions.elementToBeClickable(exclCancelsChkBox_IH));
                //js.executeScript("arguments[0].click;", exclCancelsChkBox_IH);
                exclCancelsChkBox_IH.click();
                System.out.println("unchecked excl. cancels checkbox in Intraday History");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to uncheck element!");
        }

        //expand toggle
        wait.until(ExpectedConditions.visibilityOf(expandToggle_IH));
        js.executeScript("arguments[0].click;", expandToggle_IH);
        expandToggle_IH.click();
        System.out.println("Triggered toggle expand..");

    }

    public void checkItems_FromDashBoard() {


    }

    public void validateIntraday() {

        String timeStamp_dashboardgrid = timeStampVal.getText();
        String timeStamp_intraday = timeStamp_intradayVal.getText();
        String amount_dashboardgrid = amountVal.getText();
        String amount_intraday = amount_intradayVal.getText();

        //compare timestamp
        try {
            if (timeStamp_dashboardgrid.equals(timeStamp_intraday)) {
                Assert.assertTrue("Time stamp value is a MATCH..", true);
                System.out.println("Time stamp value is a MATCH..");
                System.out.println("Time stamp on dashboard grid is: " + timeStamp_dashboardgrid);
                System.out.println("Time stamp on intraday is: " + timeStamp_intraday);
            }
        } catch (Exception e) {

            Assert.fail("Time stamp value did not match!");
            System.out.println("Time stamp value did not match!");
        }
        //compare amount
        try {
            if (amount_dashboardgrid.equals(amount_intraday)) {
                Assert.assertTrue("Amount value is a MATCH..", true);
                System.out.println("Amount value is a MATCH..");
                System.out.println("Amount on dashboard grid is: " + amount_dashboardgrid);
                System.out.println("Amount on intraday is: " + amount_intraday);
            }
        } catch (Exception e) {

            Assert.fail("Amount value did not match!");
            System.out.println("Amount value did not match!");
        }


    }

    public void uncheckCXL_MarketDepth() {

        wait.until(ExpectedConditions.elementToBeClickable(cancelCheckBox_MarketDepth));
        cancelCheckBox_MarketDepth.click();
        System.out.println("Triggered cancel checkbox");
    }

    public void checkStatus_MarketDepth(String value) {

        wait.until(ExpectedConditions.visibilityOf(cancelStat_youBuy));
        if (value.equals(cancelStat_youBuy.getText())) {
            Assert.assertTrue(" Status" + value + "is displayed...", true);
            System.out.println(" Status " + value + " is displayed... ");
        } else if (value.equals(cancelStat_youSell.getText())) {
            Assert.assertTrue(" Status" + value + "is displayed...", true);
            System.out.println(" Status " + value + " is displayed... ");
        } else {
            Assert.fail(" Status " + cancelStat_youBuy.getText() + " is displayed" + "instead of " + value);
            Assert.fail(" Status " + cancelStat_youSell.getText() + " is displayed" + "instead of " + value);
        }
    }

    public void checkStat_MD(String value) {

        List<WebElement> entityName_Column = driver.findElements(statusColumnMD);
        for (WebElement trElement : entityName_Column) {
            List<WebElement> td_collection = trElement.findElements(statusRowMD);
            for (WebElement webElement : td_collection)
                try {
                    if (webElement.getText().equalsIgnoreCase(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }
                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void checkStatus_IntradayHistory(String value) {

        List<WebElement> status_ColumnMD = driver.findElements(statusColumnMD);
        int row_num = 1, col_num = 1;
        for (WebElement trElement : status_ColumnMD) {
            List<WebElement> td_collection = trElement.findElements(statusRowIH);
            System.out.println(td_collection.size());
            for (WebElement webElement : td_collection)
                if (webElement.getText().equals(value)) {
                    Assert.assertTrue(" Data " + value + " is present on the grid...", true);
                    System.out.println("Data " + value + " is present on the grid...");
                } else {
                    //Assert.fail(" Data is NOT present on the grid! ");
                    System.out.println("Data is NOT present on the grid!");
                }
        }

        for (WebElement tdElement : status_ColumnMD) {
            System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
            row_num++;
        }

    }

    public void clickExpandScreen() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        //wait.until(ExpectedConditions.visibilityOf(expandTablebtn));
        //click fit to screen
        expandTablebtn.click();
        wait.until(ExpectedConditions.visibilityOf(gridResult_Status));
        if (gridResult_Status.isDisplayed()) {
            Assert.assertTrue("Expand Screen is now ENABLED", true);
            System.out.println("Expand Screen is now ENABLED");

            expandTablebtn.click();
            System.out.println("Expand Screen is now DISABLED..");

        } else {
            Assert.fail("Expand screen DID not triggered!");
        }

    }

    public void clickFitToScreen() throws InterruptedException {
        //click fit to screen
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        //wait.until(ExpectedConditions.elementToBeClickable(fullScreenbtn));
        //js.executeScript("arguments[0].click;", fullScreenbtn);
        fullScreenbtn.click();
        System.out.println("Triggered full screen button");
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //wait.until(ExpectedConditions.visibilityOf(gridResult_Status));

        try {
            System.out.println(bench_WE.isDisplayed());
            if ((bench_WE.isDisplayed())) {
                Assert.assertTrue("Full Screen is now ENABLED", true);
                System.out.println("Full Screen is now ENABLED");

                action.doubleClick(bench_WE);
                System.out.println("FUll Screen is now DISABLED");
            }
        } catch (Exception e) {

            Assert.fail("FULL screen did not triggered!");
            e.printStackTrace();

        }

    }


    public void selectValuefropDropdownList(String value) {
        WebElement select = driver.findElement(By.xpath("//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul"));
        List<WebElement> options = select.findElements(By.tagName("li"));
        for (WebElement option : options) {
            if (value.equals(option.getText()))
                option.click();
        }
    }

    public void scrollScreen() {

    }

    public void checkLoop() {

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 31; i++) {
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
            }

        } catch (AWTException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        System.out.println("Key press complete!");

    }

    public void initialLoop() {

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 3; i++) {
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
            }

        } catch (AWTException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        System.out.println("Key press complete!");
        waitforPageLoaded();

    }


    public void checkMidLoop() {

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 25; i++) {
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
            }

        } catch (AWTException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        System.out.println("Key press complete!");

    }

    //Verification Part:
    public void verifyGrid_EntityName(String value) {

        List<WebElement> entityName_Column = driver.findElements(entityNameColumn);
        for (WebElement trElement : entityName_Column) {
            List<WebElement> td_collection = trElement.findElements(entityNameRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value)) {
                        Assert.assertTrue("Search Complete: " + value,true);
                        System.out.println("Data " + value + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }





       /* ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", gridResult_Entity);
        wait.until(ExpectedConditions.visibilityOf(gridResult_Entity));
        System.out.println("Entity Name is visible");

        try {

            if (!gridResult_Entity.getText().contains(value)) {
                wait.until(ExpectedConditions.visibilityOf(gridResult_Entity));

                System.out.println("Entity Name is visible again");
                Assert.assertTrue("Search Complete", true);
                System.out.println("Search Complete" + "Value displayed is: " + value);
            }
            if (!gridResult_Entity.getText().contains(value)) {
                wait.until(ExpectedConditions.visibilityOf(gridResult_Entity));

                System.out.println("Entity Name is visible again");
                Assert.assertTrue("Search Complete", true);
                System.out.println("Search Complete" + "Value displayed is: " + value);
            }

        } catch (Exception e) {

            Assert.fail("Data not found");
            System.out.println("Data not found!");
            e.printStackTrace();

        }*/

    }

    public void verifyGrid_SIDE(String value) {

        List<WebElement> side_Column = driver.findElements(sideColumn);
        for (WebElement trElement : side_Column) {
            List<WebElement> td_collection = trElement.findElements(sideRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }
                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_Type(String value) {
        List<WebElement> type_Column = driver.findElements(typeColumn);
        for (WebElement trElement : type_Column) {
            List<WebElement> td_collection = trElement.findElements(typeRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }
                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }


    public void verifyGrid_Min(String value) {

        if (gridResult_Yield.getText().contains(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found");
            System.out.println("Data not found!");
        }

    }

    public void verifySortYield() {
        String temp = null;
        /**
         * Retrieve the List of Items in the Table before Sorting and Store into Array
         */
        List<WebElement> tdList = driver.findElements(By.xpath("//div[@class='slick-cell l15 r15 right-aligned']"));
        String strArray[] = new String[tdList.size()];
        for (int i = 0; i < tdList.size(); i++) {
            System.out.println(tdList.get(i).getText());
            strArray[i] = tdList.get(i).getText();
        }

        /**
         * Sort the Array by Swapping the Elements
         */
        for (int i = 0; i < strArray.length; i++) {
            for (int j = i + 1; j < strArray.length; j++) {
                if (strArray[i].compareTo(strArray[j]) > 0) {
                    temp = strArray[i];
                    strArray[i] = strArray[j];
                    strArray[j] = temp;
                }
            }
        }

        /**
         * Printing the Values after sorting
         */
        System.out.println("##################Sorted values in the Array####################");
        for (int i = 0; i < strArray.length; i++) {

            System.out.println(strArray[i]);
        }


    }

    public void verifyGrid_secName(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_SecName));
        if (gridResult_SecName.getText().contains(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "\n" + "Value displayed is: " + value);
        } else if (gridResult_SecName.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete " + "\n" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }
    }

    public void verifyGrid_Status_Dashboard(String value) {
        wait.until(ExpectedConditions.visibilityOf(statusColumn));

        try {
            if (statusColumn.isDisplayed()) {
                js.executeScript("arguments[0].focus;", statusColumn);
                //js.executeScript("arguments[0].click;", statusColumn);
                statusColumn.click();
                System.out.println("Sort Status....");
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

                //check result:
                if (cancelStat.getText().equals(value)) {
                    Assert.assertTrue("Search Cancelled Status Completed: ", true);
                    System.out.println("Search Cancelled Status Completed" + "\n " + " Value displayed is: " + value);
                }
            }
        } catch (Exception e) {

            Assert.fail("Data not found");
            System.out.println("Data not found!");
        }

    }

    public void sortStatus() {

        try {
            if (statusColumn.isDisplayed()) {

                js.executeScript("arguments[0].focus;", statusColumn);
                //js.executeScript("arguments[0].click();", statusColumn);
                statusColumn.click();
                System.out.println("Sort Status....");
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            }

        } catch (Exception e) {

            e.printStackTrace();
            Assert.fail("Can't sort element...");
            System.out.println("Can't sort element...");


        }

    }

    public void verifyDefaultResultInWatchList() {


    }


    public void verifyGrid_Status(String value) {

        List<WebElement> entityName_Column = driver.findElements(statusColumn_Dashboard);
        //int row_num = 1, col_num = 1;
        for (WebElement trElement : entityName_Column) {
            List<WebElement> td_collection = trElement.findElements(statusRow_Dashboard);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }
                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }


    }


    public void verifyMarketDepth_Status(String value) {


    }

    public void verifyGrid_PartialSecID(String partial_value) {

        //get main grid
        WebElement main_grid = driver.findElement(By.xpath("//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div"));
        //get rows:
        By gridRows = By.xpath("//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div//*[contains(@class,'ui-widget-content slick-row')]");
        List<WebElement> allRows = main_grid.findElements(gridRows);
        //loop through each row and compare actual value with expected value no.
        for (WebElement row : allRows) {
            //Get the column
            By isinColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[1]");

            String actualValue = row.findElement(isinColumn).getText();
            //Compare actual vs. expected
            if (actualValue.equals(partial_value)) {
                row.click();
                System.out.println("Selected row " + (allRows.indexOf(row) + 1) + " having Actual. No. " + partial_value);
                break;
            }
        }
    }

    public void testColumnAgain() {
        List<WebElement> isin_Column = driver.findElements(By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[1]"));
        System.out.println("NUMBER OF ROWS IN THIS TABLE = " + isin_Column.size());
        int row_num = 0, col_num = 0;
        for (WebElement trElement : isin_Column) {
            List<WebElement> td_collection = trElement.findElements(By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]/span"));
            //List<WebElement> td_collection = trElement.findElements(By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]/span"));
            System.out.println(td_collection.size());
            for (int i = 0; i < td_collection.size(); i++)
                System.out.println(td_collection.get(i).getText());
            System.out.println("NUMBER OF COLUMNS=" + td_collection.size());

            for (WebElement tdElement : isin_Column) {
                System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
                col_num++;
            }
            row_num++;
        }

    }

    public void verifyGrid_MultipleEntityName(String value1, String value2) throws InterruptedException {

        List<WebElement> entityName_Column = driver.findElements(entityNameColumn);
        for (WebElement trElement : entityName_Column) {
            List<WebElement> td_collection = trElement.findElements(entityNameRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_MultipleScenarios(String value1, String value2, By columnName, By rowName) {

        List<WebElement> test_Column = driver.findElements(columnName);
        int row_num = 1, col_num = 1;
        for (WebElement trElement : test_Column) {
            List<WebElement> td_collection = trElement.findElements(rowName);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
        for (WebElement tdElement : test_Column) {
            System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
            row_num++;
        }


    }

    public void verifyGridSEC_ID(String value) {
        List<WebElement> isin_Column = driver.findElements(isinColumn);
        int row_num = 1, col_num = 1;
        for (WebElement trElement : isin_Column) {
            List<WebElement> td_collection = trElement.findElements(isinRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }


    public void verifyGrid_MultipleSecID(String value1, String value2) {

        List<WebElement> isin_Column = driver.findElements(isinColumn);
        for (WebElement trElement : isin_Column) {
            List<WebElement> td_collection = trElement.findElements(isinRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().contains(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().contains(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_MultipleSecName(String value) {

        List<WebElement> sec_Column = driver.findElements(secNameColumn);
        //System.out.println("NUMBER OF ROWS IN THIS COLUMN = " + ccy_Column.size());
        for (WebElement trElement : sec_Column) {
            List<WebElement> td_collection = trElement.findElements(secNameRow);
            int n = td_collection.size();
            System.out.println("Number of Rows in this COLUMN: " + n);
            for (WebElement webElement : td_collection)
                if (webElement.getText().equals(value)) {
                    System.out.println("Data " + value + " is present on the grid...");
                } else {
                    Assert.fail("Data is not present on the grid!");
                    System.out.println("Data is not present on the grid!");
                }
        }
    }


    public void verifyGrid_MultipleCCy(String value1, String value2) {

        List<WebElement> ccy_Column = driver.findElements(ccyColumn);
        for (WebElement trElement : ccy_Column) {
            List<WebElement> td_collection = trElement.findElements(ccyRow);
            for (WebElement webElement : td_collection)
                try {
                    if (webElement.getText().equals(value2)) {
                        Assert.assertTrue("Search Complete: " +value2,true);
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        Assert.assertTrue("Search Complete: " +value1,true);
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }

        }
    }

    public void verifyGrid_MultipleSector(String value1, String value2) {
        List<WebElement> sector_Column = driver.findElements(sectorColumn);
        for (WebElement trElement : sector_Column) {
            List<WebElement> td_collection = trElement.findElements(sectorRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equalsIgnoreCase(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }
                    if (webElement.getText().equalsIgnoreCase(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_MultipleRegion(String value1, String value2) {
        List<WebElement> region_Column = driver.findElements(regionColumn);
        for (WebElement trElement : region_Column) {
            List<WebElement> td_collection = trElement.findElements(regionRow);
            System.out.println("NUMBER OF ROWS = " + td_collection.size());
            for (WebElement webElement : td_collection)
                try {
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_MultipleCtryIssuer(String value1, String value2) {
        List<WebElement> ctryIssuer_Column = driver.findElements(ctryofIssuerColumn);
        //int row_num = 1, col_num = 1;
        for (WebElement trElement : ctryIssuer_Column) {
            List<WebElement> td_collection = trElement.findElements(ctryofIssuerRow);
            System.out.println("NUMBER OF ROWS = " + td_collection.size());
            for (WebElement webElement : td_collection)
                try {
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_MultipleCtryofRisk(String value1, String value2) {
        List<WebElement> risk_Column = driver.findElements(ctryofRiskColumn);
        for (WebElement trElement : risk_Column) {
            List<WebElement> td_collection = trElement.findElements(ctryofRiskRow);
            System.out.println("NUMBER OF ROWS = " + td_collection.size());
            for (WebElement webElement : td_collection)
                try {
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyGrid_MultipleSeniority(String value1, String value2) {

        List<WebElement> risk_Column = driver.findElements(seniorityColumn);
        for (WebElement trElement : risk_Column) {
            List<WebElement> td_collection = trElement.findElements(seniorityRow);
            System.out.println("NUMBER OF ROWS in this COLUMN =" + td_collection.size());
            for (WebElement webElement : td_collection) {
                try {
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }

            }
        }


    }


    public void verifyGrid_MultipleAssetClass(String value1, String value2) {
        List<WebElement> asset_Column = driver.findElements(assetClassColumn);
        for (WebElement trElement : asset_Column) {
            List<WebElement> td_collection = trElement.findElements(assetClassRow);
            System.out.println("NUMBER OF ROWS in this COLUMN =" + td_collection.size());
            for (WebElement webElement : td_collection) {
                try {
                    if (webElement.getText().equals(value2)) {
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        System.out.println("Data " + value1 + " is present on the grid...");
                    }

                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }

            }
        }
    }


    public void verifyGrid_MultipleSide(String value) {
        List<WebElement> side_Column = driver.findElements(sideColumn);
        int row_num = 1, col_num = 1;
        for (WebElement trElement : side_Column) {
            List<WebElement> td_collection = trElement.findElements(sideRow);
            System.out.println("NUMBER OF ROWS in this COLUMN =" + td_collection.size());
            for (WebElement webElement : td_collection) {
                String outputVal = webElement.getText();
                //System.out.println(outputVal);
                if (outputVal.equals(value)) {
                    System.out.println(" Data: " + value + " is present on the grid... ");
                } /*else {
                    Assert.fail(" Data is not present on the grid! ");
                    System.out.println("  Data is not present on the grid! ");
                }*/
            }
            //System.out.println(td_collection.get(i).getText());
            for (WebElement tdElement : side_Column) {
                System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
                col_num++;
            }
            row_num++;
        }
    }

    public void verifyGrid_secID(String value) throws InterruptedException, IOException {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOf(gridResult_SecID));
        if (gridResult_SecID.getText().equals(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else if (gridResult_SecID.getText().contains(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }


    public void verifyGrid_CCY(String value) {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOf(gridResult_CCY));
        if (gridResult_CCY.getText().equalsIgnoreCase(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void strongclass() {

    }

    public void getTextfromStrongClass() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Object load = js.executeScript("var value = document.evaluate(\"//div[@class='slick-cell l7 r7']/strong\",document, null, XPathResult.STRING_TYPE, null ); return value.stringValue;");
        System.out.println("Load Number : " + load.toString());

    }

    public void verifyGrid_Dealer(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_Dealer));
        System.out.println(gridResult_Dealer.getText());
        System.out.println(value);
        System.out.println(gridResult_Dealer.equals(value));
        if (gridResult_Dealer.getText().equals(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");

        }

    }

    public void verifyGrid_Side(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_SIDE));

        if (gridResult_SIDE.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }


    }

    public void verifyGrid_Sector(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_Sector));

        if (gridResult_Sector.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }
    }


    public void verifyGrid_Region(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_Region));

        System.out.println(gridResult_Region.getText());
        System.out.println(value);
        if (gridResult_Region.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_CtryIssuer(String value) {
        //((JavascriptExecutor) driver).executeScript("arguments[0].focus();", gridResult_CtryIssuer);
        //wait.until(ExpectedConditions.visibilityOf(gridResult_CtryIssuer));

        try {
            wait.until(ExpectedConditions.visibilityOf(gridResult_CtryIssuer));
            System.out.println(value);
            System.out.println(gridResult_CtryIssuer.getText().equals(value));
            if (gridResult_CtryIssuer.getText().equals(value)) {

                Assert.assertTrue("Search Complete ", true);
                System.out.println("Search Complete" + "\n" + "Value displayed is: " + value);
            } else {
                Assert.fail("Data not matched!");
                System.out.println("Data not matched!" + "\n" + "Value displayed is: " + value);

            }
        } catch (Exception e) {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
            e.printStackTrace();
        }
    }

    public void verifyGrid_CtryRisk(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_CtryRisk));
        if (gridResult_CtryRisk.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete " + "\n" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_AssetClass(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_AssetClass));
        System.out.println(gridResult_AssetClass.getText());
        if (gridResult_AssetClass.getText().equalsIgnoreCase(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete " + "\n" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_Seniority(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_Seniority));

        if (gridResult_Seniority.getText().equalsIgnoreCase(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }
    }

    public void verifyGrid_MultipleDealer(String value1, String value2) {

        List<WebElement> dealer_Column = driver.findElements(dealerColumn);
        for (WebElement trElement : dealer_Column) {
            List<WebElement> td_collection = trElement.findElements(dealerRow);
            for (WebElement webElement : td_collection)
                try {
                    if (webElement.getText().equals(value2)) {
                        Assert.assertTrue("Data " + value2 + " is present on the grid.... ", true);
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                    if (webElement.getText().equals(value1)) {
                        Assert.assertTrue("Data " + value1 + " is present on the grid.... ", true);
                        System.out.println("Data " + value2 + " is present on the grid...");
                    }
                } catch (Exception e) {
                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }


    }

    public void verifyGrid_EM(String value) {

        List<WebElement> em_Column = driver.findElements(emColumn);
        for (WebElement trElement : em_Column) {
            List<WebElement> td_collection = trElement.findElements(emRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equals(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }
                } catch (Exception e) {
                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }

    }

    public void verifyErrorMessage() {

        System.out.println(wait.until(ExpectedConditions.visibilityOf(noAxesMessage)));

        try {
            System.out.println(noAxesMessage.isDisplayed());
            if (noAxesMessage.isDisplayed()) {
                Assert.assertTrue("No current axes/inventory on these securities message is displayed", true);
                System.out.println("No current axes/inventory on these securities message is displayed");
            }
        } catch (Exception e) {

            Assert.fail("Error message was not displayed!");
            System.out.println("Error message was not displayed!");

        }

    }

    public void verifyDataONGrid() throws IOException {

        try {
            if (gridDataChecking.isEnabled()) {
                Assert.assertTrue("Data is existing on the grid..", true);

                System.out.println("Data is existing on the grid..");
            }
        } catch (NoSuchElementException e) {

            Assert.fail("No current axes/inventory on these securities...");
            System.out.println("No current axes/inventory on these securities...");
            takeScreenshot(driver);

        }

    }

    public void verifyGrid_FRN(String value) {

        List<WebElement> frn_Column = driver.findElements(frnColumn);
        for (WebElement trElement : frn_Column) {
            List<WebElement> td_collection = trElement.findElements(frnRow);
            for (WebElement webElement : td_collection)

                try {
                    if (webElement.getText().equalsIgnoreCase(value)) {
                        System.out.println("Data " + value + " is present on the grid...");
                    }
                } catch (Exception e) {

                    Assert.fail("DATA IS NOT PRESENT ON THE GRID!");
                    System.out.println("DATA IS NOT PRESENT ON THE GRID!");
                }
        }
    }

    public void verifyRecentSearch(String query) {
        wait.until(ExpectedConditions.elementToBeClickable(recentSearchBtn));


        try {
            if (recentSearchBtn.isDisplayed()) {

                recentSearchBtn.click();
                System.out.println("Triggered recent search button");
                List<WebElement> listOfRecentQuery = driver.findElements(recentSearchResult);
                //action.click(recentSearchBtn);

                for (int i = 1; i <= listOfRecentQuery.size(); i++) {

                    listOfRecentQuery = driver.findElements(recentSearchResult);
                    wait.until(ExpectedConditions.visibilityOf(listOfRecentQuery.get(i - 1)));
                    String recentQuery = recentQuery_WE.getText();
                    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

                    System.out.println(query);
                    System.out.println(recentQuery);
                    if (query.equals(recentQuery)) {
                        Assert.assertTrue("Query FOUND!", true);
                        System.out.println("Query FOUND!");
                    } else {
                        Assert.fail("QUERY not FOUND!");
                        System.out.println("QUERY not FOUND!");
                    }

                    System.out.println(i);

                }

            }


        } catch (Exception e) {

            e.printStackTrace();

        }
    }

}
