package pageObjects;

import Base.BaseUtil;
import com.sun.source.tree.AssertTree;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AdvancedSearchPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;

    //Initialize page objects:
    MainPage main = new MainPage(driver);

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[1]/input[@placeholder='Security ID']")

    WebElement securityIDField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[2]/input[@placeholder='Security Name']")

    WebElement securityNameField;

    @FindBy(xpath = "//button[@class='btn btn-primary search-form-performSearch']")

    WebElement searchBtn;

    @FindBy(xpath = "//*[@id='generic-modal-body-text']")

    WebElement dataCheck;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[6]/div/div[2]/input[@placeholder='Entity Name']")

    WebElement entityNameField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[2]/div/div/div/input[@name='yield_min']")

    WebElement minIDField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[2]/div/div/div/input[@name='yield_max']")

    WebElement maxIDField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/div/a")

    WebElement sideDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[2]/div/a")

    WebElement typeDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select/option[2]")

    WebElement ASK;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/button[@class='multiselect dropdown-toggle btn btn-default ccy-multiselect']")

    WebElement ccyDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[1]/a/label/input[1]")

    WebElement ccy_AllChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[3]/a/label/input[@value='EUR_top']")

    WebElement ccy_EURChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[4]/a/label/input")
    WebElement ccy_USDChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[5]/a/label/input")
    WebElement ccy_GBPChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/button")
    WebElement dealer_ChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement dealer_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[2]/a/label/input[@value='Etrading Software Bank A']")

    WebElement dealer_ETChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[5]/a/label/input")
    WebElement dealer_BankD;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[4]/a/label/input")
    WebElement dealer_BankC;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[3]/a/label/input")
    WebElement dealer_BankB;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[2]/a/label/input")
    WebElement dealer_BankA;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[6]/div/div[1]/div/label[2]/input[@name='excl_cancels']")

    WebElement exclCancelsChkBox;


    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/button")

    WebElement seniority_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement seniority_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[11]/a/label/input")

    WebElement seniority_SenChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[20]/a/label/input")

    WebElement seniority_UnsubordinatedChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[15]/a/label/input[@value='Subordinated']")
    WebElement seniority_Subordinated;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/button")

    WebElement sectorDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[1]/a/label/input[@value='ALL']")

    WebElement sector_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div/div/div/form/div/div/div/div/div/div/div/ul/li/a/label/input[@value='Banks']")
    WebElement sector_Banks;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[5]/a/label/input")
    WebElement sector_BasicResources;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div/div/div/form/div/div/div/div/div/div/div/ul/li/a/label/input[@value='Covered']")
    WebElement sector_Covered;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/button")
    WebElement region_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[1]/a/label/input")
    WebElement region_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[3]/a/label/input[@value='Asia']")
    WebElement region_Asia;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[7]/a/label/input[@value='Europe']")
    WebElement region_Europe;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[11]/a/label/input[@value='N.Amer']")
    WebElement region_NA;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/button")
    WebElement ctryIssuer_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[1]/div/input[@placeholder='Search']")
    WebElement ctryIssuer_txtField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[2]/a/label/input[@value='ALL']")
    WebElement ctryIssuer_ALL;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[195]/a/label/input[@value='United Arab Emirates']")
    WebElement ctryofIssuer_ArabE;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[13]/a/label/input[@value='Australia']")
    WebElement ctryOfIssuer_AUS;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[197]/a/label/input[@value='United States']")
    WebElement ctryIssuer_USA;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[196]/a/label/input[@value='United Kingdom']")
    WebElement ctryIssuer_UK;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/button")

    WebElement ctryRisk_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[2]/a/label/input[@value='ALL']")
    WebElement ctryRisk_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[1]/div/input[@placeholder='Search']")
    WebElement ctryRisk_textField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[196]/a/label/input[@value='United Kingdom']")
    WebElement ctryRisk_GBR;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[13]/a/label/input[@value='Australia']")
    WebElement ctryRisk_AUS;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[197]/a/label/input[@value='United States']")
    WebElement ctryRisk_USA;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[1]/input[@value='Asset Class (ALL)']")
    WebElement assetClass_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div[1]/div/ul/li[1]/span/input")
    WebElement assetClass_ALL;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div[1]/div/ul/li[4]/span/input[@data-value='Corporate']")
    WebElement assetClass_Corporate;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div[1]/div/ul/li[6]/span/input[@data-value='Financial']")

    WebElement assetClass_Financial;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[2]/ul/li[3]/span/input[@class='asset Agency']")

    WebElement assetClass_Agency;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[2]/ul/li[8]/span/input[@class='asset Municipal']")

    WebElement assetClass_Municipal;

    @FindBy(xpath = "//div[@class='slick-cell l1 r1']/span")

    WebElement grid_SecID;

    @FindBy(xpath = "//button[@class='btn btn-warning search-form-clearSearch']")

    WebElement clearBtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[1]/button")

    WebElement saveQuery;

    public AdvancedSearchPage(WebDriver driver)  {

        BaseUtil.driver = driver;
        wait = new WebDriverWait(driver, 60);
        //Initialize advanced search page object
        PageFactory.initElements(driver, this);

    }
    public void getValueFromSecIDField(String secID){

        securityIDField.getAttribute("value");
        if(securityIDField.getAttribute("value").equals(secID)){
            Assert.assertTrue("Value is a matched..",true);
            System.out.println("Value is a matched " + securityIDField.getAttribute("value"));
        }
        //System.out.println("Value in input field is: " + securityIDField.getAttribute("value"));
    }

    public void enterSecurityID(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(securityIDField));
        securityIDField.clear();
        System.out.println("Cleared value before input...");
        securityIDField.sendKeys(value);
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        System.out.println("Entered ISIN:" + value);
    }

    public void keyPress() throws AWTException {
        //wait.until(ExpectedConditions.visibilityOf(securityIDField));
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", securityIDField);

        Robot robot = new Robot();

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        waitforPageLoaded();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

        wait.until(ExpectedConditions.visibilityOf(entityNameField));
        if (grid_SecID.isDisplayed()) {
            Assert.assertTrue("Search thru Key Press Enter is Complete", true);
            System.out.println("Search thru Key Press Enter is Complete");
        } else {
            Assert.fail("Data not Found!");
            System.out.println("Data not Found!");
        }

    }

    public void clearSearchResults() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", clearBtn);
        System.out.println("Triggered clear button");
        //clearBtn.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //waitforPageLoaded();
        try {
            if(saveQuery.isDisplayed()) {
                //wait.until(ExpectedConditions.invisibilityOf(grid_SecID));
                Assert.assertTrue("Clear search results COMPLETED", true);
                System.out.println("Clear search results has been COMPLETED");
            }
        }catch(Exception e) {
            Assert.fail("Data was not cleared!");
            System.out.println("Data not cleared!");
        }
    }

    public void loopEnterSecID(String[] values, WebElement enterSecID) {
        for (String code : values) {
            enterSecID.sendKeys(code + "\n");
        }
    }

    public void enterSecurityName(String value) throws InterruptedException {

        //wait.until(ExpectedConditions.elementToBeClickable(securityNameField));
        securityNameField.sendKeys(value);
        Thread.sleep(6000);
        System.out.println("Entered Security Name is:" + value);

    }

    public void enterEntityName(String value) throws InterruptedException {
        entityNameField.clear();
        System.out.println("Cleared value before input....");
        entityNameField.sendKeys(value);
        Thread.sleep(6000);
        System.out.println("Entered Entity Name is:" + value);
    }

    public void enterMinandMax(String minValue, String maxValue) {

        minIDField.sendKeys(minValue);
        System.out.println("Entered min value is: " + minValue);

        maxIDField.sendKeys(maxValue);
        System.out.println("Entered max value is: " + maxValue);

    }


    public void enterMinValue(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(minIDField));
        minIDField.sendKeys(value);
        System.out.println("Entered min value is: " + value);
    }

    public void enterMaxValue(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(maxIDField));
        maxIDField.sendKeys(value);
        System.out.println("Entered max value is: " + value);

    }
    public void setExclCancelsChkBox(){

        try {
            if(exclCancelsChkBox.isDisplayed()){
                wait.until(ExpectedConditions.elementToBeClickable(exclCancelsChkBox));
                js.executeScript("arguments[0].click;", exclCancelsChkBox);
                exclCancelsChkBox.click();
                System.out.println("unchecked excl. cancels checkbox");
            }

        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Unable to uncheck element!");
        }

    }
    public void selectfromSIDE(String value) throws AWTException {
        wait.until(ExpectedConditions.visibilityOf(sideDropdown));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click;", sideDropdown);
        sideDropdown.click();
        System.out.println("Triggered side drop down list");
        Robot robot = new Robot();
        try {
            if(value.equals("BID")){
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }
            if(value.equals("ASK")){
                for(int i = 0; i<2; i++){
                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                }
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

            }

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void selectfromType(String value) throws AWTException {

        wait.until(ExpectedConditions.visibilityOf(typeDropdown));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click;", typeDropdown);
        typeDropdown.click();
        System.out.println("Triggered type drop down list");
        Robot robot = new Robot();
        try {
            if(value.equalsIgnoreCase("AXE")){
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);

                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }
            if(value.equalsIgnoreCase("INV")){
                for(int i = 0; i<2; i++){
                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                }
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);

            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void clickCCYandValidateList(String value){

        wait.until(ExpectedConditions.visibilityOf(ccyDropdown));
        js.executeScript("arguments[0].click;", ccyDropdown);
        ccyDropdown.click();
        System.out.println("Triggered ccy dropdown");

        //Validate list:
        if(value.equalsIgnoreCase("Top CCY")){
            ccy_EURChkBox.isDisplayed();
            Assert.assertTrue("EUR is present....",true);
            System.out.println("EUR is present...");
            ccy_USDChkBox.isDisplayed();
            Assert.assertTrue("USD is present....",true);
            System.out.println("USd is present...");
            ccy_GBPChkBox.isDisplayed();
            Assert.assertTrue("GBP is present....",true);
            System.out.println("GBP is present...");
        }
        if(value.equalsIgnoreCase("Recent Search")){


        }


    }


    public void selectCCY(String value){

        wait.until(ExpectedConditions.elementToBeClickable(ccyDropdown));
        js.executeScript("arguments[0].click;", ccyDropdown);
        ccyDropdown.click();

        try {
            //verify if selected value is single or multiple...
            if(value.equalsIgnoreCase("Single")){

                //Uncheck CCY - ALL
                wait.until(ExpectedConditions.elementToBeClickable(ccy_AllChkBox));
                js.executeScript("arguments[0].click;", ccy_AllChkBox);
                ccy_AllChkBox.click();
                System.out.println("Triggered all checkbox");

                //Check EUR
                wait.until(ExpectedConditions.elementToBeClickable(ccy_EURChkBox));
                js.executeScript("arguments[0].click;", ccy_EURChkBox);
                ccy_EURChkBox.click();
                System.out.println("Triggered eur checkbox");
            }
            if(value.equalsIgnoreCase("Multiple")){
                //Check GBP
                wait.until(ExpectedConditions.elementToBeClickable(ccy_GBPChkBox));
                js.executeScript("arguments[0].click;", ccy_GBPChkBox);
                ccy_GBPChkBox.click();
                System.out.println("Triggered gbp checkbox");

                //Check USD
                wait.until(ExpectedConditions.elementToBeClickable(ccy_USDChkBox));
                js.executeScript("arguments[0].click;", ccy_USDChkBox);
                ccy_USDChkBox.click();
                System.out.println("Triggered usd checkbox");
            }
        }catch (Exception e){

            System.out.println("Cannot select element..");
        }
    }
    public void selectDealer(String value){

        wait.until(ExpectedConditions.elementToBeClickable(dealer_ChkBox));
        dealer_ChkBox.click();

        //Uncheck - ALL
        dealer_ALLChkBox.click();
        System.out.println("Triggered dealer all checkbox");

        try {
            if(value.equalsIgnoreCase("Single")){

                //check e trading
                dealer_BankA.click();
                System.out.println("Triggered dealer checkbox");

            }
            if(value.equalsIgnoreCase("Multiple")){

                //check available dealers:
                dealer_BankC.click();
                System.out.println("Triggered dealer bank C checkbox");

                dealer_BankD.click();
                System.out.println("Triggered dealer bank D checkbox");

            }

        }catch (Exception e){

            Assert.fail("Can't select element...");
            System.out.println("Can't select element...");

        }
    }
    public void selectAssetClass(String value){
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOf(assetClass_DropDown));
        js.executeScript("arguments[0].click;", assetClass_DropDown);
        assetClass_DropDown.click();

        //Uncheck - ALL
        js.executeScript("arguments[0].click;", assetClass_ALL);
        assetClass_ALL.click();
        System.out.println("Triggered asset class all checkbox");

        try{
            if(value.equalsIgnoreCase("Single")){

                //check asset class - financial
                //js.executeScript("arguments[0].click;", assetClass_Financial);
                assetClass_Corporate.click();
                System.out.println("Triggered asset class Corporate checkbox");
            }
            if(value.equalsIgnoreCase("Multiple")){

                //select agency and municipal
                js.executeScript("arguments[0].click;", assetClass_Agency);
                assetClass_Agency.click();
                System.out.println("Triggered asset class agency checkbox");

                js.executeScript("arguments[0].click;", assetClass_Municipal);
                assetClass_Municipal.click();
                System.out.println("Triggered asset class municipal checkbox");
            }
        }catch (Exception e){

            Assert.fail("Unable to select element...");
            System.out.println("Unable to select element...");
        }




    }
    public void selectSector(String value){

        wait.until(ExpectedConditions.visibilityOf(sectorDropdown));
        js.executeScript("arguments[0].click;", sectorDropdown);
        sectorDropdown.click();

        try {
            System.out.println(value);
            if(value.equalsIgnoreCase("Single")){

                //Uncheck - ALL
                js.executeScript("arguments[0].click;", sector_ALLChkBox);
                sector_ALLChkBox.click();
                System.out.println("Triggered sector all checkbox");

                js.executeScript("arguments[0].click;", sector_BasicResources);
                sector_BasicResources.click();
                System.out.println("Triggered sector basic resources checkbox");

            }
            System.out.println(value);
            if(value.equalsIgnoreCase("Multiple")){

                //check banks
                js.executeScript("arguments[0].click;", sector_Banks);
                sector_Banks.click();
                System.out.println("Triggered sector banks checkbox");

                //check covered
                js.executeScript("arguments[0].click;", sector_Covered);
                sector_Covered.click();
                System.out.println("Triggered sector covered checkbox");

            }
        }catch (Exception e){

            Assert.fail("Can't select the sector");
            System.out.println("Can't select the sector");
        }






    }
    public void selectRegion(String value){
        System.out.println(wait.until(ExpectedConditions.visibilityOf(region_DropDown)));
        wait.until(ExpectedConditions.elementToBeClickable(region_DropDown));
        js.executeScript("arguments[0].click;", region_DropDown);
        region_DropDown.click();

        try {
            if(value.equalsIgnoreCase("Single")){

                //uncheck all
                System.out.println(wait.until(ExpectedConditions.visibilityOf(region_ALLChkBox)));
                wait.until(ExpectedConditions.visibilityOf(region_ALLChkBox));
                js.executeScript("arguments[0].click;", region_ALLChkBox);
                region_ALLChkBox.click();
                System.out.println("Triggered region all checkbox");

                //Check asia from region
                wait.until(ExpectedConditions.visibilityOf(region_Asia));
                js.executeScript("arguments[0].click;", region_Asia);
                region_Asia.click();
                System.out.println("Triggered region asia checkbox");

            }
            if(value.equalsIgnoreCase("Multiple")){

                //Check europe from region
                //wait.until(ExpectedConditions.visibilityOf(region_Europe));
                js.executeScript("arguments[0].click;", region_Europe);
                region_Europe.click();
                System.out.println("Triggered region europe checkbox");

                //Check n.america from region
                wait.until(ExpectedConditions.visibilityOf(region_NA));
                js.executeScript("arguments[0].click;", region_NA);
                region_NA.click();
                System.out.println("Triggered region n.america checkbox");

            }
        }catch (Exception e){

            Assert.fail("Unable to select region!");
            System.out.println("Unable to select region!");

        }
    }

    public void selectCtryOfIssuer(String country_code,String value){

        wait.until(ExpectedConditions.visibilityOf(ctryIssuer_DropDown));
        //js.executeScript("arguments[0].click;", ctryIssuer_DropDown);
        ctryIssuer_DropDown.click();

        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

        try {
            System.out.println(value);
            if(value.equalsIgnoreCase("Single")){

                //uncheck all
                //js.executeScript("arguments[0].click;", ctryRisk_ALLChkBox);
                ctryIssuer_ALL.click();
                System.out.println("Triggered ctry of issuer all checkbox");

                //enter country_code in text field
                wait.until(ExpectedConditions.elementToBeClickable(ctryIssuer_txtField));
                ctryIssuer_txtField.clear();
                System.out.println("Cleared value before input");
                ctryIssuer_txtField.sendKeys(country_code);
                System.out.println("Entered country code");

                js.executeScript("arguments[0].click;", ctryOfIssuer_AUS);
                ctryOfIssuer_AUS.click();
                System.out.println("Triggered ctry of issuer AUS checkbox");
            }
            System.out.println(value);
            if(value.equalsIgnoreCase("Multiple")){

                //enter country_code in text field
                wait.until(ExpectedConditions.elementToBeClickable(ctryIssuer_txtField));
                ctryIssuer_txtField.clear();
                System.out.println("Cleared value before input...");
                ctryIssuer_txtField.sendKeys(country_code);
                System.out.println("Entered country code");

                //Check country from list
                js.executeScript("arguments[0].click;", ctryIssuer_USA);
                ctryIssuer_USA.click();
                System.out.println("Triggered ctry of issuer USA checkbox");

                js.executeScript("arguments[0].click;",ctryIssuer_UK);
                ctryIssuer_UK.click();
                System.out.println("Triggered ctry of issuer UK checkbox");

            }
        }catch (Exception e){

            Assert.fail("Ctry of issuer value not defined!");
            System.out.println("Ctry of issuer value not defined!");


        }

    }
    public void selectCtryOfRisk(String country_code,String value){

        wait.until(ExpectedConditions.visibilityOf(ctryRisk_DropDown));
        js.executeScript("arguments[0].click;", ctryRisk_DropDown);
        ctryRisk_DropDown.click();

      try {
          if(value.equalsIgnoreCase("Single")){

              //uncheck all
              js.executeScript("arguments[0].click;", ctryRisk_ALLChkBox);
              ctryRisk_ALLChkBox.click();
              System.out.println("Triggered ctry of risk all checkbox");

              //enter country_code in text field
              wait.until(ExpectedConditions.elementToBeClickable(ctryRisk_textField));
              ctryRisk_textField.clear();
              System.out.println("Cleared value before input....");
              ctryRisk_textField.sendKeys(country_code);
              System.out.println("Entered country code");

              //Check country from list
              js.executeScript("arguments[0].click;", ctryRisk_AUS);
              ctryRisk_AUS.click();
              System.out.println("Triggered ctry of risk AUS checkbox");
          }
          if(value.equalsIgnoreCase("Multiple")){

              //enter country_code in text field
              wait.until(ExpectedConditions.elementToBeClickable(ctryRisk_textField));
              ctryRisk_textField.clear();
              System.out.println("Cleared value before input....");
              ctryRisk_textField.sendKeys(country_code);
              System.out.println("Entered country code");

              //Check country from list
              js.executeScript("arguments[0].click;", ctryRisk_USA);
              ctryRisk_USA.click();
              System.out.println("Triggered ctry of risk USA checkbox");

              js.executeScript("arguments[0].click;",ctryRisk_GBR);
              ctryRisk_GBR.click();
              System.out.println("Triggered ctry of risk UK checkbox");
          }
      }catch (Exception e){

          Assert.fail("Ctry of Risk value not defined!");
          System.out.println("Ctry of Risk value not defined!");

      }
    }
    public void selectSeniority(String value){

        wait.until(ExpectedConditions.visibilityOf(seniority_DropDown));
        js.executeScript("arguments[0].click;", seniority_DropDown);
        seniority_DropDown.click();

        try {
            System.out.println(value);
            if(value.equalsIgnoreCase("Single")){

                //uncheck all
                wait.until(ExpectedConditions.visibilityOf(seniority_ALLChkBox));
                js.executeScript("arguments[0].click;", seniority_ALLChkBox);
                seniority_ALLChkBox.click();
                System.out.println("Triggered seniority all checkbox");


                //Check country from list
                System.out.println(wait.until(ExpectedConditions.visibilityOf(seniority_SenChkBox)));
                wait.until(ExpectedConditions.visibilityOf(seniority_SenChkBox));
                //js.executeScript("arguments[0].click;", seniority_SenChkBox);
                seniority_SenChkBox.click();
                System.out.println("Triggered seniority Seniority checkbox");
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            }
            System.out.println(value);
            if(value.equalsIgnoreCase("Multiple")){

                //wait.until(ExpectedConditions.visibilityOf(seniority_UnsubordinatedChkBox));
                js.executeScript("arguments[0].click;", seniority_UnsubordinatedChkBox);
                seniority_UnsubordinatedChkBox.click();
                System.out.println("Triggered seniority Unsubordinated checkbox");
                //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

                //wait.until(ExpectedConditions.visibilityOf(seniority_Subordinated));
                js.executeScript("arguments[0].click;", seniority_Subordinated);
                seniority_Subordinated.click();
                System.out.println("Triggered seniority subordinated checkbox");
                //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

            }
           /* else {
                Assert.fail("Value not defined!");
                System.out.println("Value not defined!");
            }*/
        }catch(Exception e){

            Assert.fail("Seniority value not defined!");
            System.out.println("Seniority value not defined!");

        }


    }
    public void selectVALUEfromSIDE(String value) throws InterruptedException {
        WebElement select = driver.findElement(By.xpath("//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select"));
        System.out.println("Triggered select");

        Thread.sleep(6000);

        List<WebElement> options = select.findElements(By.xpath("//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select/option"));
        for (WebElement option : options) {
            if (value.equals(option.getText()))
                option.click();
            System.out.println("Selected option is: " + value);
        }

    }

    public void clickSearchBtn() throws InterruptedException {
        //wait.until(ExpectedConditions.elementToBeClickable(searchBtn));
        try {

            if(searchBtn.isDisplayed()){
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchBtn);
                //searchBtn.click();
                System.out.println("Triggered search button");

                //Validate existence of data:
              if(dataCheck.isDisplayed()){
                    Assert.fail("No current axes/inventory on these securities...");
                    System.out.println("No current axes/inventory on these securities...");
                }
                else {
                    Assert.assertTrue("Initial data check...",true);
                    System.out.println("Initial data check...");
                    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Element not clickable at this point!");
        }
    }




    public boolean retryingFindClick(By by) {
        boolean result = false;
        int attempts = 0;
        while (attempts < 2) {
            try {
                driver.findElement(by).click();
                result = true;
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }
}
