package Runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/Features",
        glue = {"Steps"},
        //tags  = {"@AdvancedSearch","@QuickSearch","@WatchList","@SaveQuery"},
        //tags
        tags  = {"@wip"},
        plugin = {"pretty","html:target/cucumber-report-html", "json:target/cucumber.json" },
        strict = true,
        dryRun = false,
        monochrome = true)//generate report in different formats                                                                                                                           t reporting types

public class TestRunner {


}


